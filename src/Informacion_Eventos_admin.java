import java.awt.Component;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
//import java.sql.Date;
import java.util.StringTokenizer;
import java.util.Date;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import BD.Evento;
import BD.IAdministrador;
import BD.Parada;

public class Informacion_Eventos_admin extends Zona_Admin_Botones {
	private static Evento index;
	private JPanel eventosAdmin;
	private Informacion_Eventos evt;
	public Zona_Informacion_Eventos_admin zona_Informacion_Eventos_admin;
	
	
	
	public Informacion_Eventos_admin(){
		
		evt = new Informacion_Eventos();
		eventosAdmin=evt.getEventos();
		
		/*JTable a=new JTable();
		a.setTableHeader(evt.getEventosTable().getTableHeader());
		for (int c = 0; c < evt.getEventosTable().getColumnCount(); c++)
		{
		     Class col_class = evt.getEventosTable().getColumnClass(c);
		     Class col_class_a = evt.getEventosTable().getColumnClass(c);
		     evt.getEventosTable().setDefaultEditor(col_class, a.getDefaultEditor(col_class_a));        // Hace que se pueda editar
		}*/
		
		eventosAdmin.remove(evt.zona_Informacion_Eventos.getScrollPane());
		eventosAdmin.add(getA�adirB());
		getA�adirB().setBounds(12, 476, 89, 23);
		eventosAdmin.add(getModificarB());
		getModificarB().setBounds(111, 476, 89, 23);
		eventosAdmin.add(getBorrarB());
		getBorrarB().setBounds(210, 476, 89, 23);
		
		zona_Informacion_Eventos_admin=new Zona_Informacion_Eventos_admin();
		eventosAdmin.add(zona_Informacion_Eventos_admin.getAgregarB());
		eventosAdmin.add(zona_Informacion_Eventos_admin.getQuitarB());
		eventosAdmin.add(zona_Informacion_Eventos_admin.getScrollPaneF());
		eventosAdmin.add(zona_Informacion_Eventos_admin.getScrollPaneT());
		eventosAdmin.add(zona_Informacion_Eventos_admin.getSubirCartelB());
		evt.getEventosTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {

		    public void valueChanged(ListSelectionEvent lse) {
		        if(evt.getEventosTable().getSelectedRow()>-1&&evt.us!=null){
		        	aux();
		        	if(index!=null){
		        	evt.actualizarDetallesEvento(false);
		        	zona_Informacion_Eventos_admin.actualizarTF();
		        	zona_Informacion_Eventos_admin.actualizarTT();
		        	}
		        }
		    }
		});
		evt.actualizar(false);
		zona_Informacion_Eventos_admin.actualizarTF();
    	zona_Informacion_Eventos_admin.actualizarTT();
	}
	protected void aux() {
		if (evt.us.size()>evt.getEventosTable().getSelectedRow())
		index=evt.us.get(evt.getEventosTable().getSelectedRow());
		
	}
	
	public static Evento EventoOrmIndex(){
		return index;
	}
	public JPanel getEventosAdmin() {
		return eventosAdmin;
	}
	public void setEventosAdmin(JPanel eventosAdmin) {
		this.eventosAdmin = eventosAdmin;
	}
	
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addEvento("", "", new Date() , new Date(), "", "");
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		evt.actualizar(false);
	}
	@Override
	public void modificar(){
		JTable tabla = evt.getEventosTable();
		int fila = tabla.getSelectedRow();
		try {
			StringTokenizer fechaIni=new StringTokenizer((String)tabla.getValueAt(fila, 2), "/");
			int d=Integer.parseInt(fechaIni.nextToken()) ;
			int m=Integer.parseInt(fechaIni.nextToken()) ;
			int y=Integer.parseInt(fechaIni.nextToken()) ;
			StringTokenizer fechaFin=new StringTokenizer((String)tabla.getValueAt(fila, 3), "/");
			int d1=Integer.parseInt(fechaFin.nextToken()) ;
			int m1=Integer.parseInt(fechaFin.nextToken()) ;
			int y1=Integer.parseInt(fechaFin.nextToken()) ;
			MainAplicacion.getBdAdministrador().modEvento(evt.us.get(fila).getORMID(), (String)tabla.getValueAt(fila, 0), (String)tabla.getValueAt(fila, 1), new Date(y+"/"+m+"/"+d), new Date(y1+"/"+m1+"/"+d1), (String)tabla.getValueAt(fila, 4), evt.us.get(fila).getDirCartel());
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		evt.actualizar(false);
	}
	@Override
	public void borrar(){
		JTable tabla = evt.getEventosTable();
		int fila = tabla.getSelectedRow();
		try {
			Parada[] PE = MainAplicacion.getBdAdministrador().cargar_ParadasEvento(evt.us.get(fila).getORMID());
			if(PE!=null){
				for (int i = 0; i < PE.length; i++) {
					MainAplicacion.getBdAdministrador().quitarParada_LineaEvento(evt.us.get(fila).getORMID(), PE[i].getORMID());
				}
			}
			MainAplicacion.getBdAdministrador().delEvento(evt.us.get(fila).getORMID());
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		evt.actualizar(false);
	}
}