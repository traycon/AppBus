import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import BD.Evento;
import BD.Punto_Interes;

public class Zona_informacion_PuntosInteresParada_admin extends Modelo_asignacion {
	public Zona_informacion_PuntosInteresParada_admin(){
	
	this.getAgregarB().setBounds(400, 270, 41, 26);
	this.getQuitarB().setBounds(509, 270, 41, 26);
	this.getScrollPaneF().setBounds(400, 159, 150, 105);
	this.getScrollPaneT().setBounds(400, 300, 150, 105);
	this.getTablaTotal().setModel(new DefaultTableModel(
			new Object[][] {
					{null},
					{null},
					{null},
					{null},
				},
				new String[] {
					"P. Interes"
				}
			));
	for (int c = 0; c < this.getTablaTotal().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaTotal().getColumnClass(c);
	     this.getTablaTotal().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	this.getTablaFiltrada().setModel(new DefaultTableModel(
			new Object[][] {
					{null},{null},{null},{null},{null},{null},{null},
				},
				new String[] {
					"P. Interes"
				}
			));
	for (int c = 0; c < this.getTablaFiltrada().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaFiltrada().getColumnClass(c);
	     this.getTablaFiltrada().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	actualizarTT();
	actualizarTF();
	}

	public int getIndexParada(){
		if(Informacion_Paradas_admin.par.getParadasCB().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas_admin.par.getParadasCB().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	
	public void actualizarTF() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
			Punto_Interes[] eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_PuntosInteresParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.length][2];
						for (int i = 0; i < eventos.length; i++) {
									usTable[i][0]=eventos[i].getORMID();
									usTable[i][1]=eventos[i].getNombre();
						}
					}
		}
		getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Puntos interes"}));
		
		for (int c = 0; c <
				getTablaFiltrada().getColumnCount(); c++) 
		{
			Class col_class =
					getTablaFiltrada().getColumnClass(c);
			getTablaFiltrada().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getScrollPaneF().setViewportView(getTablaFiltrada());
		getScrollPaneF().revalidate();
		getScrollPaneF().repaint();
		
	}

	public void actualizarTT() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
			ArrayList<Punto_Interes> eventos = new ArrayList<Punto_Interes>();
			Punto_Interes[] PIF=null;
			try {
				eventos = MainAplicacion.getBdAdministrador().Cargar_PuntosInteres();
				PIF = MainAplicacion.getBdAdministrador().cargar_PuntosInteresParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
				if(eventos.size()>0&&PIF!=null){
					for (int i = 0; i < PIF.length; i++) {
						int id=buscarIndice(eventos,PIF[i].getORMID());
						if(id>-1){
						eventos.remove(id);
						}
					}
					
					usTable=new Object[eventos.size()][2];
						for (int i = 0; i < eventos.size(); i++) {
									usTable[i][0]=eventos.get(i).getORMID();
									usTable[i][1]=eventos.get(i).getNombre();
						}
					}
		}
		getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Puntos interes"}));
		
		for (int c = 0; c <
				getTablaTotal().getColumnCount(); c++) 
		{
			Class col_class =
					getTablaTotal().getColumnClass(c);
			getTablaTotal().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getScrollPaneT().setViewportView(getTablaTotal());
		getScrollPaneT().revalidate();
		getScrollPaneT().repaint();
		
	}
	private int buscarIndice(ArrayList<Punto_Interes> eventos, int ormid) {
		int aux=-1;
		for (int i = 0; i < eventos.size(); i++) {
			if(eventos.get(i).getORMID()==ormid){
				aux=i;
				break;
			}
		}
		return aux;
	}

	@Override
	public void agregar() {
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().agregarPuntosInteresParada(Integer.parseInt(tabla.getValueAt(fila, 0).toString()),Informacion_Paradas_admin.par.us.get(Informacion_Paradas_admin.par.getParadasCB().getSelectedIndex()).getORMID());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
	@Override
	public void quitar() {
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().quitarPuntosInteresParada(Integer.parseInt(tabla.getValueAt(fila, 0).toString()),Informacion_Paradas_admin.par.us.get(Informacion_Paradas_admin.par.getParadasCB().getSelectedIndex()).getORMID());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
}