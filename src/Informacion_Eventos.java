import java.awt.Color;
import java.awt.SystemColor;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import BD.Evento;
import BD.Linea;

public class Informacion_Eventos {
	private static Evento index;
	private JTable eventosTable;
	private JLabel eventosL;
	public Zona_Informacion_Eventos zona_Informacion_Eventos;
	private JPanel eventos;
	public ArrayList<Evento> us;
	public JScrollPane scrollPane_3;
	
	public Informacion_Eventos(){
		
		zona_Informacion_Eventos=new Zona_Informacion_Eventos();
		eventos=new JPanel();
		eventos.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		eventos.setForeground(Color.CYAN);
		eventos.setLayout(null);
		
		eventosL = new JLabel("Eventos :");
		eventosL.setBounds(11, 17, 118, 16);
		eventos.add(eventosL);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(12, 40, 621, 427);
		eventos.add(scrollPane_3);
		
		/*eventosTable = new JTable();
		eventosTable.setModel(new DefaultTableModel(
				new Object[][] {
						{null, null, null, null},
						{null, null, null, null},
						{null, null, null, null},
						{null, null, null, null},
						{null, null, null, null},
						{null, null, null, null},
						{null, null, null, null},
					},
					new String[] {
						"Nombre", "Lugar", "Fecha Inicio", "Fecha Fin"
					}
		));
		for (int c = 0; c < eventosTable.getColumnCount(); c++)
		{
		     Class col_class = eventosTable.getColumnClass(c);
		     eventosTable.setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		eventosTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_3.setViewportView(eventosTable);*/
		eventos.add(zona_Informacion_Eventos.getScrollPane());
		eventos.add(zona_Informacion_Eventos.getDescripcionL());
		eventos.add(zona_Informacion_Eventos.getDescripcionTA());
		eventos.add(zona_Informacion_Eventos.getCartel());
		eventos.add(zona_Informacion_Eventos.getCartelL());
		eventosTable = new JTable();
		eventosTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

		    public void valueChanged(ListSelectionEvent lse) {
		        if(eventosTable.getSelectedRow()>-1&& us!=null){
		        	aux();
		        	if (us.get(eventosTable.getSelectedRow())!=null)
		        	actualizarDetallesEvento(true);
		        }
		    }
		});
		actualizar(true);
	}
	
	protected void actualizarDetallesEvento(boolean b) {
		zona_Informacion_Eventos.getDescripcionTA().setText(us.get(eventosTable.getSelectedRow()).getDescripcion());
		String sourceCartel=us.get(eventosTable.getSelectedRow()).getDirCartel();
		zona_Informacion_Eventos.setCartel(new JEditorPane("text/html",
	            "<html><img src='"+sourceCartel+"' width=268height=161></img>"));
		zona_Informacion_Eventos.getCartel().revalidate();
		zona_Informacion_Eventos.getCartel().repaint();
		if(b){
			zona_Informacion_Eventos.actualizar(true);
		}
	}

	protected void actualizar(boolean b) {
		Object[][] desTable = new Object[1][7];
		try {
			us = MainAplicacion.getBdInvitado().Cargar_Eventos();
			if (us != null) {
				desTable = new Object[us.size()][6];
				for (int i = 0; i < us.size(); i++) {
					desTable[i][0] = us.get(i).getNombre();
					desTable[i][1] = us.get(i).getLugar();
					//desTable[i][2] = us.get(i).getFechaIni();
					//desTable[i][3] = us.get(i).getFechaFin();
					desTable[i][4] = us.get(i).getDescripcion();
					//desTable[i][5] = us.get(i).getDirCartel();
					SimpleDateFormat fechaFormat = new SimpleDateFormat("yyyy-MM-dd");
					StringTokenizer fechaSubida=new StringTokenizer(fechaFormat.format(us.get(i).getFechaIni()), "-");
					int y=Integer.parseInt(fechaSubida.nextToken()) ;
					int m=Integer.parseInt(fechaSubida.nextToken()) ;
					int d=Integer.parseInt(fechaSubida.nextToken()) ;
					StringTokenizer fechaModi=new StringTokenizer(fechaFormat.format(us.get(i).getFechaFin()), "-");
					int y1=Integer.parseInt(fechaModi.nextToken()) ;
					int m1=Integer.parseInt(fechaModi.nextToken()) ;
					int d1=Integer.parseInt(fechaModi.nextToken()) ;
					desTable[i][2] = d+"/"+m+"/"+y;
					desTable[i][3] = d1+"/"+m1+"/"+y1;
				}
				
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		eventosTable.setModel(new DefaultTableModel(desTable, new String[] {
				"Nombre", "Lugar", "Fecha Inicio", "Fecha Fin", "Descripcion" }));
		if (b){
		for (int c = 0; c <
				eventosTable.getColumnCount(); c++) 
		{
			Class col_class =
			eventosTable.getColumnClass(c);
			eventosTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}else{
			JTable a=new JTable();
			a.setTableHeader(eventosTable.getTableHeader());
			for (int c = 0; c < eventosTable.getColumnCount(); c++)
			{
			     Class col_class = eventosTable.getColumnClass(c);
			     Class col_class_a = eventosTable.getColumnClass(c);
			     eventosTable.setDefaultEditor(col_class, a.getDefaultEditor(col_class_a));        // Hace que se pueda editar
			}
		}
		eventosTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_3.setViewportView(eventosTable);
		scrollPane_3.revalidate();
		scrollPane_3.repaint();
	}
	
	public JTable getEventosTable() {
		return eventosTable;
	}
	public void setEventosTable(JTable eventosTable) {
		this.eventosTable = eventosTable;
	}
	public JLabel getEventosL() {
		return eventosL;
	}
	public void setEventosL(JLabel eventosL) {
		this.eventosL = eventosL;
	}
	public JPanel getEventos() {
		return eventos;
	}
	public void setEventos(JPanel eventos) {
		this.eventos = eventos;
	}
	protected void aux() {
		if (us.size()>eventosTable.getSelectedRow())
		index=us.get(eventosTable.getSelectedRow());
		
	}
	
	public static Evento EventoOrmIndex(){
		return index;
	}
}