import java.rmi.RemoteException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import BD.Evento;
import BD.Punto_Interes;

public class Informacion_Puntos_Interes_admin extends Zona_Admin_Botones {
	private JPanel PinteresAdmin;
	public Zona_Informacion_Puntos_Interes_admin zona_Informacion_Puntos_Interes_admin;
	private Informacion_Puntos_Interes pinter;
	private static Punto_Interes index;
	public Informacion_Puntos_Interes_admin(){
		pinter=new Informacion_Puntos_Interes();
		PinteresAdmin=pinter.getPuntosInteres();
		pinter.zona_Informacion_Puntos_Interes.getDescripcionTA().setEditable(true);
		pinter.zona_Informacion_Puntos_Interes.getDescripcionTA().setOpaque(true);
		
		PinteresAdmin.remove(pinter.zona_Informacion_Puntos_Interes.getScrollPane());
		PinteresAdmin.add(getA�adirB());
		getA�adirB().setBounds(12, 476, 89, 23);
		PinteresAdmin.add(getModificarB());
		getModificarB().setBounds(111, 476, 89, 23);
		PinteresAdmin.add(getBorrarB());
		getBorrarB().setBounds(210, 476, 89, 23);
		
		zona_Informacion_Puntos_Interes_admin=new Zona_Informacion_Puntos_Interes_admin();
		PinteresAdmin.add(zona_Informacion_Puntos_Interes_admin.getAgregarB());
		PinteresAdmin.add(zona_Informacion_Puntos_Interes_admin.getQuitarB());
		PinteresAdmin.add(zona_Informacion_Puntos_Interes_admin.getScrollPaneF());
		PinteresAdmin.add(zona_Informacion_Puntos_Interes_admin.getScrollPaneT());
		PinteresAdmin.add(zona_Informacion_Puntos_Interes_admin.getSubirFotoB());
		
		pinter.getPuntos_interesTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {

		    public void valueChanged(ListSelectionEvent lse) {
		        if(pinter.getPuntos_interesTable().getSelectedRow()>-1&&pinter.us!=null){
		        	aux();
		        	if(pinter.us.get(pinter.getPuntos_interesTable().getSelectedRow())!=null){
		        	pinter.actualizarDetallesPinter(false);
		        	zona_Informacion_Puntos_Interes_admin.actualizarTF();
		        	zona_Informacion_Puntos_Interes_admin.actualizarTT();
		        	}
		        }
		    }
		});
		pinter.actualizar(false);
		zona_Informacion_Puntos_Interes_admin.actualizarTF();
    	zona_Informacion_Puntos_Interes_admin.actualizarTT();
	}
	protected void aux() {
		if (pinter.us.size()>pinter.getPuntos_interesTable().getSelectedRow())
		index=pinter.us.get(pinter.getPuntos_interesTable().getSelectedRow());
		
	}
	
	public static Punto_Interes PuntoOrmIndex(){
		return index;
	}
	public JPanel getPinteresAdmin() {
		return PinteresAdmin;
	}
	public void setPinteresAdmin(JPanel pinteresAdmin) {
		PinteresAdmin = pinteresAdmin;
	}
	@Override
	public void a�adir() {
		try {
			MainAplicacion.getBdAdministrador().addPunto_Interes(
					"",
					"",
					0,
					new Date(),
					new Date(),
					pinter.zona_Informacion_Puntos_Interes.getDescripcionTA().getText(), "");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pinter.actualizar(false);
	}
	@Override
	public void modificar() {
		JTable tabla = pinter.getPuntos_interesTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().modPunto_Interes(
					index.getORMID(), tabla.getValueAt(fila, 0).toString(),
					tabla.getValueAt(fila, 1).toString(),
					Double.parseDouble(tabla.getValueAt(fila, 2).toString()),
					new Date("2000/02/02 "+tabla.getValueAt(fila, 3).toString()+":00"),
					new Date("2000/02/02 "+tabla.getValueAt(fila, 4).toString()+":00"),
					pinter.zona_Informacion_Puntos_Interes.getDescripcionTA().getText(), "");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pinter.actualizar(false);
	}
	@Override
	public void borrar() {
		try {
			Evento[] EPI = MainAplicacion.getBdAdministrador().cargar_Eventos_PuntosInteres(pinter.us.get(pinter.getPuntos_interesTable().getSelectedRow()).getORMID());
			if(EPI!=null){
				for (int i = 0; i < EPI.length; i++) {
					MainAplicacion.getBdAdministrador().quitarEventoPunto_Interes(EPI[i].getORMID(), pinter.us.get(pinter.getPuntos_interesTable().getSelectedRow()).getORMID());
				}
			}
			MainAplicacion.getBdAdministrador().delPunto_Interes(pinter.us.get(pinter.getPuntos_interesTable().getSelectedRow()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pinter.actualizar(false);
	}
}