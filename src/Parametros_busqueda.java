public class Parametros_busqueda {
	private paradas parada;
	private barrios__1 barrio;
	private calles calle;
	public Parametros_busqueda(){
		this.parada=new paradas();
		this.barrio=new barrios__1();
		this.calle= new calles();
	}
	public paradas getParada() {
		return parada;
	}
	public void setParada(paradas parada) {
		this.parada = parada;
	}
	public barrios__1 getBarrio() {
		return barrio;
	}
	public void setBarrio(barrios__1 barrio) {
		this.barrio = barrio;
	}
	public calles getCalle() {
		return calle;
	}
	public void setCalle(calles calle) {
		this.calle = calle;
	}
}