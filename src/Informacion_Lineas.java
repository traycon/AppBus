import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import maps.java.Route;

import BD.Linea;
import BD.Parada;

public class Informacion_Lineas {
	public ArrayList<Linea> us;
	private JComboBox lineasCB;
	private JLabel lineasL;
	public Zona_Informacion_Lineas zona_Informacion_Lineas;
	private JPanel lineas;
	private static Linea index;
	public Informacion_Lineas(){
		zona_Informacion_Lineas=new Zona_Informacion_Lineas();
		lineas=new JPanel();
		lineas.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		lineas.setForeground(Color.CYAN);
		lineas.setLayout(null);
		
		lineasL = new JLabel("Seleccionar Linea :");
		lineasL.setBounds(33, 29, 127, 16);
		lineas.add(lineasL);
		
		 lineasCB = new JComboBox();
		lineasCB.setBounds(151, 25, 152, 25);
		lineas.add(lineasCB);
		
		lineas.add(zona_Informacion_Lineas.getDetallesL());
		lineas.add(zona_Informacion_Lineas.getHoraEstimadaL());
		lineas.add(zona_Informacion_Lineas.getHoraEstimadaTF());
		lineas.add(zona_Informacion_Lineas.getHoraIniL());
		lineas.add(zona_Informacion_Lineas.getHoraIniTF());
		lineas.add(zona_Informacion_Lineas.getMapaL());
		lineas.add(zona_Informacion_Lineas.getNombreLineaL());
		lineas.add(zona_Informacion_Lineas.getNombreLineaTF());
		lineas.add(zona_Informacion_Lineas.getNombreParadaFinL());
		lineas.add(zona_Informacion_Lineas.getNombreParadaFinTF());
		lineas.add(zona_Informacion_Lineas.getNombreParadaIniL());
		lineas.add(zona_Informacion_Lineas.getNombreParadaIniTF());
		lineas.add(zona_Informacion_Lineas.getNumeroLineaL());
		lineas.add(zona_Informacion_Lineas.getNumeroLineaTF());
		lineas.add(zona_Informacion_Lineas.getObservacionesL());
		lineas.add(zona_Informacion_Lineas.getObservacionesTF());
		lineas.add(zona_Informacion_Lineas.getPanelMapa());
		lineas.add(zona_Informacion_Lineas.getParadaFinL());
		lineas.add(zona_Informacion_Lineas.getParadaFinTF());
		lineas.add(zona_Informacion_Lineas.getParadaIniL());
		lineas.add(zona_Informacion_Lineas.getParadaIniTF());
		lineas.add(zona_Informacion_Lineas.getParadasL());
		lineas.add(zona_Informacion_Lineas.getScrollPane());
		lineas.add(zona_Informacion_Lineas.getTarifaL());
		lineas.add(zona_Informacion_Lineas.getTarifaTF());
		
		lineasCB.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(lineasCB.getSelectedIndex()>-1 && !us.isEmpty()){
				aux();
				actualizarDetallesLinea(true);
				}
			}
			
		});
		actualizar();
	}
	protected void actualizarDetallesLinea(boolean b) {
		SimpleDateFormat horaFormat = new SimpleDateFormat("hh:mm");
		zona_Informacion_Lineas.getHoraIniTF().setText(horaFormat.format(us.get(lineasCB.getSelectedIndex()).getFechaIni()));
		zona_Informacion_Lineas.getHoraEstimadaTF().setText(horaFormat.format(us.get(lineasCB.getSelectedIndex()).getFechaFin()));
		zona_Informacion_Lineas.getNombreLineaTF().setText(us.get(lineasCB.getSelectedIndex()).getNombre());
		zona_Informacion_Lineas.getNombreParadaFinTF().setText(us.get(lineasCB.getSelectedIndex()).getNombreParadFin());
		zona_Informacion_Lineas.getNombreParadaIniTF().setText(us.get(lineasCB.getSelectedIndex()).getNombreParadaIni());
		zona_Informacion_Lineas.getNumeroLineaTF().setText(""+us.get(lineasCB.getSelectedIndex()).getORMID());
		zona_Informacion_Lineas.getObservacionesTF().setText(us.get(lineasCB.getSelectedIndex()).getObservaciones());
		zona_Informacion_Lineas.getParadaFinTF().setText(""+us.get(lineasCB.getSelectedIndex()).getParadaFin());
		zona_Informacion_Lineas.getParadaIniTF().setText(""+us.get(lineasCB.getSelectedIndex()).getParadaIni());
		zona_Informacion_Lineas.getTarifaTF().setText(""+us.get(lineasCB.getSelectedIndex()).getTarifa());
		try {
			zona_Informacion_Lineas.mapa.crearContenidoLinea(us.get(lineasCB.getSelectedIndex()).getORMID());
			zona_Informacion_Lineas.getPanelMapa().revalidate();
			zona_Informacion_Lineas.getPanelMapa().repaint();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (b){
		zona_Informacion_Lineas.actualizar(true);
		}
	}
	public void actualizar(){
		int lineaActual = lineasCB.getSelectedIndex();
		lineasCB.removeAllItems();
		
		try {
			us = MainAplicacion.getBdAdministrador().Cargar_lineas();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				lineasCB.addItem("Linea "+us.get(i).getORMID());
				comprobarLinea(us.get(i));
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		lineasCB.revalidate();
		lineasCB.repaint();
		if(us.size()>lineaActual){
		lineasCB.setSelectedIndex(lineaActual);
		}
		aux();
	}
	private void comprobarLinea(Linea linea) {
		try {
			Parada[] paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(linea.getORMID());
			if(paradas.length>0){
			linea.setParadaIni(paradas[0].getORMID());
			linea.setNombreParadaIni(paradas[0].getNombre());
			if(paradas.length>1){
			linea.setParadaFin(paradas[paradas.length-1].getORMID());
			linea.setNombreParadFin(paradas[paradas.length-1].getNombre());
			}
			SimpleDateFormat horaFormat = new SimpleDateFormat("hh:mm");
			StringTokenizer h=new StringTokenizer(horaFormat.format(linea.getFechaIni()), ":");
			int horaEstimada=Integer.parseInt(h.nextToken());
			int minEstimada=Integer.parseInt(h.nextToken());
			if(paradas.length>1){
			for (int i = 0; i < paradas.length-1; i++) {
				
				h=new StringTokenizer(horaFormat.format(tiempoDosPuntos(paradas[i].getDireccion(),paradas[i+1].getDireccion())), ":");
				horaEstimada+=Integer.parseInt(h.nextToken());
				minEstimada+=Integer.parseInt(h.nextToken());
				if(minEstimada>=60){
					minEstimada-=60;
					horaEstimada++;
				}
			}
			}
			linea.setFechaFin(new Date("2000/02/02 "+horaEstimada+":"+minEstimada+":00"));
			try {
				MainAplicacion.getBdAdministrador().modLinea(
						linea.getORMID(),
						linea.getNombre(), 
						linea.getORMID(), 
						linea.getParadaIni(), 
						linea.getFechaIni(), 
						linea.getNombreParadaIni(), 
						linea.getParadaFin(), 
						linea.getFechaFin(), 
						linea.getNombreParadFin(), 
						linea.getObservaciones(), 
						linea.getTarifa(), 
						"");
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			}else{
				try {
					MainAplicacion.getBdAdministrador().modLinea(
							linea.getORMID(),
							linea.getNombre(), 
							linea.getORMID(), 
							linea.getParadaIni(), 
							linea.getFechaIni(), 
							linea.getNombreParadaIni(), 
							linea.getParadaFin(), 
							linea.getFechaFin(), 
							linea.getNombreParadFin(), 
							linea.getObservaciones(), 
							linea.getTarifa(), 
							"");
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	private Date tiempoDosPuntos(String direccion, String direccion2) {
		int tiempo=0;
		Route ObjRout=new Route();
		int horas=0;
		String[][] resultadoRuta;
		try {
			resultadoRuta = ObjRout.getRoute(direccion+", Almeria", direccion2+", Almeria", null, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		if (resultadoRuta!=null){
		for(int i=0;i< resultadoRuta.length;i++){
			StringTokenizer token=new StringTokenizer(resultadoRuta[i][0], " ");
		   tiempo+=Integer.parseInt(token.nextToken());
		}
		}
		while(tiempo>=60){
			tiempo-=60;
			horas++;
		}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Date("2000/02/02 "+horas+":"+tiempo+":00");
	}
	protected void aux() {
		if (lineasCB.getSelectedIndex()>-1 && !us.isEmpty())
		index=us.get(lineasCB.getSelectedIndex());
		
	}
	
	public static Linea LineaOrmIndex(){
		return index;
	}
	public JComboBox getLineasCB() {
		return lineasCB;
	}
	public void setLineasCB(JComboBox lineasCB) {
		this.lineasCB = lineasCB;
	}
	public JLabel getLineasL() {
		return lineasL;
	}
	public void setLineasL(JLabel lineasL) {
		this.lineasL = lineasL;
	}
	public JPanel getLineas() {
		return lineas;
	}
	public void setLineas(JPanel lineas) {
		this.lineas = lineas;
	}
}