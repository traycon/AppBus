import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.hibernate.metamodel.relational.Size;

import BD.Linea;
import BD.Recorrido;

public class Zona_informacion_LineasParada_admin extends Modelo_asignacion {
	public Zona_informacion_LineasParada_admin(){
		
	this.getAgregarB().setBounds(20, 270, 41, 26);
	this.getQuitarB().setBounds(129, 270, 41, 26);
	this.getScrollPaneF().setBounds(20, 159, 150, 105);
	this.getScrollPaneT().setBounds(20, 300, 150, 105);
	this.getTablaTotal().setModel(new DefaultTableModel(
			new Object[][] {
					{null},
					{null},
					{null},
					{null},
				},
				new String[] {
					"Lineas"
				}
			));
	for (int c = 0; c < this.getTablaTotal().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaTotal().getColumnClass(c);
	     this.getTablaTotal().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	this.getTablaFiltrada().setModel(new DefaultTableModel(
			new Object[][] {
					{null},{null},{null},{null},{null},{null},{null},
				},
				new String[] {
					"Linea"
				}
			));
	for (int c = 0; c < this.getTablaFiltrada().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaFiltrada().getColumnClass(c);
	     this.getTablaFiltrada().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	actualizarTF();
	actualizarTT();
	}
	public int getIndexParada(){
		if(Informacion_Paradas_admin.par.getParadasCB().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas_admin.par.getParadasCB().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	public void actualizarTF() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
		try {
				
			
			ArrayList<Linea> lineas = MainAplicacion.getBdAdministrador().cargar_LineasParada(getIndexParada());
			if(lineas.size()>0){
			usTable = new Object[lineas.size()][2];
			for (int i = 0; i < lineas.size(); i++) {
				usTable[i][0]=lineas.get(i).getORMID();
				usTable[i][1]=lineas.get(i).getNombre();
			}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Linea"}));
		
		for (int c = 0; c <
				this.getTablaFiltrada().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaFiltrada().getColumnClass(c);
			this.getTablaFiltrada().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
		
	}

	public void actualizarTT() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
		try {
			ArrayList<Linea> lineas = MainAplicacion.getBdAdministrador().Cargar_lineas();
			ArrayList<Linea> lineAux = MainAplicacion.getBdAdministrador().cargar_LineasParada(getIndexParada());
			if(lineAux.size()>0){
				for (int i = 0; i < lineAux.size(); i++) {
					int id=buscarIndice(lineas,lineAux.get(i).getORMID());
					if(id>-1){
					lineas.remove(id);
					}
				}
			}
			usTable = new Object[lineas.size()][2];
			for (int i = 0; i < lineas.size(); i++) {
				usTable[i][0]=lineas.get(i).getORMID();
				usTable[i][1]=lineas.get(i).getNombre();
			}
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		this.getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Linea"}));
		
		for (int c = 0; c <
				this.getTablaTotal().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaTotal().getColumnClass(c);
			this.getTablaTotal().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneT().setViewportView(this.getTablaTotal());
		this.getScrollPaneT().revalidate();
		this.getScrollPaneT().repaint();
		
	}
	private int buscarIndice(ArrayList<Linea> lineas, int id) {
		int aux=-1;
		for (int i = 0; i < lineas.size(); i++) {
			if(lineas.get(i).getORMID()==id){
				aux=i;
				break;
			}
		}
		return aux;
	}
	@Override
	public void agregar() {
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().agregarLineasParada(getIndexParada(), Integer.parseInt(tabla.getValueAt(fila, 0).toString()), new Recorrido());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTT();
		actualizarTF();
	}
	@Override
	public void quitar() {
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().quitarLineasParada(getIndexParada(), Integer.parseInt(tabla.getValueAt(fila, 0).toString()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTT();
		actualizarTF();
	}
}