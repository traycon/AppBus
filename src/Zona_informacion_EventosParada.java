import java.rmi.RemoteException;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Zona_informacion_EventosParada {
	private JLabel eventosL;
	private JTable eventosTable;
	private JScrollPane scrollPaneEventos;
	public Zona_informacion_EventosParada(){
		 scrollPaneEventos = new JScrollPane();
		scrollPaneEventos.setBounds(212, 159, 150, 250);
		
		eventosTable = new JTable();
		eventosTable.setModel(new DefaultTableModel(
			new Object[][] {
				{null},
				{null},
				{null},
				{null},
				{null},
			},
			new String[] {
				"Eventos"
			}
		));
		for (int c = 0; c < eventosTable.getColumnCount(); c++)
		{
		     Class col_class = eventosTable.getColumnClass(c);
		     eventosTable.setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		eventosTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneEventos.setViewportView(eventosTable);
		
		 eventosL = new JLabel("\uF0B7 Eventos :");
		eventosL.setBounds(250, 135, 114, 16);
	}
	public JLabel getEventosL() {
		return eventosL;
	}
	public void setEventosL(JLabel eventosL) {
		this.eventosL = eventosL;
	}
	public JTable getEventosTable() {
		return eventosTable;
	}
	public void setEventosTable(JTable eventosTable) {
		this.eventosTable = eventosTable;
	}
	public JScrollPane getScrollPaneEventos() {
		return scrollPaneEventos;
	}
	public void setScrollPaneEventos(JScrollPane scrollPaneEventos) {
		this.scrollPaneEventos = scrollPaneEventos;
	}
	public int getIndexParada(){
		if(Informacion_Paradas.getParadasCBClone().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas.getParadasCBClone().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	public void actualizar() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
			BD.Evento[] eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_EventosParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.length][2];
						for (int i = 0; i < eventos.length; i++) {
									usTable[i][0]=eventos[i].getORMID();
									usTable[i][1]=eventos[i].getNombre();
						}
					}
		}
		eventosTable = new JTable();
		eventosTable.setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Evento"}));
		
		for (int c = 0; c <
				 eventosTable.getColumnCount(); c++) 
		{
			Class col_class =
			eventosTable.getColumnClass(c);
			eventosTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		eventosTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneEventos.setViewportView(eventosTable);
		scrollPaneEventos.revalidate();
		scrollPaneEventos.repaint();
		
	}
}