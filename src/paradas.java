import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.*;

import BD.Barrio;
import BD.Parada;

public class paradas {
	private JComboBox paradasCB;
	private ArrayList<Parada> us;
	private static Parada index=null;
	
	public paradas(){
		this.paradasCB=new JComboBox();
		actualizar();
		paradasCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
	public JComboBox getParadasCB() {
		return paradasCB;
	}
	public void setParadasCB(JComboBox paradasCB) {
		this.paradasCB = paradasCB;
	}
	
	public void actualizar(){
		paradasCB.removeAllItems();
		try {
			us = MainAplicacion.getBdInvitado().Cargar_Paradas();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				paradasCB.addItem(us.get(i).getNombre());
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		paradasCB.revalidate();
		paradasCB.repaint();
	}
	
	public Parada getIndex(){
		return index;
	}
	public void aux(){
		index=us.get(paradasCB.getSelectedIndex());
	}
	public ArrayList<Parada> getUs() {
		return us;
	}
	public void setUs(ArrayList<Parada> us) {
		this.us = us;
	}
}