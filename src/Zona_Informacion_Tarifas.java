import java.awt.SystemColor;

import javax.swing.*;

public class Zona_Informacion_Tarifas {
	private JLabel descripcionL;
	private JTextPane descripcionTP;
	public Zona_Informacion_Tarifas(){
		descripcionL = new JLabel("Recordar :");
		descripcionL.setBounds(337, 43, 75, 16);
		
		descripcionTP = new JTextPane();
		descripcionTP.setBackground(SystemColor.info);
		descripcionTP.setText("Los precios pueden cambiar, tanto si son bono o no.\r\nAs\u00ED que recomendamos revisar esta pesta\u00F1a\r\nperiodicamente, por si los precio han cambiado.\r\n\r\nSi no tiene tarjeta, puede ir a las oficinas para obtener\r\nuna y tambien acceso a mas opciones de la aplicacion.\r\n\r\nLos transbordos solo son validos los primeros 30 minutos.");
		descripcionTP.setBounds(337, 60, 321, 150);
		descripcionTP.setEditable(false);
	}
	public JLabel getDescripcionL() {
		return descripcionL;
	}
	public void setDescripcionL(JLabel descripcionL) {
		this.descripcionL = descripcionL;
	}
	public JTextPane getDescripcionTP() {
		return descripcionTP;
	}
	public void setDescripcionTP(JTextPane descripcionTP) {
		this.descripcionTP = descripcionTP;
	}
}