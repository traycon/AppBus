import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import org.hibernate.dialect.FirebirdDialect;

import maps.java.Route;

import BD.Barrio;
import BD.Calle;
import BD.Evento;
import BD.Historial_Solucion;
import BD.Linea;
import BD.Parada;

public class Aplicacion {
	private JButton compartirB;
	private JButton buscarB;
	private JComboBox mostrarCB;
	private JButton borrarB;
	public Consulta consulta;
	public Filtrar filtrar;
	public Zona_solucion zona_solucion;
	private JPanel aplicacion;
	
	private Historial_Solucion us;
	private ArrayList<ArrayList<TreeMap<Integer, Parada>>> solucion;
	
	public Aplicacion(){
		consulta=new Consulta();
		filtrar=new Filtrar();
		zona_solucion=new Zona_solucion();
		
		aplicacion=new JPanel();
		aplicacion.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		aplicacion.setForeground(Color.CYAN);
		aplicacion.setLayout(null);
		
		buscarB = new JButton("Buscar");
		buscarB.setIcon(new ImageIcon("C:\\Users\\ANTO\u00E9s\\Desktop\\IconoLupa.gif"));
		buscarB.setBounds(40, 447, 98, 26);
		buscarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*//consultar();
				String destino = zona_solucion.getMapa().destino = consulta.consultarDestino();
				String origen =zona_solucion.getMapa().origen = consulta.consultarOrigen();
				try {
					zona_solucion.getMapa().crearContenido();
					zona_solucion.getItinerario().escribirRuta(zona_solucion.getMapa().unnamed_googleMaps_.getRoute(origen, destino));
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				*/
				String origen = "";
				String destino = "";
				Parada[] origenes=null;
				Parada[] destinos=null;
				//String tipoFiltro ="";
				//int mostrar = 0;
				//Obtenemos la idConsulta
				String auxId = consulta.getIdConsulta().getIdTF().getText();
				boolean isIdConsulta = false;
				
				//Comprobamos si existe ya la consulta
				if(auxId != null){
					/*for(;;){
						if(consulta.getIdConsulta() == )
					}*/
					
					try {
						us = MainAplicacion.getBdInvitado().cargar_consulta(auxId);
						if(us != null)
							isIdConsulta = true;
						
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}
				}
				//Utilizamos los valores de la consulta que ya existe
				if (isIdConsulta == true){
					origen = us.getOrigen();
					destino = us.getDestino();
				}else{
					//Obtenemos el origen
					if (consulta.origen.getCalleOrigenRB().isSelected())
						origen =zona_solucion.getMapa().origen = consulta.consultarOrigenCalle();
					if (consulta.origen.getParadaOrigenRB().isSelected())	
						origen =zona_solucion.getMapa().origen = consulta.origen.getParada().getUs().get(consulta.origen.getParada().getParadasCB().getSelectedIndex()).getDireccion();
					if (consulta.origen.getBarrioOrigenRB().isSelected()){	
						ArrayList<Parada> paradas=new ArrayList<Parada>();
						Barrio bar=consulta.origen.getBarrio().getUs().get(consulta.origen.getBarrio().getBarriosCB().getSelectedIndex());
						try {
							paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
							
							
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 ArrayList<Parada> paradasBarrio = new ArrayList<Parada>();
						for (int i = 0; i < paradas.size(); i++) {
							if(paradas.get(i).getEs_contenida_paradas()!=null&&paradas.get(i).getEs_contenida_paradas().getORMID()==bar.getORMID()){
								paradasBarrio.add(paradas.get(i));
							}
						}
						if(!paradasBarrio.isEmpty()){
						origenes=new Parada[paradasBarrio.size()];
						for (int i = 0; i < origenes.length; i++) {
							origenes[i]=paradasBarrio.get(i);
						}
						}
					}
					//Obtenemos el destino
					if (consulta.destino.getCalleDestinRB().isSelected())
						destino =zona_solucion.getMapa().destino = consulta.consultarDestinoCalle();
					if (consulta.destino.getParadaDestinoRB().isSelected())	
						destino =zona_solucion.getMapa().destino = consulta.destino.getParada().getUs().get(consulta.destino.getParada().getParadasCB().getSelectedIndex()).getDireccion();
					if (consulta.destino.getBarrioDestinoRB().isSelected()){	
						ArrayList<Parada> paradas=new ArrayList<Parada>();
						Barrio bar=consulta.destino.getBarrio().getUs().get(consulta.destino.getBarrio().getBarriosCB().getSelectedIndex());
						try {
							paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
							
							
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 ArrayList<Parada> paradasBarrio = new ArrayList<Parada>();
						for (int i = 0; i < paradas.size(); i++) {
							if(paradas.get(i).getEs_contenida_paradas()!=null&&paradas.get(i).getEs_contenida_paradas().getORMID()==bar.getORMID()){
								paradasBarrio.add(paradas.get(i));
							}
						}
						if(!paradasBarrio.isEmpty()){
						destinos=new Parada[paradasBarrio.size()];
						for (int i = 0; i < destinos.length; i++) {
							destinos[i]=paradasBarrio.get(i);
						}
						}
					}
					
					if (consulta.destino.getEvtCultDestinoRB().isSelected()){	
						Evento evnt=consulta.destino.getEvento_cultural_().getUs().get(consulta.destino.getEvento_cultural_().getEventosCB().getSelectedIndex());
						try {
							destinos = MainAplicacion.getBdAdministrador().cargar_ParadasEvento(evnt.getORMID());
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				
				
				String tipoFiltro = "";
				//Obtenemos el filtro
				if(filtrar.getFiltrosCB().getSelectedIndex() == 0)
					tipoFiltro = "Menor Coste";
				if(filtrar.getFiltrosCB().getSelectedIndex() == 1)
					tipoFiltro = "Menor Tiempo";
				if(filtrar.getFiltrosCB().getSelectedIndex() == 2)
					tipoFiltro = "Menos Transbordos";
				
				
				//Obtenemos el numero de resultados
				int mostrar = 5;
				if(mostrarCB.getSelectedIndex() == 0)
					mostrar = 5;
				if(mostrarCB.getSelectedIndex() == 1)
					mostrar = 10;
				if(mostrarCB.getSelectedIndex() == 2)
					mostrar = 15;
				
				
				//Calculamos la ruta y mostramos el mapa
					if(origen!=""&&destino!=""){
						escribirItinerario(origen,destino,paradaParada(paradaCercana(origen), paradaCercana(destino), tipoFiltro));
					}else if(origen!=""&&destinos!=null){
						escribirItinerarioMultiple(origen,"",paradaParadas(paradaCercana(origen), destinos, tipoFiltro, mostrar));
					}else if(origenes!=null&&destino!=""){
						escribirItinerarioMultiple("",destino,paradaParadas(paradaCercana(destino), origenes, tipoFiltro, mostrar));
					}else if(origenes!=null&&destinos!=null){
						escribirItinerarioMultiple("","",paradasParadas(origenes, destinos, tipoFiltro, mostrar));
					}   
			}

		});
		aplicacion.add(buscarB);
		
		borrarB = new JButton("Borrar");
		borrarB.setIcon(new ImageIcon("C:\\Users\\ANTO\u00E9s\\Desktop\\delete_16.png"));
		borrarB.setBounds(158, 447, 98, 26);
		borrarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consulta.borrarOrigen();
				consulta.borrarDestino();
				zona_solucion.getMapa().origen = "";
				zona_solucion.getMapa().destino = "";
				try {
					zona_solucion.getMapa().crearContenido();
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}	
		});
		
		aplicacion.add(borrarB);
		JLabel mostrarL = new JLabel("N\u00FAmero Resultados :");
		mostrarL.setBounds(40, 368, 165, 16);
		aplicacion.add(mostrarL);
		
		mostrarCB = new JComboBox();
		mostrarCB.setModel(new DefaultComboBoxModel(new String[] {"5", "10", "15"}));
		mostrarCB.setBounds(40, 396, 125, 25);
		aplicacion.add(mostrarCB);
		
		compartirB= new JButton("Copiar C\u00F3digo");
		compartirB.setBounds(274, 447, 125, 26);
		compartirB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//compartir();
				   StringSelection stringSelection = new StringSelection(consulta.idconsulta.getIdTF().getText());
				   Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				   clipboard.setContents(stringSelection, stringSelection );
			}
		});
		aplicacion.add(compartirB);
		//componentes  consulta destino
		aplicacion.add(consulta.destino.getBarrioDestinoRB());
		aplicacion.add(consulta.destino.getCalleDestinRB());
		aplicacion.add(consulta.destino.getEvtCultDestinoRB());
		aplicacion.add(consulta.destino.getParadaDestinoRB());
		aplicacion.add(consulta.destino.getParamDestinoL());
		aplicacion.add(consulta.destino.getCalleDest());
		aplicacion.add(consulta.destino.getParadaDest());
		aplicacion.add(consulta.destino.getBarrioDest());
		aplicacion.add(consulta.destino.getEventoDest());
		//componentes consulta origen
		aplicacion.add(consulta.origen.getBarrioOrigenRB());
		aplicacion.add(consulta.origen.getCalleOrigenRB());
		aplicacion.add(consulta.origen.getParadaOrigenRB());
		aplicacion.add(consulta.origen.getParamOrigenL());
		aplicacion.add(consulta.origen.getCalleOrig());
		aplicacion.add(consulta.origen.getParadaOrig());
		aplicacion.add(consulta.origen.getBarrioOrig());
		//componentes id consulta
		aplicacion.add(consulta.idconsulta.getIdL());
		final JTextField idTF = consulta.idconsulta.getIdTF();
		idTF.addKeyListener(
	            new KeyListener(){

	                public void keyPressed(KeyEvent e){
	                	if(!idTF.getText().isEmpty()){
	                		consulta.destino.getBarrioDestinoRB().setEnabled(false);
	                		consulta.destino.getCalleDestinRB().setEnabled(false);
	                		consulta.destino.getEvtCultDestinoRB().setEnabled(false);
	                		consulta.destino.getParadaDestinoRB().setEnabled(false);
	       
	                		consulta.origen.getBarrioOrigenRB().setEnabled(false);
	                		consulta.origen.getCalleOrigenRB().setEnabled(false);
	                		consulta.origen.getParadaOrigenRB().setEnabled(false);
	                	}else{
	                		consulta.destino.getBarrioDestinoRB().setEnabled(true);
	                		consulta.destino.getCalleDestinRB().setEnabled(true);
	                		consulta.destino.getEvtCultDestinoRB().setEnabled(true);
	                		consulta.destino.getParadaDestinoRB().setEnabled(true);
	       
	                		consulta.origen.getBarrioOrigenRB().setEnabled(true);
	                		consulta.origen.getCalleOrigenRB().setEnabled(true);
	                		consulta.origen.getParadaOrigenRB().setEnabled(true);
	                	}
	                         
	                }

					
					public void keyTyped(KeyEvent e) {
						// TODO Auto-generated method stub
						
					}

					
					public void keyReleased(KeyEvent e) {
						// TODO Auto-generated method stub
						
					}
	            }
	        );
		aplicacion.add(idTF);
		//componentes filtros
		aplicacion.add(filtrar.getFiltrosL());
		aplicacion.add(filtrar.getFiltrosCB());
		//componentes zona solucion
		aplicacion.add(zona_solucion.getScrollPane());
		aplicacion.add(zona_solucion.getMapa().posicionMapa);
	}

	public JPanel getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(JPanel aplicacion) {
		this.aplicacion = aplicacion;
	}

	public void compartir() {
		throw new UnsupportedOperationException();
	}

	public void consultar() {
		throw new UnsupportedOperationException();
	}
	
	private ArrayList<ArrayList<TreeMap<Integer, Parada>>> paradasParadas(Parada[] origen, Parada[] destino,String tipoFiltro, int resultados){
		ArrayList<ArrayList<TreeMap<Integer, Parada>>> sol=new ArrayList<ArrayList<TreeMap<Integer,Parada>>>();
		ArrayList<ArrayList<TreeMap<Integer, Parada>>> aux=new ArrayList<ArrayList<TreeMap<Integer,Parada>>>();
		for (int i = 0; i < destino.length; i++) {
			for (int j = 0; j < origen.length; j++) {
				
			}
			sol.add(paradaParada(origen[i], destino[i], tipoFiltro));
		}
		while(aux.size()<resultados&&!sol.isEmpty()){
			ArrayList<TreeMap<Integer, Parada>> var = mejorRecorrido(sol,tipoFiltro);
			aux.add((ArrayList<TreeMap<Integer, Parada>>) var.clone());
			sol.remove(var);
		}
		return aux;
	}
	
	private ArrayList<ArrayList<TreeMap<Integer, Parada>>> paradaParadas(Parada origen, Parada[] destino,String tipoFiltro,int resultados){
		ArrayList<ArrayList<TreeMap<Integer, Parada>>> sol=new ArrayList<ArrayList<TreeMap<Integer,Parada>>>();
		ArrayList<ArrayList<TreeMap<Integer, Parada>>> aux=new ArrayList<ArrayList<TreeMap<Integer,Parada>>>();
		for (int i = 0; i < destino.length; i++) {
			sol.add(paradaParada(origen, destino[i], tipoFiltro));
		}

		while(aux.size()<resultados&&!sol.isEmpty()){
			ArrayList<TreeMap<Integer, Parada>> var = mejorRecorrido(sol,tipoFiltro);
			aux.add((ArrayList<TreeMap<Integer, Parada>>) var.clone());
			sol.remove(var);
		}
		return aux;
	}
	
	
	private ArrayList<TreeMap<Integer, Parada>> mejorRecorrido(
			ArrayList<ArrayList<TreeMap<Integer, Parada>>> sol,
			String tipoFiltro) {
ArrayList<TreeMap<Integer, Parada>> aux = null;
		
		if(tipoFiltro=="Menor Coste"){
			int min=Integer.MAX_VALUE;
			int minLinea=0;
			int actual=-1;
			
			for (int i = 0; i < sol.size(); i++) {
				minLinea=0;
				for (int j = 0; j < sol.get(i).size(); j++) {
					Iterator<Integer> iterator = sol.get(i).get(j).navigableKeySet().iterator();
					while(iterator.hasNext()){
						Integer item = iterator.next();
						if(item!=actual){
							actual=item;
							minLinea++;
						}
					}
					if(minLinea<min){
						min=minLinea;
						aux = sol.get(i);
					}
				}
			}
			
		}else if(tipoFiltro=="Menor Tiempo"){
			int min=Integer.MAX_VALUE;
			int tiempo=0;
			
			for (int i = 0; i < sol.size(); i++) {
				tiempo=0;
				for (int j = 0; j < sol.get(i).size(); j++) {
					Iterator<Integer> iterator = sol.get(i).get(j).navigableKeySet().iterator();
					while(iterator.hasNext()){
						tiempo+=tiempoDosPuntos(sol.get(i).get(j).get(iterator.next()).getDireccion(), sol.get(i).get(j).get(iterator.next()).getDireccion());	
					}
					if(tiempo<min){
						min=tiempo;
						aux = sol.get(i);
					}
				}
			}
		}else{
			int min=Integer.MAX_VALUE;
			int minLinea=0;
			int actual=-1;
			
			for (int i = 0; i < sol.size(); i++) {
				minLinea=0;
				for (int j = 0; j < sol.get(i).size(); j++) {
					Iterator<Integer> iterator = sol.get(i).get(j).navigableKeySet().iterator();
					while(iterator.hasNext()){
						Integer item = iterator.next();
						if(item!=actual){
							actual=item;
							minLinea++;
						}
					}
					if(minLinea<min){
						min=minLinea;
						aux = sol.get(i);
					}
				}
			}
		}
		return aux;
	}

	private ArrayList<TreeMap<Integer, Parada>> paradaParada(Parada origen, Parada destino,String tipoFiltro){
		ArrayList<Linea> lineas = mismasLineas(origen,destino);
		ArrayList<TreeMap<Integer, Parada>> aux = null;
		if (!lineas.isEmpty()){
			aux=rutaLineaParadaParada(origen,destino,lineas,tipoFiltro);
		}else{
			aux=rutaVariasLineasParadaParada(origen,destino,tipoFiltro);
		}
		return aux;
	}
	private ArrayList<TreeMap<Integer, Parada>> rutaVariasLineasParadaParada(
			Parada origen, Parada destino, String tipoFiltro) {
		
		TreeMap<Integer, ArrayList<Linea>> lineasParada=listaParadasLineas();
		TreeMap<Integer, Parada[]> paradasLinea = listaLineasParadas();
		lineasAlcanzadasParada(origen, destino, lineasParada, paradasLinea);
		ArrayList<TreeMap<Integer, Parada>> aux = null;
		
		if(tipoFiltro=="Menor Coste"){
			int min=Integer.MAX_VALUE;
			int minLinea=0;
			int actual=-1;
			
			for (int i = 0; i < solucion.size(); i++) {
				minLinea=0;
				for (int j = 0; j < solucion.get(i).size(); j++) {
					Iterator<Integer> iterator = solucion.get(i).get(j).navigableKeySet().iterator();
					while(iterator.hasNext()){
						Integer item = iterator.next();
						if(item!=actual){
							actual=item;
							minLinea++;
						}
					}
					if(minLinea<min){
						min=minLinea;
						aux = solucion.get(i);
					}
				}
			}
			
		}else if(tipoFiltro=="Menor Tiempo"){
			int min=Integer.MAX_VALUE;
			int tiempo=0;
			
			for (int i = 0; i < solucion.size(); i++) {
				tiempo=0;
				for (int j = 0; j < solucion.get(i).size()-1; j++) {
						tiempo+=tiempoDosPuntos(solucion.get(i).get(j).get(solucion.get(i).get(j).firstKey()).getDireccion(), solucion.get(i).get(j+1).get(solucion.get(i).get(j+1).firstKey()).getDireccion());	
					
					if(tiempo<min){
						min=tiempo;
						aux = solucion.get(i);
					}
				}
			}
		}else{
			int min=Integer.MAX_VALUE;
			int minLinea=0;
			int actual=-1;
			
			for (int i = 0; i < solucion.size(); i++) {
				minLinea=0;
				for (int j = 0; j < solucion.get(i).size(); j++) {
					Iterator<Integer> iterator = solucion.get(i).get(j).navigableKeySet().iterator();
					while(iterator.hasNext()){
						Integer item = iterator.next();
						if(item!=actual){
							actual=item;
							minLinea++;
						}
					}
					if(minLinea<min){
						min=minLinea;
						aux = solucion.get(i);
					}
				}
			}
		}
		return aux;
	}

	private void lineasAlcanzadasParada(Parada origen,Parada destino,TreeMap<Integer, ArrayList<Linea>> lineasParada,TreeMap<Integer, Parada[]> paradasLinea){
		ArrayList<TreeMap<Integer, Parada>> lineasAlcanzadas=new ArrayList<TreeMap<Integer, Parada>>();
		 Parada orig = origen;
		 Parada dest=destino;
		solucion=new ArrayList<ArrayList<TreeMap<Integer, Parada>>>();
		soluciones(origen,destino,lineasParada,paradasLinea,lineasAlcanzadas);
		for (int i = 0; i < solucion.size(); i++) {
			TreeMap<Integer, Parada> aux=new TreeMap<Integer, Parada>();
			aux.put(solucion.get(i).get(0).firstKey(), orig);
			solucion.get(i).add(0,(TreeMap<Integer, Parada>) aux.clone());
			

		}
	}
	
	private void soluciones(Parada origen, Parada destino,
			TreeMap<Integer, ArrayList<Linea>> lineasParada,
			TreeMap<Integer, Parada[]> paradasLinea, ArrayList<TreeMap<Integer, Parada>> lineasAlcanzadas) {
		ArrayList<Linea> line = mismasLineas(origen, destino);
		if(!mismasLineas(origen, destino).isEmpty()){
			TreeMap<Integer, Parada> aux=new TreeMap<Integer, Parada>();
			aux.put(line.get(0).getORMID(), destino);
			lineasAlcanzadas.add(aux);
			solucion.add((ArrayList<TreeMap<Integer, Parada>>) lineasAlcanzadas.clone());
		}else{
		TreeMap<Integer, Parada> aux=new TreeMap<Integer, Parada>();
		Iterator<Linea> iterator = lineasParada.get(origen.getORMID()).iterator();
		while(iterator.hasNext()){
			Linea linea = iterator.next();
			for (int i = 0; i < paradasLinea.get(linea.getORMID()).length; i++) {
				aux.put(linea.getORMID(), paradasLinea.get(linea.getORMID())[i]);
				if(!contiene(lineasAlcanzadas,aux)){
				lineasAlcanzadas.add((TreeMap<Integer, Parada>) aux.clone());
				soluciones(paradasLinea.get(linea.getORMID())[i],destino, lineasParada, paradasLinea, lineasAlcanzadas);
				
				}
				}
			
		}
		}
	}

	private boolean contiene(ArrayList<TreeMap<Integer, Parada>> lineasAlcanzadas, TreeMap<Integer, Parada> aux) {
		for (int i = 0; i < lineasAlcanzadas.size(); i++) {
				if(lineasAlcanzadas.get(i).get(aux.firstKey())!=null){
					if(lineasAlcanzadas.get(i).get(aux.firstKey()).getORMID()==aux.get(aux.firstKey()).getORMID()){
						return true;
					}
				}
			
			
		}
		return false;
	}

	private TreeMap<Integer, ArrayList<Linea>> listaParadasLineas() {
		TreeMap<Integer, ArrayList<Linea>> mapeo=new TreeMap<Integer, ArrayList<Linea>>();
				try {
					ArrayList<Parada> paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
					for (int i = 0; i < paradas.size(); i++) {
						mapeo.put(paradas.get(i).getORMID(), MainAplicacion.getBdAdministrador().cargar_LineasParada(paradas.get(i).getORMID()) );
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return mapeo;
	}
	private TreeMap<Integer, Parada[]> listaLineasParadas() {
		TreeMap<Integer, Parada[]> mapeo=new TreeMap<Integer, Parada[]>();
				try {
					ArrayList<Linea> lineas = MainAplicacion.getBdAdministrador().Cargar_lineas();
					for (int i = 0; i < lineas.size(); i++) {
						mapeo.put(lineas.get(i).getORMID(), MainAplicacion.getBdAdministrador().cargar_ParadaLinea(lineas.get(i).getORMID()) );
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return mapeo;
	}
	

	private ArrayList<TreeMap<Integer, Parada>> rutaLineaParadaParada(Parada origen, Parada destino,
			ArrayList<Linea> lineas, String tipoFiltro) {
		
		ArrayList<TreeMap<Integer, Parada>> solucionLinea =new ArrayList<TreeMap<Integer,Parada>>();
		
		if(tipoFiltro=="Menor Coste"){
			double minTarifa=Double.MAX_VALUE;
			Linea aux=null;
			for (int i = 0; i < lineas.size(); i++) {
				if(lineas.get(i).getTarifa()<minTarifa){
					minTarifa=lineas.get(i).getTarifa();
					aux=lineas.get(i);
				}
			}
			try {
				Parada[] paradasLinea = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(aux.getORMID());
				int add=0;
				for (int i = 0; i < paradasLinea.length; i++) {
					if(paradasLinea[i].getORMID()==origen.getORMID()||paradasLinea[i].getORMID()==destino.getORMID()){
						add++;
					}
					if (add==1){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
					}
					if (add==2){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
						break;
					}
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else if(tipoFiltro=="Menor Tiempo"){
			int tiempo=Integer.MAX_VALUE;
			
			Linea aux=null;
			for (int i = 0; i < lineas.size(); i++) {
				int tiempoAux=Integer.MAX_VALUE;
				try {
					Parada[] paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(lineas.get(i).getORMID());
					int empezar=0;
					tiempoAux=0;
					for (int j = 0; j < paradas.length-1; j++) {
						if(paradas[j].getORMID()==origen.getORMID()||paradas[j].getORMID()==destino.getORMID()){
							empezar++;
						}
						if (empezar==1){
							tiempoAux+=tiempoDosPuntos(paradas[j].getDireccion(), paradas[j+1].getDireccion());
						}
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(tiempoAux<=tiempo){
					tiempo=tiempoAux;
					aux=lineas.get(i);
				}
			}
			try {
				Parada[] paradasLinea = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(aux.getORMID());
				int add=0;
				for (int i = 0; i < paradasLinea.length; i++) {
					if(paradasLinea[i].getORMID()==origen.getORMID()||paradasLinea[i].getORMID()==destino.getORMID()){
						add++;
					}
					if (add==1){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
					}
					if (add==2){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
						break;
					}
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			double minTarifa=Double.MAX_VALUE;
			Linea aux=null;
			for (int i = 0; i < lineas.size(); i++) {
				if(lineas.get(i).getTarifa()<minTarifa){
					minTarifa=lineas.get(i).getTarifa();
					aux=lineas.get(i);
				}
			}
			try {
				Parada[] paradasLinea = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(aux.getORMID());
				int add=0;
				for (int i = 0; i < paradasLinea.length; i++) {
					if(paradasLinea[i].getORMID()==origen.getORMID()||paradasLinea[i].getORMID()==destino.getORMID()){
						add++;
					}
					if (add==1){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
					}
					if (add==2){
						TreeMap<Integer, Parada> sol=new TreeMap<Integer, Parada>();
						sol.put(aux.getORMID(), paradasLinea[i]);
						solucionLinea.add((TreeMap<Integer, Parada>) sol.clone());
						sol.clear();
						break;
					}
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return solucionLinea;
		
	}

	private void escribirItinerario(String origen, String destino, ArrayList<TreeMap<Integer, Parada>> Recorrido) {
		String recorrido="";
		ArrayList<String> ruta = new ArrayList<String>();
		ArrayList<TreeMap<Integer, Parada>> aux = new ArrayList<TreeMap<Integer,Parada>>();
		String orig=origen;
		String dest=destino;
		aux.addAll(Recorrido);
		
		if(origen!=""||destino!=""){
			if(origen!=""){
				if(aux.get(0).get(aux.get(0).firstKey()).getORMID()!=paradaCercana(origen).getORMID()){
					aux.clear();
					for (int i = Recorrido.size()-1; i >-1 ; i--) {
						aux.add(Recorrido.get(i));
					}
				}
			}else if(destino!=""){
if(aux.get(0).get(aux.get(0).firstKey()).getORMID()==paradaCercana(destino).getORMID()){
	aux.clear();
	for (int i = Recorrido.size()-1; i >-1 ; i--) {
		aux.add(Recorrido.get(i));
	}
				}
			}
			if(orig==""){
				orig=aux.get(0).get(aux.get(0).firstKey()).getDireccion();
			}
			if(dest==""){
				dest=aux.get(aux.size()-1).get(aux.get(aux.size()-1).firstKey()).getDireccion();
			}
		}
		
		if(origen!=""){
			recorrido+="Dir�jase desde "+origen+" a la parada ";
			ruta.add(origen+", Almeria");
		}else{
			recorrido+="Dir�jase a la parada ";
		}
		
		int linea=-1;
		recorrido+=aux.get(0).get(aux.get(0).firstKey()).getNombre()+":<br/>";
		for (int i = 0; i < aux.size(); i++) {
			
			ruta.add(aux.get(i).get(aux.get(i).firstKey()).getDireccion()+", Almeria");
			if(linea!=aux.get(i).firstKey()){
				if(linea==-1){
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;Suba a la linea "+aux.get(i).firstKey()+".<br/>";
				}else{
					if(aux.size()-1!=i){
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Baje en la parada "+aux.get(i-1).get(aux.get(i-1).firstKey()).getNombre()+".<br/><br/>";
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;Suba a la linea "+aux.get(i).firstKey()+".<br/>";
					}
				}
				linea=aux.get(i).firstKey();
			}
			if(aux.size()-1==i){
				recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Baje en la parada "+aux.get(i).get(aux.get(i).firstKey()).getNombre();
			}
		}
		if(destino!=""){
			recorrido+=" y camine hasta "+destino;
			ruta.add(destino+", Almeria");
		}
		recorrido+=".<br/><br/> <center>�Ha llegado a su destino!</center>";
		
		
		try {
			zona_solucion.getMapa().crearContenidoRuta(ruta);
			zona_solucion.getItinerario().escribirRuta(recorrido);
			if (Registrado.getCurrent()!=null) {
				try {
					MainAplicacion.getBdAdministrador().addHistorial(origen, destino, Registrado.getCurrent().getORMID());
					Historial.compartirB.doClick();
					ArrayList<Historial_Solucion> cod = MainAplicacion.getBdAdministrador().Cargar_Historial(Registrado.getCurrent().getORMID());
					String codigo = cod.get(cod.size()-1).getCodigo();
					consulta.idconsulta.getIdTF().setText(codigo);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
	}
	private void escribirItinerarioMultiple(String origen, String destino,ArrayList<ArrayList<TreeMap<Integer, Parada>>> Recorrido) {
		String recorrido="";
		ArrayList<String> ruta = new ArrayList<String>();
		String orig=origen;
		String dest=destino;
		for (int l = 0; l < Recorrido.size(); l++) {
			ArrayList<TreeMap<Integer, Parada>> aux = new ArrayList<TreeMap<Integer,Parada>>();
		
		aux.addAll(Recorrido.get(l));
		if(origen!=""||destino!=""){
			if(origen!=""){
				if(aux.get(0).get(aux.get(0).firstKey()).getORMID()!=paradaCercana(origen).getORMID()){
					aux.clear();
					for (int i = Recorrido.size()-1; i >-1 ; i--) {
						aux.add(Recorrido.get(l).get(i));
					}
				}
			}else if(destino!=""){
if(aux.get(0).get(aux.get(0).firstKey()).getORMID()==paradaCercana(destino).getORMID()){
	aux.clear();
	for (int i = Recorrido.size()-1; i >-1 ; i--) {
		aux.add(Recorrido.get(l).get(i));
	}
				}
			}
			if(orig==""){
				orig=aux.get(0).get(aux.get(0).firstKey()).getDireccion();
			}
			if(dest==""){
				dest=aux.get(aux.size()-1).get(aux.get(aux.size()-1).firstKey()).getDireccion();
			}
		}
		if(origen!=""){
			recorrido+="Ruta "+(l+1)+":<br/>Dir�jase desde "+origen+" a la parada ";
			ruta.add(origen+", Almeria");
		}else{
			recorrido+="Ruta "+(l+1)+":<br/>Dir�jase a la parada ";
		}
		
		int linea=-1;
		recorrido+=aux.get(0).get(aux.get(0).firstKey()).getNombre()+":<br/>";
		for (int i = 0; i < aux.size(); i++) {
			
			ruta.add(aux.get(i).get(aux.get(i).firstKey()).getDireccion()+", Almeria");
			if(linea!=aux.get(i).firstKey()){
				if(linea==-1){
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;Suba a la linea "+aux.get(i).firstKey()+".<br/>";
				}else{
					if(aux.size()-1!=i){
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Baje en la parada "+aux.get(i-1).get(aux.get(i-1).firstKey()).getNombre()+".<br/><br/>";
					recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;Suba a la linea "+aux.get(i).firstKey()+".<br/>";
					}
				}
				linea=aux.get(i).firstKey();
			}
			if(aux.size()-1==i){
				recorrido+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Baje en la parada "+aux.get(i).get(aux.get(i).firstKey()).getNombre();
			}
		}
		if(destino!=""){
			recorrido+=" y camine hasta "+destino;
			ruta.add(destino+", Almeria");
		}
		recorrido+=".<br/><br/> <center>�Ha llegado a su destino!</center><br/><br/>";
		
		
		try {
			if(l==0){
			zona_solucion.getMapa().crearContenidoRuta(ruta);
			}
			
			
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		}
		zona_solucion.getItinerario().escribirRuta(recorrido);
		if (Registrado.getCurrent()!=null) {
			try {
				MainAplicacion.getBdAdministrador().addHistorial(orig, dest, Registrado.getCurrent().getORMID());
				Historial.compartirB.doClick();
				ArrayList<Historial_Solucion> cod = MainAplicacion.getBdAdministrador().Cargar_Historial(Registrado.getCurrent().getORMID());
				String codigo = cod.get(cod.size()-1).getCodigo();
				consulta.idconsulta.getIdTF().setText(codigo);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private ArrayList<Linea> mismasLineas(Parada origen, Parada destino) {
		ArrayList<Linea> lineas=new ArrayList<Linea>();
		try {
			ArrayList<Linea> lineasO = MainAplicacion.getBdAdministrador().cargar_LineasParada(origen.getORMID());
			ArrayList<Linea> lineasD = MainAplicacion.getBdAdministrador().cargar_LineasParada(destino.getORMID());
			if(lineasD!=null&&!lineasD.isEmpty()&&lineasO!=null&&!lineasO.isEmpty()){
				for (int i = 0; i < lineasO.size(); i++) {
					for (int j = 0; j < lineasD.size(); j++) {
						if(lineasO.get(i).getORMID()==lineasD.get(j).getORMID()){
							lineas.add(lineasO.get(i));
						}
					}
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return lineas;
	}
	

	private Parada paradaCercana(String direccion){
		Parada aux=null;
		double metros=Double.MAX_VALUE;
		try {
			ArrayList<Parada> paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
			for (int i = 0; i < paradas.size(); i++) {
				double dist=distanciaDosPuntos(direccion, paradas.get(i).getDireccion());
				if(dist<=metros){
					metros=dist;
					aux=paradas.get(i);
				}
				
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aux;
		
	}
	private int tiempoDosPuntos(String direccion, String direccion2) {
		int tiempo=0;
		Route ObjRout=new Route();
		String[][] resultadoRuta;
		try {
			resultadoRuta = ObjRout.getRoute(direccion+", Almeria", direccion2+", Almeria", null, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		
		for(int i=0;i< resultadoRuta.length;i++){
			StringTokenizer token=new StringTokenizer(resultadoRuta[i][0], " ");
		   tiempo+=Integer.parseInt(token.nextToken());
		}
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tiempo;
	}
	private Double distanciaDosPuntos(String direccion, String direccion2) {
		double dist=0;
		Route ObjRout=new Route();
		String[][] resultadoRuta;
		try {
			resultadoRuta = ObjRout.getRoute(direccion+", Almeria", direccion2+", Almeria", null, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		
		for(int i=0;i< resultadoRuta.length;i++){
			StringTokenizer token=new StringTokenizer(resultadoRuta[i][1], " ");
			double aux=Double.parseDouble(token.nextToken().replace(",", "."));
			String medida=token.nextToken();
			if(medida.equalsIgnoreCase("Km")){
				dist+=(aux*1000);
			}else{
			dist+=aux;
			}
		}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dist;
	}
}