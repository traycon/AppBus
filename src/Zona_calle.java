import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.chainsaw.Main;

import BD.Calle;
import BD.IAdministrador;

public class Zona_calle extends Zona_Admin_Botones {
	private JLabel calleL;
	private JTable calleTable;
	private JScrollPane scrollPane_calle;
	private ArrayList<Calle> us;
	
	public Zona_calle(){
		getA�adirB().setBounds(243, 219, 89, 23);
		getModificarB().setBounds(342, 219, 89, 23);
		getBorrarB().setBounds(441, 219, 89, 23);
		calleL = new JLabel("Calle:");
		calleL.setBounds(243, 51, 80, 14);
		scrollPane_calle = new JScrollPane();
		scrollPane_calle.setBounds(243, 76, 287, 132);
		calleTable = new JTable();
			calleTable.setModel(new DefaultTableModel(
					new Object[][] {
							{null},
						},
						new String[] {
							"Calles"
						}
					));
			if (Barrios__2.BarrioOrmIndex().getSelectedIndex()>-1){
				actualizar();
				}
		calleTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_calle.setViewportView(calleTable);
		
}
	public void actualizar(){
		Object[][] usTable = new Object[0][1];
		if(Barrios__2.BarrioOrmIndex().getSelectedIndex()>-1){
		try {
			us = MainAplicacion.getBdAdministrador().cargar_calles(Barrios__2.getUsClone().get(Barrios__2.BarrioOrmIndex().getSelectedIndex()).getORMID());
			if (us!=null){
			usTable=new Object[us.size()][1];
			for (int i = 0; i < us.size(); i++) {
				usTable[i][0]=us.get(i).getDireccion();
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		}
		calleTable.setModel(new DefaultTableModel(
			usTable,
			new String[] {
				"Calles"
			}
		));
		
		scrollPane_calle.setViewportView(calleTable);
		scrollPane_calle.revalidate();
		scrollPane_calle.repaint();
	}

	public JLabel getCalleL() {
		return calleL;
	}

	public void setCalleL(JLabel calleL) {
		this.calleL = calleL;
	}

	public JTable getCalleTable() {
		return calleTable;
	}

	public void setCalleTable(JTable calleTable) {
		this.calleTable = calleTable;
	}

	public JScrollPane getScrollPane_calle() {
		return scrollPane_calle;
	}

	public void setScrollPane_calle(JScrollPane scrollPane_calle) {
		this.scrollPane_calle = scrollPane_calle;
	}
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addCalle("",Barrios__2.getUsClone().get(Barrios__2.BarrioOrmIndex().getSelectedIndex()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
	}
	@Override
	public void modificar(){
		JTable tabla = getCalleTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().modCalle(us.get(fila).getORMID(), tabla.getValueAt(fila, 0).toString());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
	}
	@Override
	public void borrar(){
		JTable tabla = getCalleTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().delCalle(us.get(fila).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
	}
}