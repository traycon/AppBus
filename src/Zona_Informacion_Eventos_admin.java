import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.chainsaw.Main;

import com.mysql.fabric.xmlrpc.base.Array;

import BD.Linea;
import BD.Parada;

public class Zona_Informacion_Eventos_admin extends Modelo_asignacion {
	private JButton subirCartelB;

	public Zona_Informacion_Eventos_admin(){
		subirCartelB = new JButton("Subir Cartel");
		subirCartelB.setBounds(683, 9, 120, 26);
		subirCartelB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subirFoto();
			}
		});
		this.getAgregarB().setBounds(730, 395, 41, 26);
		this.getQuitarB().setBounds(788, 395, 41, 26);
		this.getScrollPaneF().setBounds(644, 300, 268, 90);
		this.getScrollPaneT().setBounds(644, 425, 270, 75);

		actualizarTT();
		actualizarTF();
	}
 void actualizarTF() {
		Object[][] usTable=new Object[0][3];
		if(Informacion_Eventos_admin.EventoOrmIndex()!=null){
			Parada[] paradas = null;
			try {
				paradas = MainAplicacion.getBdAdministrador().cargar_ParadasEvento(Informacion_Eventos_admin.EventoOrmIndex().getORMID());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(paradas!=null){
					usTable=new Object[paradas.length][3];
						for (int i = 0; i < paradas.length; i++) {
							ArrayList<Linea> lineas=null;
							try {
								lineas=MainAplicacion.getBdAdministrador().cargar_LineasParada(paradas[i].getORMID());
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (lineas!=null){
								for (int j = 0; j < lineas.size(); j++) {
									usTable[i][0]=paradas[i].getORMID();
									usTable[i][1]=paradas[i].getDireccion();
									usTable[i][2]="Linea "+lineas.get(j).getORMID();
								}
							}
							
						}
					}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Direccion","Linea"}));
		
		for (int c = 0; c <
				this.getTablaFiltrada().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaFiltrada().getColumnClass(c);
			this.getTablaFiltrada().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
		
	}
 void actualizarTT() {
	 Object[][] usTable=new Object[0][3];
		if(Informacion_Eventos_admin.EventoOrmIndex()!=null){
			Parada[] paradas = null;
			ArrayList<Parada> cont=new ArrayList<Parada>();
			try {
				paradas = MainAplicacion.getBdAdministrador().cargar_ParadasEvento(Informacion_Eventos_admin.EventoOrmIndex().getORMID());
				cont=MainAplicacion.getBdAdministrador().Cargar_Paradas();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(paradas!=null){
					for (int i = 0; i < paradas.length; i++) {
						int id=buscarIndice(cont,paradas[i].getORMID());
						if(id>-1){
						cont.remove(id);
						}
					}
					
					
					usTable=new Object[cont.size()][3];
					if(cont.size()>0){
						for (int i = 0; i < cont.size(); i++) {
							ArrayList<Linea> lineas=null;
							try {
								lineas=MainAplicacion.getBdAdministrador().cargar_LineasParada(cont.get(i).getORMID());
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (lineas!=null){
								for (int j = 0; j < lineas.size(); j++) {
									usTable[i][0]=cont.get(i).getORMID();
									usTable[i][1]=cont.get(i).getDireccion();
									usTable[i][2]="Linea "+lineas.get(j).getORMID();
								}
							}
							
						}
					}
					}
		}
		this.getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Direccion","Linea"}));
		
		for (int c = 0; c <
				this.getTablaTotal().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaTotal().getColumnClass(c);
			this.getTablaTotal().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneT().setViewportView(this.getTablaTotal());
		this.getScrollPaneT().revalidate();
		this.getScrollPaneT().repaint();
		
	}
	private int buscarIndice(ArrayList<Parada> cont, int ormid) {
	int aux=-1;
	for (int i = 0; i < cont.size(); i++) {
		if(cont.get(i).getORMID()==ormid){
			aux=i;
			break;
		}
	}
	return aux;
}
	public void subirFoto() {
		throw new UnsupportedOperationException();
	}
	public JButton getSubirCartelB() {
		return subirCartelB;
	}
	public void setSubirCartelB(JButton subirCartelB) {
		this.subirCartelB = subirCartelB;
	}
	@Override
	public void agregar() {
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().agregarParada_LineaEvento(Informacion_Eventos_admin.EventoOrmIndex().getORMID(),Integer.parseInt(tabla.getValueAt(fila, 0).toString()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
	@Override
	public void quitar() {
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().quitarParada_LineaEvento(Informacion_Eventos_admin.EventoOrmIndex().getORMID(),Integer.parseInt(tabla.getValueAt(fila, 0).toString()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
}