import java.awt.SystemColor;

import javax.swing.*;

public class Zona_Configuracion {
	private JLabel nombreL;
	private JTextField nombreTF;
	private JLabel oldPasswordL;
	private JPasswordField oldPasswordTF;
	private JLabel newPasswordL;
	private JPasswordField newPasswordTF;
	private JLabel newPassword2L;
	private JPasswordField newPassword2TF;
	private JLabel bonoL;
	private JTextField bonoTF;
public Zona_Configuracion(){
	 nombreL = new JLabel("Nombre :");
	nombreL.setBounds(121, 79, 55, 16);
	
	 oldPasswordL = new JLabel("Contrase\u00F1a Actual :");
	oldPasswordL.setBounds(121, 107, 111, 16);
	
	 newPasswordL = new JLabel("Contrase\u00F1a Nueva :");
	newPasswordL.setBounds(121, 135, 111, 16);
	
	 newPassword2L = new JLabel("Reescribir Nueva Contrase\u00F1a :");
	newPassword2L.setBounds(121, 163, 172, 16);
	
	nombreTF = new JTextField();
	nombreTF.setBackground(SystemColor.textHighlightText);
	nombreTF.setEditable(false);
	nombreTF.setBounds(309, 77, 323, 20);
	nombreTF.setColumns(10);
	
	oldPasswordTF = new JPasswordField();
	oldPasswordTF.setBounds(309, 105, 323, 20);
	
	newPasswordTF = new JPasswordField();
	newPasswordTF.setBounds(309, 133, 323, 20);
	
	newPassword2TF = new JPasswordField();
	newPassword2TF.setBounds(309, 161, 323, 20);
	
	bonoL = new JLabel("Id. Bono :");
	bonoL.setBounds(121, 191, 55, 16);
	
	bonoTF = new JTextField();
	bonoTF.setBackground(SystemColor.textHighlightText);
	bonoTF.setEditable(false);
	bonoTF.setBounds(309, 189, 323, 20);
	bonoTF.setColumns(10);
}
public JLabel getNombreL() {
	return nombreL;
}
public void setNombreL(JLabel nombreL) {
	this.nombreL = nombreL;
}
public JTextField getNombreTF() {
	return nombreTF;
}
public void setNombreTF(JTextField nombreTF) {
	this.nombreTF = nombreTF;
}
public JLabel getOldPasswordL() {
	return oldPasswordL;
}
public void setOldPasswordL(JLabel oldPasswordL) {
	this.oldPasswordL = oldPasswordL;
}
public JPasswordField getOldPasswordTF() {
	return oldPasswordTF;
}
public void setOldPasswordTF(JPasswordField oldPasswordTF) {
	this.oldPasswordTF = oldPasswordTF;
}
public JLabel getNewPasswordL() {
	return newPasswordL;
}
public void setNewPasswordL(JLabel newPasswordL) {
	this.newPasswordL = newPasswordL;
}
public JPasswordField getNewPasswordTF() {
	return newPasswordTF;
}
public void setNewPasswordTF(JPasswordField newPasswordTF) {
	this.newPasswordTF = newPasswordTF;
}
public JLabel getNewPassword2L() {
	return newPassword2L;
}
public void setNewPassword2L(JLabel newPassword2L) {
	this.newPassword2L = newPassword2L;
}
public JPasswordField getNewPassword2TF() {
	return newPassword2TF;
}
public void setNewPassword2TF(JPasswordField newPassword2TF) {
	this.newPassword2TF = newPassword2TF;
}
public JLabel getBonoL() {
	return bonoL;
}
public void setBonoL(JLabel bonoL) {
	this.bonoL = bonoL;
}
public JTextField getBonoTF() {
	return bonoTF;
}
public void setBonoTF(JTextField bonoTF) {
	this.bonoTF = bonoTF;
}
}