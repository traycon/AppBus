import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Linea;

public class Zona_informacion_LineasParada {
	private JLabel lineasL;
	private JTable lineasTable;
	private JScrollPane scrollPaneLineas;
	public Zona_informacion_LineasParada(){
		 lineasL = new JLabel("\uF0B7 Lineas :");
		lineasL.setBounds(61, 135, 95, 16);
		
		 scrollPaneLineas = new JScrollPane();
		scrollPaneLineas.setBounds(20, 159, 150, 250);
		
		lineasTable = new JTable();
		lineasTable.setModel(new DefaultTableModel(
			new Object[][] {
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
			},
			new String[] {
				"Lineas"
			}
		));
		for (int c = 0; c < lineasTable.getColumnCount(); c++)
		{
		     Class col_class = lineasTable.getColumnClass(c);
		     lineasTable.setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		lineasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneLineas.setViewportView(lineasTable);
	}
	public int getIndexParada(){
		if(Informacion_Paradas.getParadasCBClone().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas.getParadasCBClone().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	public JLabel getLineasL() {
		return lineasL;
	}
	public void setLineasL(JLabel lineasL) {
		this.lineasL = lineasL;
	}
	public JTable getLineasTable() {
		return lineasTable;
	}
	public void setLineasTable(JTable lineasTable) {
		this.lineasTable = lineasTable;
	}
	public JScrollPane getScrollPaneLineas() {
		return scrollPaneLineas;
	}
	public void setScrollPaneLineas(JScrollPane scrollPaneLineas) {
		this.scrollPaneLineas = scrollPaneLineas;
	}
	public void actualizar() {
		Object[][] usTable=new Object[1][2];
		if(getIndexParada()>-1){
			ArrayList<Linea> eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_LineasParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.size()][2];
						for (int i = 0; i < eventos.size(); i++) {
									usTable[i][0]=eventos.get(i).getORMID();
									usTable[i][1]=eventos.get(i).getNombre();
						}
					}
		}
		lineasTable = new JTable();
		lineasTable.setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Linea"}));
		
		for (int c = 0; c <
				 lineasTable.getColumnCount(); c++) 
		{
			Class col_class =
			lineasTable.getColumnClass(c);
			lineasTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		lineasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneLineas.setViewportView(lineasTable);
		scrollPaneLineas.revalidate();
		scrollPaneLineas.repaint();
		
	}
}