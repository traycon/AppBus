import javax.swing.JScrollPane;

public class Zona_solucion {
	private Mapa mapa;
	private Itinerario itinerario;
	private JScrollPane scrollPane;
	public Zona_solucion(){
		mapa=new Mapa();
		itinerario=new Itinerario();
		scrollPane = new JScrollPane();
		scrollPane.setBounds(488, 276, 425, 197);
		scrollPane.setViewportView(itinerario.getSolucionesT());
	}
	public Mapa getMapa() {
		return mapa;
	}
	public void setMapa(Mapa mapa) {
		this.mapa = mapa;
	}
	public Itinerario getItinerario() {
		return itinerario;
	}
	public void setItinerario(Itinerario itinerario) {
		this.itinerario = itinerario;
	}
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
}