import javax.swing.*;

public class Zona_Informacion_Barrios {
	private JLabel nombreL;
	private JTextField nombreTF;
	private JLabel paradasL;
	public Zona_Paradas_Barrios zona_Paradas_Barrios;
	public Zona_Informacion_Barrios(){
		zona_Paradas_Barrios=new Zona_Paradas_Barrios();
		
		paradasL = new JLabel("Paradas :");
		paradasL.setBounds(12, 51, 73, 14);
		
		 nombreL = new JLabel("Nombre :");
		nombreL.setBounds(243, 16, 73, 14);
		
		nombreTF=new JTextField();
		nombreTF.setBounds(320, 16, 73, 14);
	}
	public JLabel getNombreL() {
		return nombreL;
	}
	public void setNombreL(JLabel nombreL) {
		this.nombreL = nombreL;
	}
	public JLabel getParadasL() {
		return paradasL;
	}
	public void setParadasL(JLabel paradasL) {
		this.paradasL = paradasL;
	}
	public JTextField getNombreTF() {
		return nombreTF;
	}
	public void setNombreTF(JTextField nombreTF) {
		this.nombreTF = nombreTF;
	}
}