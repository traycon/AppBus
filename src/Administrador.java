import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;

import BD.IAdministrador;

public class Administrador extends Registrado{
	public Barrios__2 barrios;
	public Usuarios usuarios;
	public Informacion_Puntos_Interes_admin Informacion_Puntos_Interes_admin;
	public Zona_descargas_admin Zona_descargas_admin;
	public Informacion_Tarifas_admin Informacion_Tarifas_admin;
	public Informacion_Paradas_admin Informacion_Paradas_admin;
	public Informacion_Eventos_admin Informacion_Eventos_admin;
	public Informacion_Lineas_admin Informacion_Lineas_admin;

	public Administrador(JFrame frame) {
		super(frame);
		Zona_descargas_admin=new Zona_descargas_admin();
		this.tabbedPane.removeTabAt(6);
		this.tabbedPane.insertTab("Descargas", null, Zona_descargas_admin.getDescargasAdmin(), null, 6);
		usuarios=new Usuarios();
		this.tabbedPane.insertTab("Usuarios", null, usuarios.getUsuarios(), null, 5);
		barrios=new Barrios__2();
		this.tabbedPane.insertTab("Barrios", null, barrios.getBarrios(), null, 3);
		Informacion_Tarifas_admin=new Informacion_Tarifas_admin();
		this.tabbedPane.removeTabAt(7);
		this.tabbedPane.insertTab("Tarifas", null, Informacion_Tarifas_admin.getTarifasAdmin(), null, 7);
		Informacion_Puntos_Interes_admin=new Informacion_Puntos_Interes_admin();
		this.tabbedPane.removeTabAt(5);
		this.tabbedPane.insertTab("Puntos de Interes", null, Informacion_Puntos_Interes_admin.getPinteresAdmin(), null, 5);
		Informacion_Eventos_admin=new Informacion_Eventos_admin();
		this.tabbedPane.removeTabAt(4);
		this.tabbedPane.insertTab("Eventos", null, Informacion_Eventos_admin.getEventosAdmin(), null, 4);
		Informacion_Lineas_admin=new Informacion_Lineas_admin();
		this.tabbedPane.removeTabAt(1);
		this.tabbedPane.insertTab("Lineas", null, Informacion_Lineas_admin.getLineasAdmin(), null, 1);
		Informacion_Paradas_admin=new Informacion_Paradas_admin();
		this.tabbedPane.removeTabAt(2);
		this.tabbedPane.insertTab("Paradas", null, Informacion_Paradas_admin.getParadasAdmin(), null, 2);
		
		this.tabbedPane.revalidate();
		this.tabbedPane.repaint();
		
	}
	
	public void cerrarSesion() {
		throw new UnsupportedOperationException();
	}
}