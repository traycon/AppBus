import BD.Barrio;

public class Consulta {
	public Origen origen;
	public Destino destino;
	public idConsulta idconsulta;

	public Consulta() {
		origen = new Origen();
		destino = new Destino();
		idconsulta = new idConsulta();
	}

	public String consultarOrigenCalle() {
		return origen.getCalleOrig().getText();
	}

	public String consultarDestinoCalle() {
		return destino.getCalleDest().getText();
	}

	public String consultarOrigenParada() {
		return origen.getParada().getIndex().getDireccion();
	}

	public String consultarDestinoParada() {
		return destino.getParada().getIndex().getDireccion();
	}

	public Barrio consultarOrigenBarrio() {
		return origen.getBarrio().getIndex();
	}

	public Barrio consultarDestinoBarrio() {
		return destino.getBarrio().getIndex();
	}

	public String consultarDestinoEveCult() {
		return destino.getEvento_cultural_().getIndex().getLugar();
	}

	public void borrarOrigen() {
		origen.getCalleOrig().setText("");
		origen.getParadaOrig().setSelectedIndex(0);
		origen.getBarrioOrig().setSelectedIndex(0);
	}

	public void borrarDestino() {
		destino.getCalleDest().setText("");
		destino.getParadaDest().setSelectedIndex(0);
		destino.getBarrioDest().setSelectedIndex(0);
		destino.getEventoDest().setSelectedIndex(0);
	}
	
	public idConsulta getIdConsulta(){
		return idconsulta;
	}
	
	public void setIdConsulta(idConsulta idConsulta){
		idConsulta = idconsulta;
	}


}