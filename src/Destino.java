import java.awt.SystemColor;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import BD.Barrio;

public class Destino extends Parametros_busqueda {
	private JLabel paramDestinoL;
	private JRadioButton calleDestinRB;
	private JRadioButton paradaDestinoRB;
	private JRadioButton barrioDestinoRB;
	private JRadioButton evtCultDestinoRB;
	private evento_cultural evento_cultural_;
	private JTextField calleDest;
	private JComboBox paradaDest;
	private JComboBox barrioDest;
	private JComboBox eventoDest;
	public ArrayList<Barrio> auxBarrio;
	public Destino(){
		auxBarrio=getBarrio().us;
		calleDest= getCalle().getCallesTF();
		calleDest.setBounds(258, 141, 165, 20);
		calleDest.setColumns(10);
		paradaDest = getParada().getParadasCB();
		paradaDest.setBounds(258, 201, 165, 25);
		barrioDest = getBarrio().getBarriosCB();
		barrioDest.setBounds(258, 266, 165, 25);
		evento_cultural_=new evento_cultural();
		eventoDest=evento_cultural_.getEventosCB();
		eventoDest.setBounds(258, 331, 165, 25);
		this.paramDestinoL=new JLabel("Destino :");
		this.paramDestinoL.setBounds(258, 75, 114, 16);
		this.calleDestinRB = new JRadioButton("Calle :");
		this.calleDestinRB.setBounds(237, 109, 121, 24);
		this.calleDestinRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(calleDestinRB.isSelected()){
					calleDest.setEnabled(true);
					paradaDest.setEnabled(false);
					barrioDest.setEnabled(false);
					eventoDest.setEnabled(false);
					getBarrio().actualizar();
					evento_cultural_.actualizar();
					getParada().actualizar();
				}
			}
		});
		this.paradaDestinoRB= new JRadioButton("Parada :");
		this.paradaDestinoRB.setBounds(237, 169, 121, 24);
		this.paradaDestinoRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(paradaDestinoRB.isSelected()){
					calleDest.setEnabled(false);
					paradaDest.setEnabled(true);
					barrioDest.setEnabled(false);
					eventoDest.setEnabled(false);
					getBarrio().actualizar();
					evento_cultural_.actualizar();
					
					}
			}
		});
		this.barrioDestinoRB = new JRadioButton("Barrio :");
		this.barrioDestinoRB.setBounds(237, 234, 121, 24);	
		this.barrioDestinoRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(barrioDestinoRB.isSelected()){
					calleDest.setEnabled(false);
					paradaDest.setEnabled(false);
					barrioDest.setEnabled(true);
					eventoDest.setEnabled(false);
					evento_cultural_.actualizar();
					getParada().actualizar();
				}
			}
		});
		this.evtCultDestinoRB = new JRadioButton("Evento Cultura :");
		this.evtCultDestinoRB.setBounds(237, 299, 226, 24);
		this.evtCultDestinoRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(evtCultDestinoRB.isSelected()){
					calleDest.setEnabled(false);
					paradaDest.setEnabled(false);
					barrioDest.setEnabled(false);
					eventoDest.setEnabled(true);
					getBarrio().actualizar();
					getParada().actualizar();
					
				}
			}
		});
		ButtonGroup menuRadios=new ButtonGroup();
		menuRadios.add(calleDestinRB);
		menuRadios.add(paradaDestinoRB);
		menuRadios.add(barrioDestinoRB);
		menuRadios.add(evtCultDestinoRB);
		calleDestinRB.setSelected(true);
	}
	public JLabel getParamDestinoL() {
		return paramDestinoL;
	}
	public void setParamDestinoL(JLabel paramDestinoL) {
		this.paramDestinoL = paramDestinoL;
	}
	public JRadioButton getCalleDestinRB() {
		return calleDestinRB;
	}
	public void setCalleDestinRB(JRadioButton calleDestinRB) {
		this.calleDestinRB = calleDestinRB;
	}
	public JRadioButton getParadaDestinoRB() {
		return paradaDestinoRB;
	}
	public void setParadaDestinoRB(JRadioButton paradaDestinoRB) {
		this.paradaDestinoRB = paradaDestinoRB;
	}
	public JRadioButton getBarrioDestinoRB() {
		return barrioDestinoRB;
	}
	public void setBarrioDestinoRB(JRadioButton barrioDestinoRB) {
		this.barrioDestinoRB = barrioDestinoRB;
	}
	public JRadioButton getEvtCultDestinoRB() {
		return evtCultDestinoRB;
	}
	public void setEvtCultDestinoRB(JRadioButton evtCultDestinoRB) {
		this.evtCultDestinoRB = evtCultDestinoRB;
	}
	public evento_cultural getEvento_cultural_() {
		return evento_cultural_;
	}
	public void setEvento_cultural_(evento_cultural evento_cultural_) {
		this.evento_cultural_ = evento_cultural_;
	}
	public JTextField getCalleDest() {
		return calleDest;
	}
	public void setCalleDest(JTextField calleDest) {
		this.calleDest = calleDest;
	}
	public JComboBox getParadaDest() {
		return paradaDest;
	}
	public void setParadaDest(JComboBox paradaDest) {
		this.paradaDest = paradaDest;
	}
	public JComboBox getBarrioDest() {
		return barrioDest;
	}
	public void setBarrioDest(JComboBox barrioDest) {
		this.barrioDest = barrioDest;
	}
	public JComboBox getEventoDest() {
		return eventoDest;
	}
	public void setEventoDest(JComboBox eventoDest) {
		this.eventoDest = eventoDest;
	}
	
}