import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import BD.Linea;
import BD.Parada;

public class Informacion_Paradas {
	public JComboBox paradasCB;
	private JLabel paradasL;
	public Zona_Informacion_Paradas zona_Informacion_Paradas;
	private JPanel paradas;
	ArrayList<Parada> us;
	private static JComboBox index;
	public Informacion_Paradas(){
		zona_Informacion_Paradas=new Zona_Informacion_Paradas();
		paradas=new JPanel();
		paradas.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		paradas.setForeground(Color.CYAN);
		paradas.setLayout(null);
		
		 paradasL = new JLabel("Seleccionar Parada :");
		paradasL.setBounds(33, 29, 132, 16);
		paradas.add(paradasL);
		
		 paradasCB = new JComboBox();
		paradasCB.setBounds(159, 25, 175, 25);
		paradas.add(paradasCB);
		
		paradas.add(zona_Informacion_Paradas.getDireccionL());
		paradas.add(zona_Informacion_Paradas.getDireccionTF());
		paradas.add(zona_Informacion_Paradas.getFotoI());
		paradas.add(zona_Informacion_Paradas.getFotoL());
		paradas.add(zona_Informacion_Paradas.getNombreL());
		paradas.add(zona_Informacion_Paradas.getNombreTF());
		paradas.add(zona_Informacion_Paradas.zona_informacion_EventosParada.getEventosL());
		paradas.add(zona_Informacion_Paradas.zona_informacion_EventosParada.getScrollPaneEventos());
		paradas.add(zona_Informacion_Paradas.zona_informacion_LineasParada.getLineasL());
		paradas.add(zona_Informacion_Paradas.zona_informacion_LineasParada.getScrollPaneLineas());
		paradas.add(zona_Informacion_Paradas.zona_informacion_PuntosInteresParada.getPuntos_interesL());
		paradas.add(zona_Informacion_Paradas.zona_informacion_PuntosInteresParada.getScrollPanePInteres());
		paradasCB.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(paradasCB.getSelectedIndex()>-1 && !us.isEmpty()){
					index=paradasCB;
				actualizarDetallesParada(true);
				}
			}
			
		});
		actualizar();
	}
	protected void actualizarDetallesParada(boolean b) {
		zona_Informacion_Paradas.getDireccionTF().setText(us.get(paradasCB.getSelectedIndex()).getDireccion());
		zona_Informacion_Paradas.getNombreTF().setText(us.get(paradasCB.getSelectedIndex()).getNombre());
		String sourceFoto=us.get(paradasCB.getSelectedIndex()).getDirFoto();
		zona_Informacion_Paradas.setFotoI(new JEditorPane("text/html",
	            "<html><img src='"+sourceFoto+"' width=268height=161></img>"));
		if (b){
		zona_Informacion_Paradas.actualizar(true);
		}
	}
	public void actualizar(){
		int paradaActual = paradasCB.getSelectedIndex();
		paradasCB.removeAllItems();
		try {
			us = MainAplicacion.getBdAdministrador().Cargar_Paradas();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				paradasCB.addItem("Parada "+us.get(i).getORMID());
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		paradasCB.revalidate();
		paradasCB.repaint();
		if(us.size()>paradaActual){
			paradasCB.setSelectedIndex(paradaActual);
			}
		index=paradasCB;
	}
	public static JComboBox getParadasCBClone(){
		return index;
	}
	public JComboBox getParadasCB() {
		return paradasCB;
	}
	public void setParadasCB(JComboBox paradasCB) {
		this.paradasCB = paradasCB;
	}
	public JLabel getParadasL() {
		return paradasL;
	}
	public void setParadasL(JLabel paradasL) {
		this.paradasL = paradasL;
	}
	public JPanel getParadas() {
		return paradas;
	}
	public void setParadas(JPanel paradas) {
		this.paradas = paradas;
	}
}