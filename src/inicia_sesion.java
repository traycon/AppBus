import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;

import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;

import BD.IInvitado;
import BD.IRegistrado;
import BD.Usuario;

public class inicia_sesion {
	private JLabel idTarjetaL;
	private JTextField idTarjetaTF;
	private JLabel passwordL;
	private JPasswordField passwordPF;
	private JButton iniciarSesionB;
	public JFrame ventanaIniSesion;
	private JButton cancelarB;
	private JTabbedPane pestanas;
	private JButton del;
	private JFrame cuadro;

	
	public inicia_sesion(JTabbedPane tabbedPane, JButton btnIniciarSesin, JFrame frame){
		
		pestanas=tabbedPane;
		del=btnIniciarSesin;
		cuadro=frame;
		ventanaIniSesion= new JFrame();
		ventanaIniSesion.setBounds(100, 100, 342, 247);
		ventanaIniSesion.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ventanaIniSesion.getContentPane().setLayout(null);
		ventanaIniSesion.setResizable(false);

		
		idTarjetaTF = new JTextField();
		idTarjetaTF.setBounds(142, 22, 147, 20);
		ventanaIniSesion.getContentPane().add(idTarjetaTF);
		idTarjetaTF.setColumns(10);
		
		passwordPF = new JPasswordField();
		passwordPF.setColumns(10);
		passwordPF.setBounds(142, 72, 147, 20);
		ventanaIniSesion.getContentPane().add(passwordPF);
		
		iniciarSesionB = new JButton("Aceptar");
		iniciarSesionB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iniSesion(idTarjetaTF.getText(),passwordPF.getPassword());
			}
		});
		iniciarSesionB.setBounds(75, 172, 89, 23);
		ventanaIniSesion.getContentPane().add(iniciarSesionB);
		
		cancelarB = new JButton("Cancelar");
		cancelarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaIniSesion.dispose();
			}
		});
		cancelarB.setBounds(191, 172, 89, 23);
		ventanaIniSesion.getContentPane().add(cancelarB);
		
		idTarjetaL = new JLabel("ID Tarjeta");
		idTarjetaL.setBounds(23, 25, 76, 14);
		ventanaIniSesion.getContentPane().add(idTarjetaL);
		
		passwordL = new JLabel("Contrase\u00F1a");
		passwordL.setBounds(22, 75, 77, 14);
		ventanaIniSesion.getContentPane().add(passwordL);
	}
	
	private void iniSesion(String idTarjeta, char[] password) {
		//Verificar si se encuentra en la base de datos
		String pass="";
		for (int i = 0; i < password.length; i++) {
			pass+=password[i];
		}
		Usuario user = null;
		try {
			user = MainAplicacion.getBdInvitado().Iniciar_Sesion(idTarjeta, pass);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(user!=null){
		if(user.getAdministrador()){
			ventanaIniSesion.dispose();
			cuadro.remove(del);
			cuadro.remove(pestanas);
			Registrado.setCurrent(user);
			Administrador admin=new Administrador(cuadro);
			
		}else if(!user.getAdministrador()){
			ventanaIniSesion.dispose();
			cuadro.remove(del);
			cuadro.remove(pestanas);
			Registrado.setCurrent(user);
			Registrado reg=new Registrado(cuadro);
		}
		}
	}
}



	
