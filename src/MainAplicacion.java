import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;

import BD.IAdministrador;
import BD.IInvitado;
import BD.IRegistrado;


public class MainAplicacion {
    static JFrame frame;
	private static IInvitado bdInvitado;
	private static IRegistrado bdRegistrado;
	private static IAdministrador bdAdministrador;
	public MainAplicacion(){
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			if (System.getSecurityManager() == null) {
	            System.setSecurityManager(new SecurityManager());
	        }
            String nombre = "Servidor3";
            String nombre1 = "Servidor2";
            String nombre2 = "Servidor1";
            Registry registry = LocateRegistry.getRegistry(1099);
            bdInvitado = (IInvitado)registry.lookup(nombre);
            bdRegistrado = (IRegistrado)registry.lookup(nombre1);
            bdAdministrador = (IAdministrador)registry.lookup(nombre2);
        } catch (Exception e) {
            System.err.println("Servidor no arrancado:");
            e.printStackTrace();
        }
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.inactiveCaption);
		frame.setBounds(100, 100, 935, 613);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainAplicacion mainInvitado = new MainAplicacion();
					Invitado app=new Invitado(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	public static IInvitado getBdInvitado() {
		return bdInvitado;
	}
	public static  IRegistrado getBdRegistrado() {
		return bdRegistrado;
	}
	public static IAdministrador getBdAdministrador() {
		return bdAdministrador;
	}

}
