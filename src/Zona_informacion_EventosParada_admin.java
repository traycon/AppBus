import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import BD.Evento;

public class Zona_informacion_EventosParada_admin extends Modelo_asignacion {
	public Zona_informacion_EventosParada_admin(){
	
	this.getAgregarB().setBounds(212, 270, 41, 26);
	this.getQuitarB().setBounds(321, 270, 41, 26);
	this.getScrollPaneF().setBounds(212, 159, 150,105);
	this.getScrollPaneT().setBounds(212, 300, 150, 105);
	this.getTablaTotal().setModel(new DefaultTableModel(
			new Object[][] {
					{null},
					{null},
					{null},
					{null},
				},
				new String[] {
					"Eventos"
				}
			));
	for (int c = 0; c < this.getTablaTotal().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaTotal().getColumnClass(c);
	     this.getTablaTotal().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	this.getTablaFiltrada().setModel(new DefaultTableModel(
			new Object[][] {
					{null},{null},{null},{null},{null},{null},{null},
				},
				new String[] {
					"Eventos"
				}
			));
	for (int c = 0; c < this.getTablaFiltrada().getColumnCount(); c++)
	{
	     Class col_class = this.getTablaFiltrada().getColumnClass(c);
	     this.getTablaFiltrada().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
	}
	actualizarTT();
	actualizarTF();
	}
	public int getIndexParada(){
		if(Informacion_Paradas_admin.par.getParadasCB().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas_admin.par.getParadasCB().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	public void actualizarTF() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
			BD.Evento[] eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_EventosParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.length][2];
						for (int i = 0; i < eventos.length; i++) {
									usTable[i][0]=eventos[i].getORMID();
									usTable[i][1]=eventos[i].getNombre();
						}
					}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Evento"}));
		
		for (int c = 0; c <
				this.getTablaFiltrada().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaFiltrada().getColumnClass(c);
			this.getTablaFiltrada().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
		
	}

	public void actualizarTT() {
		Object[][] usTable=new Object[0][2];
		if(getIndexParada()>-1){
			Evento[] eventos = null;
			ArrayList<Evento> event=new ArrayList<Evento>();
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_EventosParada(getIndexParada());
				event=MainAplicacion.getBdAdministrador().Cargar_Eventos();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null&&event.size()>0){
					for (int i = 0; i < eventos.length; i++) {
						int id=buscarIndice(event,eventos[i].getORMID());
						if(id>-1){
						event.remove(id);
						}
					}
					
					usTable=new Object[event.size()][2];
						for (int i = 0; i < event.size(); i++) {
									usTable[i][0]=event.get(i).getORMID();
									usTable[i][1]=event.get(i).getNombre();
						}
					}
		}
		this.getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Evento"}));
		
		for (int c = 0; c <
				this.getTablaTotal().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaTotal().getColumnClass(c);
			this.getTablaTotal().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneT().setViewportView(this.getTablaTotal());
		this.getScrollPaneT().revalidate();
		this.getScrollPaneT().repaint();
		
	}
	private int buscarIndice(ArrayList<Evento> event, int ormid) {
		int aux=-1;
		for (int i = 0; i < event.size(); i++) {
			if(event.get(i).getORMID()==ormid){
				aux=i;
				break;
			}
		}
		return aux;
	}
	@Override
	public void agregar() {
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
try {
	MainAplicacion.getBdAdministrador().agregarEventoParada(Integer.parseInt(tabla.getValueAt(fila, 0).toString()), Informacion_Paradas_admin.ParadaOrmIndex().getORMID());
} catch (NumberFormatException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (RemoteException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
actualizarTF();
actualizarTT();
	}
	@Override
	public void quitar() {
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
try {
	MainAplicacion.getBdAdministrador().quitarEventoParada(Integer.parseInt(tabla.getValueAt(fila, 0).toString()), Informacion_Paradas_admin.ParadaOrmIndex().getORMID());
} catch (NumberFormatException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (RemoteException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
actualizarTF();
actualizarTT();
	}
}