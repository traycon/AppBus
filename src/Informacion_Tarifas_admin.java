import java.awt.Color;
import java.awt.SystemColor;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import BD.IAdministrador;
import BD.Tarifa;

public class Informacion_Tarifas_admin extends Zona_Admin_Botones {
	private JPanel tarifasAdmin;
	private Informacion_Tarifas tar;
	public Informacion_Tarifas_admin(){
		tar=new Informacion_Tarifas();
		tarifasAdmin= tar.getInformacionTarifas();
		tar.actualizar(false);
		tar.zona_informacion_tarifas.getDescripcionTP().setEditable(true);
		tarifasAdmin.add(getA�adirB());
		tarifasAdmin.add(getModificarB());
		tarifasAdmin.add(getBorrarB());
		getA�adirB().setBounds(337+22, 240, 89, 23);
		getModificarB().setBounds(337+121, 240, 89, 23);
		getBorrarB().setBounds(337+220, 240, 89, 23);
		
	}
	public JPanel getTarifasAdmin() {
		return tarifasAdmin;
	}
	public void setTarifasAdmin(JPanel tarifasAdmin) {
		this.tarifasAdmin = tarifasAdmin;
	}
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addTarifa("", 0,false);
			} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		tar.actualizar(false);
		}
	@Override
	public void modificar(){
		JTable tabla = tar.getTarifasTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().modTarifa(tar.us.get(fila).getORMID(), (String)tabla.getValueAt(fila, 0), Double.parseDouble(tabla.getValueAt(fila, 1).toString()), Boolean.parseBoolean(tabla.getValueAt(fila, 2).toString()));
			} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		tar.actualizar(false);
		}
	@Override
	public void borrar(){
		JTable tabla = tar.getTarifasTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().delTarifa(tar.us.get(fila).getORMID());	
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		tar.actualizar(false);
		
	}
}