import java.awt.Color;
import java.awt.SystemColor;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import BD.Tarifa;

public class Informacion_Tarifas {
	private JTable tarifasTable;
	private JLabel tarifasL;
	public Zona_Informacion_Tarifas zona_informacion_tarifas;
	private JPanel informacionTarifas;
	public JScrollPane scrollPane_5;
	public ArrayList<Tarifa> us;
	public Informacion_Tarifas(){
		zona_informacion_tarifas=new Zona_Informacion_Tarifas();
		informacionTarifas= new JPanel();
		informacionTarifas.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		informacionTarifas.setForeground(Color.CYAN);
		informacionTarifas.setLayout(null);
		
		tarifasL = new JLabel("Tarifas :");
		tarifasL.setBounds(12, 12, 55, 16);
		informacionTarifas.add(tarifasL);
		
		scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(12, 40, 313, 313);
		informacionTarifas.add(scrollPane_5);
		actualizar(true);
		informacionTarifas.add(zona_informacion_tarifas.getDescripcionL());
		informacionTarifas.add(zona_informacion_tarifas.getDescripcionTP());
	}
	protected void actualizar(boolean b) {
		Object[][] desTable = new Object[0][3];
		try {
			us = MainAplicacion.getBdInvitado().Cargar_Tarifas();
			if (us != null) {
				desTable = new Object[us.size()][3];
				for (int i = 0; i < us.size(); i++) {
					desTable[i][0] = us.get(i).getTipoTarifa();
					desTable[i][1] = us.get(i).getPrecio();
					desTable[i][2] = us.get(i).getBono();
				}
				
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		tarifasTable = new JTable();
		tarifasTable.setModel(new DefaultTableModel(desTable, new String[] {
				"Tipo Tarifa", "Precio", "Bono" }));
		if (b){
		for (int c = 0; c <
				 tarifasTable.getColumnCount(); c++) 
		{
			Class col_class =
			tarifasTable.getColumnClass(c);
			tarifasTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}
		tarifasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_5.setViewportView(tarifasTable);
		scrollPane_5.revalidate();
		scrollPane_5.repaint();
	}
	public JTable getTarifasTable() {
		return tarifasTable;
	}
	public void setTarifasTable(JTable tarifasTable) {
		this.tarifasTable = tarifasTable;
	}
	public JLabel getTarifasL() {
		return tarifasL;
	}
	public void setTarifasL(JLabel tarifasL) {
		this.tarifasL = tarifasL;
	}
	public JPanel getInformacionTarifas() {
		return informacionTarifas;
	}
	public void setInformacionTarifas(JPanel informacionTarifas) {
		this.informacionTarifas = informacionTarifas;
	}
}