package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Parada_EventoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression contienen_eventosId;
	public final AssociationExpression contienen_eventos;
	public final IntegerExpression eventoIdEventosId;
	public final AssociationExpression eventoIdEventos;
	public final IntegerExpression ID;
	
	public Parada_EventoDetachedCriteria() {
		super(Parada_Evento.class, Parada_EventoCriteria.class);
		contienen_eventosId = new IntegerExpression("contienen_eventos.idParadas", this.getDetachedCriteria());
		contienen_eventos = new AssociationExpression("contienen_eventos", this.getDetachedCriteria());
		eventoIdEventosId = new IntegerExpression("eventoIdEventos.idEventos", this.getDetachedCriteria());
		eventoIdEventos = new AssociationExpression("eventoIdEventos", this.getDetachedCriteria());
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
	}
	
	public Parada_EventoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, Parada_EventoCriteria.class);
		contienen_eventosId = new IntegerExpression("contienen_eventos.idParadas", this.getDetachedCriteria());
		contienen_eventos = new AssociationExpression("contienen_eventos", this.getDetachedCriteria());
		eventoIdEventosId = new IntegerExpression("eventoIdEventos.idEventos", this.getDetachedCriteria());
		eventoIdEventos = new AssociationExpression("eventoIdEventos", this.getDetachedCriteria());
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
	}
	
	public ParadaDetachedCriteria createContienen_eventosCriteria() {
		return new ParadaDetachedCriteria(createCriteria("contienen_eventos"));
	}
	
	public EventoDetachedCriteria createEventoIdEventosCriteria() {
		return new EventoDetachedCriteria(createCriteria("eventoIdEventos"));
	}
	
	public Parada_Evento uniqueParada_Evento(PersistentSession session) {
		return (Parada_Evento) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Parada_Evento[] listParada_Evento(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Parada_Evento[]) list.toArray(new Parada_Evento[list.size()]);
	}
}

