package BD;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
public class BD_Principal implements IRegistrado, IInvitado, IAdministrador {
	
	public BD_Calle _bD_Calle= new BD_Calle();
	public BD_Barrio _bD_Barrio=new BD_Barrio();
	public BD_Historial_Solucion _bD_Historial_Solucion=new BD_Historial_Solucion();
	public BD_Tarifa _bD_Tarifa=new BD_Tarifa();
	public Bd_Usuario _bd_Usuario=new Bd_Usuario();
	public BD_Descarga _bD_Descarga=new BD_Descarga();
	public BD_Parada _bD_Parada=new BD_Parada();
	public BD_Linea _bD_Linea=new BD_Linea();
	public BD_Punto_Interes _bD_Punto_Interes=new BD_Punto_Interes();
	public BD_Evento _bD_Evento=new BD_Evento();

	public static void main(String[] args)
	{
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
		try {
            String nombre1 = "Servidor1";
            String nombre2 = "Servidor2";
            String nombre3 = "Servidor3";
            LocateRegistry.createRegistry(1099);
            BD_Principal servidor = new BD_Principal();
            Remote servicio = UnicastRemoteObject.exportObject(servidor, 0);
            IAdministrador administrador =
                    (IAdministrador) servicio;
            IRegistrado registrado =
                    (IRegistrado) servicio;
            IInvitado invitado =
                    (IInvitado) servicio;
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(nombre1, administrador);
            registry.rebind(nombre2, registrado);
            registry.rebind(nombre3, invitado);
            System.out.println("Servidor Arrancado");
        } catch (Exception e) {
            System.err.println("Problema con el servidor");
            e.printStackTrace();
        }
		
	};
	
	public ArrayList<Descarga> Cargar_descargas() {
		
		try {
			return _bD_Descarga.Cargar_descargas();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Descarga descargar(int aIdDescargas) {
		try {
			return _bD_Descarga.descargar(aIdDescargas);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addDescarga(String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación) {
		try {
			_bD_Descarga.addDescarga(aNombre, aDescripcion, aTamanio, aEstado, aVersion, aFechaSubida, aUltimaModificación);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modDescarga(int aIdDescargas, String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación) {
		try {
			_bD_Descarga.modDescarga(aIdDescargas, aNombre, aDescripcion, aTamanio, aEstado, aVersion, aFechaSubida, aUltimaModificación);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delDescarga(int aIdDescargas) {
		try {
			_bD_Descarga.delDescarga(aIdDescargas);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Usuario Cargar_Configuracion(int aIdUsuario) {
		try {
			return _bd_Usuario.Cargar_Configuracion(aIdUsuario);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void GuardarConfig(int aIdUsuario, String oldPass,String newPass) {
		try {
			_bd_Usuario.GuardarConfig(aIdUsuario, oldPass, newPass);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Usuario Iniciar_Sesion(String aUsuario, String aContrasena) {
		try {
			return _bd_Usuario.Iniciar_Sesion(aUsuario, aContrasena);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Usuario> Cargar_Usuarios() {
		try {
			return _bd_Usuario.Cargar_Usuarios();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addUsuario(String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador) {
		try {
			_bd_Usuario.addUsuario(aNombre, aApellidos, aCorreo, aPassword, aTelefono, aDireccion, aCp, aIdBono, aValidezBono, aAdministrador);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modUsuario(int aIdUsuario, String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador) {
		try {
			_bd_Usuario.modUsuario(aIdUsuario, aNombre, aApellidos, aCorreo, aPassword, aTelefono, aDireccion, aCp, aIdBono, aValidezBono, aAdministrador);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void delUsuario(int aIdUsuario) {
		try {
			_bd_Usuario.delUsuario(aIdUsuario);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Recargar(String aIdBono, double aCantidad) {
		try {
			_bd_Usuario.Recargar(aIdBono, aCantidad);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cambiarValidezBono(int aIdUsuario, Date aFecha) {
		try {
			_bd_Usuario.cambiarValidezBono(aIdUsuario, aFecha);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Tarifa> Cargar_Tarifas() {
		try {
			return _bD_Tarifa.Cargar_Tarifas();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addTarifa(String aTipoTarifa, double aPrecio, boolean aBono) {
		try {
			_bD_Tarifa.addTarifa(aTipoTarifa, aPrecio, aBono);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modTarifa(int aId, String aTipoTarifa, double aPrecio, boolean aBono) {
		try {
			_bD_Tarifa.modTarifa(aId, aTipoTarifa, aPrecio, aBono);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delTarifa(int aIdTarifa) {
		try {
			_bD_Tarifa.delTarifa(aIdTarifa);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Historial_Solucion> Cargar_Historial(int aIdUsuario) {
		try {
			return _bD_Historial_Solucion.Cargar_Historial(aIdUsuario);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addHistorial(String aOrigen, String aDestino, int idUsuario) {
		try {
			_bD_Historial_Solucion.addHistorial(aOrigen, aDestino, idUsuario);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String compartir(int aId) {
		try {
			return _bD_Historial_Solucion.compartir(aId);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Historial_Solucion cargar_consulta(String aCodigo) {
		try {
			return _bD_Historial_Solucion.cargar_consulta(aCodigo);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Barrio> cargar_Barrios() {
		try {
			return _bD_Barrio.cargar_Barrios();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void modBarrio(int aIdBarrio, String aNombre) {
		try {
			_bD_Barrio.modBarrio(aIdBarrio, aNombre);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addBarrio(String aNombre) {
		try {
			_bD_Barrio.addBarrio(aNombre);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delBarrio(int aIdBarrio) {
		try {
			_bD_Barrio.delBarrio(aIdBarrio);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Barrio cargar_Barrio(int aIdBarrio) {
		try {
			return _bD_Barrio.cargar_Barrio(aIdBarrio);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void agregarParadas_Barrio(int aIdBarrio, int aIdParada) {
		try {
			_bD_Barrio.agregarParadas_Barrio(aIdBarrio, aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarParadas_Barrio(int aIdParada) {
		try {
			_bD_Barrio.quitarParadas_Barrio(aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Calle> cargar_calles(int aIdBarrio) {
		try {
			return _bD_Calle.cargar_calles(aIdBarrio);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addCalle(String aDireccion, int aBarrio) {
		try {
			_bD_Calle.addCalle(aDireccion, aBarrio);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modCalle(int aIdCalle, String aDireccion) {
		try {
			_bD_Calle.modCalle(aIdCalle, aDireccion);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delCalle(int aIdCalle) {
		try {
			_bD_Calle.delCalle(aIdCalle);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modParada(int aIdParada, String aNombre, String aDireccion, String aDirFoto) {
		try {
			_bD_Parada.modParada(aIdParada, aNombre, aDireccion, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addParada(String aNombre, String aDireccion, String aDirFoto) {
		try {
			_bD_Parada.addParada(aNombre, aDireccion, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void agregarEventoParada(int aIdEvento, int aIdParada) {
		try {
			_bD_Parada.agregarEventoParada(aIdEvento, aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarEventoParada(int aIdEvento, int aIdParada) {
		try {
			_bD_Parada.quitarEventoParada(aIdEvento, aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Parada> Cargar_Paradas() {
		try {
			return _bD_Parada.Cargar_Paradas();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void delParada(int aIdParada) {
		try {
			_bD_Parada.delParada(aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Parada Cargar_Parada(int aIdParada) {
		try {
			return _bD_Parada.Cargar_Parada(aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void subirFoto(int aIdParada, String aDirFoto) {
		try {
			_bD_Parada.subirFoto(aIdParada, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void agregarLineasParada(int aIdParada, int aIdLinea, Recorrido aRecorrido) {
		try {
			_bD_Parada.agregarLineasParada(aIdParada, aIdLinea, aRecorrido);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarLineasParada(int aIdParada, int aIdLinea) {
		try {
			_bD_Parada.quitarLineasParada(aIdParada, aIdLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void agregarPuntosInteresParada(int aIdPuntoInteres, int aIdParada) {
		try {
			_bD_Parada.agregarPuntosInteresParada(aIdPuntoInteres, aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarPuntosInteresParada(int aIdPuntoInteres, int aIdParada) {
		try {
			_bD_Parada.quitarPuntosInteresParada(aIdPuntoInteres, aIdParada);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Linea> Cargar_lineas() {
		try {
			return _bD_Linea.Cargar_lineas();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addLinea(String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa) {
		try {
			_bD_Linea.addLinea(aNombre, aNumero, aParadaIni, aFechaIni, aNombreParadaIni, aParadaFin, aFechaFin, aNombreParadaFin, aObservaciones, aTarifa, aDirMapa);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modLinea(int aIdLinea, String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa) {
		try {
			_bD_Linea.modLinea(aIdLinea, aNombre, aNumero, aParadaIni, aFechaIni, aNombreParadaIni, aParadaFin, aFechaFin, aNombreParadaFin, aObservaciones, aTarifa, aDirMapa);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delLinea(int aIdLinea) {
		try {
			_bD_Linea.delLinea(aIdLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Linea Cargar_linea(int aIdLinea) {
		try {
			return _bD_Linea.Cargar_linea(aIdLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addMapa(String aDirMapa, int aIdLinea) {
		try {
			_bD_Linea.addMapa(aIdLinea, aDirMapa);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void agregarParadaLinea(int aIdParada, int aIdLinea, Recorrido aRecorrido) {
		try {
			_bD_Linea.agregarParadaLinea(aIdParada, aIdLinea, aRecorrido);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarParadaLinea(int aIdParada, int aIdLinea) {
		try {
			_bD_Linea.quitarParadaLinea(aIdParada, aIdLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Punto_Interes> Cargar_PuntosInteres() {
		try {
			return _bD_Punto_Interes.Cargar_PuntosInteres();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addPunto_Interes(String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto) {
		try {
			_bD_Punto_Interes.addPunto_Interes(aNombre, aLugar, aPrecio, aHoraIni, aHoraFin, aDescripcion, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modPunto_Interes(int aIdPuntoInteres, String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto) {
		try {
			_bD_Punto_Interes.modPunto_Interes(aIdPuntoInteres, aNombre, aLugar, aPrecio, aHoraIni, aHoraFin, aDescripcion, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delPunto_Interes(int aIdPuntoInteres) {
		try {
			_bD_Punto_Interes.delPunto_Interes(aIdPuntoInteres);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Punto_Interes Cargar_PuntosInteres(int aIdPuntoInteres) {
		try {
			return _bD_Punto_Interes.Cargar_PuntosInteres(aIdPuntoInteres);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void subirFoto2(int aIdPuntoInteres, String aDirFoto) {
		try {
			_bD_Punto_Interes.subirFoto(aIdPuntoInteres, aDirFoto);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void agregarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres) {
		try {
			_bD_Punto_Interes.agregarEventoPunto_Interes(aIdEvento, aIdPuntoInteres);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres) {
		try {
			_bD_Punto_Interes.quitarEventoPunto_Interes(aIdEvento, aIdPuntoInteres);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Evento> Cargar_Eventos() {
		try {
			return _bD_Evento.Cargar_Eventos();
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void addEvento(String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel) {
		try {
			_bD_Evento.addEvento(aNombre, aLugar, aFechaIni, aFechaFin, aDescripcion, aDirCartel);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void modEvento(int aIdEvento, String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel) {
		try {
			_bD_Evento.modEvento(aIdEvento, aNombre, aLugar, aFechaIni, aFechaFin, aDescripcion, aDirCartel);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void delEvento(int aIdEvento) {
		try {
			_bD_Evento.delEvento(aIdEvento);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Evento Cargar_Evento(int aIdEvento) {
		try {
			return _bD_Evento.Cargar_Evento(aIdEvento);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void agregarParada_LineaEvento(int aIdEvento, int aIdParadaLinea) {
		try {
			_bD_Evento.agregarParada_LineaEvento(aIdEvento, aIdParadaLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void quitarParada_LineaEvento(int aIdEvento, int aIdParadaLinea) {
		try {
			_bD_Evento.quitarParada_LineaEvento(aIdEvento, aIdParadaLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Parada[] cargar_ParadaLinea(int ormid) throws RemoteException {
				try {
					return _bD_Linea.cargar_ParadaLinea(ormid);
				} catch (PersistentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
	}

	public Parada[] cargar_ParadasEvento(int ormid) throws RemoteException {
		try {
			return _bD_Evento.cargar_ParadasEvento(ormid);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<Linea> cargar_LineasParada(int ormid) throws RemoteException {
		try {
			return _bD_Parada.cargar_LineasParada(ormid);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Evento[] cargar_Eventos_PuntosInteres(int ormid) throws RemoteException{
	return _bD_Punto_Interes.cargar_Eventos_PuntosInteres(ormid);
	}

	public Evento[] cargar_EventosParada(int ormid) throws RemoteException {
		try {
			return _bD_Parada.cargar_EventosParada(ormid);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Punto_Interes[] cargar_PuntosInteresParada(int ormid) throws RemoteException{
			try {
				return _bD_Parada.cargar_PuntosInteresParada(ormid);
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}
	public Recorrido recorridoParadaLinea(int idParada,Linea aLinea) throws RemoteException{
		try {
			return _bD_Linea.recorridoParadaLinea(idParada, aLinea);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}