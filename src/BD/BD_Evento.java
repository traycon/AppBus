package BD;

import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Evento {
	public BD_Principal _bd_prin_eve;
	public ArrayList<Evento> _cont_eventos = new ArrayList<Evento>();

	public ArrayList<Evento> Cargar_Eventos() throws PersistentException {
		ArrayList<Evento> eventos=new ArrayList<Evento>();
		 Evento[] aux=null;

			 aux = EventoDAO.listEventoByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				eventos.add(aux[i]);
			}
		
		return eventos;
	}

	public void addEvento(String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.createEvento();
			as.setNombre(aNombre);
			as.setLugar(aLugar);
			as.setFechaIni(aFechaIni);
			as.setFechaFin(aFechaFin);
			as.setDescripcion(aDescripcion);
			as.setDirCartel(aDirCartel);
			EventoDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modEvento(int aIdEvento, String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.getEventoByORMID(aIdEvento);
			as.setNombre(aNombre);
			as.setLugar(aLugar);
			as.setFechaIni(aFechaIni);
			as.setFechaFin(aFechaFin);
			as.setDescripcion(aDescripcion);
			as.setDirCartel(aDirCartel);
			EventoDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delEvento(int aIdEvento) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.getEventoByORMID(aIdEvento);
			EventoDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Evento Cargar_Evento(int aIdEvento) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Evento as=null;
		try {
			 
			as = EventoDAO.getEventoByORMID(aIdEvento);
		}
		catch (Exception e) {
			t.rollback();
		}
		
		return as;
	}

	public void subirFoto(int aIdEvento, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.getEventoByORMID(aIdEvento);
			as.setDirCartel(aDirFoto);
			EventoDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarParada_LineaEvento(int aIdEvento, int aIdParadaLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.getEventoByORMID(aIdEvento);
			Parada parada=ParadaDAO.getParadaByORMID(aIdParadaLinea);
			Parada_Evento pev=Parada_EventoDAO.createParada_Evento();
			pev.setContienen_eventos(parada);
			pev.setEventoIdEventos(as);
			Parada_EventoDAO.save(pev);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarParada_LineaEvento(int aIdEvento, int aIdParadaLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada_Evento[] pev=Parada_EventoDAO.listParada_EventoByQuery(null, null);
			for (int i = 0; i < pev.length; i++) {
				if(aIdParadaLinea==pev[i].getContienen_eventos().getORMID()
						&& aIdEvento==pev[i].getEventoIdEventos().getORMID()){
					Parada_EventoDAO.deleteAndDissociate(pev[i]);
				}
			}
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Parada[] cargar_ParadasEvento(int ormid) throws PersistentException {
		ArrayList<Parada> pa=new ArrayList<Parada>();
		Parada_Evento[] pev=Parada_EventoDAO.listParada_EventoByQuery(null, null);
		for (int i = 0; i < pev.length; i++) {
			if(ormid==pev[i].getEventoIdEventos().getORMID()){
				pa.add(pev[i].getContienen_eventos());
			}
		}
		Parada[] as=new Parada[pa.size()];
		for (int i = 0; i < pa.size(); i++) {
			as[i]=pa.get(i);
		}
		return as;
	}
}