package BD;

import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Linea {
	public BD_Principal _bd_prin_lin;
	public ArrayList<Linea> _cont_lineas = new ArrayList<Linea>();

	public ArrayList<Linea> Cargar_lineas() throws PersistentException {
		ArrayList<Linea> lineas=new ArrayList<Linea>();
		 Linea[] aux=null;
		
			 aux = LineaDAO.listLineaByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				lineas.add(aux[i]);
			}
		
		return lineas;
	}

	public void addLinea(String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.createLinea();
			as.setNombre(aNombre);
			as.setNumero(aNumero);
			as.setParadaIni(aParadaIni);
			as.setFechaIni(aFechaIni);
			as.setNombreParadaIni(aNombreParadaIni);
			as.setParadaFin(aParadaFin);
			as.setFechaFin(aFechaFin);
			as.setNombreParadFin(aNombreParadaFin);
			as.setObservaciones(aObservaciones);
			as.setTarifa(aTarifa);
			as.setDirMapa(aDirMapa);
			LineaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modLinea(int aIdLinea, String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			as.setNombre(aNombre);
			as.setNumero(aNumero);
			as.setParadaIni(aParadaIni);
			as.setFechaIni(aFechaIni);
			as.setNombreParadaIni(aNombreParadaIni);
			as.setParadaFin(aParadaFin);
			as.setFechaFin(aFechaFin);
			as.setNombreParadFin(aNombreParadaFin);
			as.setObservaciones(aObservaciones);
			as.setTarifa(aTarifa);
			as.setDirMapa(aDirMapa);
			LineaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delLinea(int aIdLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			LineaDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Linea Cargar_linea(int aIdLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Linea as=null;
		try {
			 as=LineaDAO.getLineaByORMID(aIdLinea);
		}
		catch (Exception e) {
			t.rollback();
		}	
		
		return as;
	}

	public void addMapa(int aIdLinea,String aDirMapa) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			as.setDirMapa(aDirMapa);
			LineaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarParadaLinea(int aIdParada, int aIdLinea, Recorrido aRecorrido) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
		/**	Recorrido re=RecorridoDAO.createRecorrido();
			re.setLinea(as);
			re.setParada(aParada);**/
			as.addParada(aRecorrido, aParada);
			LineaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarParadaLinea(int aIdParada, int aIdLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
			Recorrido re=aParada.getRecorridoByLinea(as);
			RecorridoDAO.deleteAndDissociate(re);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Parada[] cargar_ParadaLinea(int ormid) throws PersistentException {
			Linea as=LineaDAO.getLineaByORMID(ormid);
			Recorrido[] re=RecorridoDAO.listRecorridoByQuery(null, "ID");
			Parada[] pa=new Parada[as.getParadas().length];
			int count=0;
			for (int i = 0; i < re.length; i++) {
				if(re[i].getLinea().getORMID()==ormid){
					pa[count]=re[i].getParada();
					count++;
				}
			}
			return pa;
	}
	public Recorrido recorridoParadaLinea(int idParada,Linea aLinea) throws PersistentException{
		Parada a= ParadaDAO.getParadaByORMID(idParada);
		return a.getRecorridoByLinea(aLinea);
	}
}