package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class BarrioDAO {
	public static Barrio loadBarrioByORMID(int idBarrio) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadBarrioByORMID(session, idBarrio);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio getBarrioByORMID(int idBarrio) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getBarrioByORMID(session, idBarrio);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByORMID(int idBarrio, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadBarrioByORMID(session, idBarrio, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio getBarrioByORMID(int idBarrio, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getBarrioByORMID(session, idBarrio, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByORMID(PersistentSession session, int idBarrio) throws PersistentException {
		try {
			return (Barrio) session.load(Barrio.class, new Integer(idBarrio));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio getBarrioByORMID(PersistentSession session, int idBarrio) throws PersistentException {
		try {
			return (Barrio) session.get(Barrio.class, new Integer(idBarrio));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByORMID(PersistentSession session, int idBarrio, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Barrio) session.load(Barrio.class, new Integer(idBarrio), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio getBarrioByORMID(PersistentSession session, int idBarrio, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Barrio) session.get(Barrio.class, new Integer(idBarrio), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBarrio(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryBarrio(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBarrio(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryBarrio(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio[] listBarrioByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listBarrioByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio[] listBarrioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listBarrioByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBarrio(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Barrio as Barrio");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryBarrio(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Barrio as Barrio");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Barrio", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio[] listBarrioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryBarrio(session, condition, orderBy);
			return (Barrio[]) list.toArray(new Barrio[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio[] listBarrioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryBarrio(session, condition, orderBy, lockMode);
			return (Barrio[]) list.toArray(new Barrio[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadBarrioByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadBarrioByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Barrio[] barrios = listBarrioByQuery(session, condition, orderBy);
		if (barrios != null && barrios.length > 0)
			return barrios[0];
		else
			return null;
	}
	
	public static Barrio loadBarrioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Barrio[] barrios = listBarrioByQuery(session, condition, orderBy, lockMode);
		if (barrios != null && barrios.length > 0)
			return barrios[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateBarrioByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateBarrioByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBarrioByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateBarrioByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBarrioByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Barrio as Barrio");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateBarrioByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Barrio as Barrio");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Barrio", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio createBarrio() {
		return new Barrio();
	}
	
	public static boolean save(Barrio barrio) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(barrio);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Barrio barrio) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(barrio);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Barrio barrio)throws PersistentException {
		try {
			Parada[] lContiene_barrioss = barrio.contiene_barrios.toArray();
			for(int i = 0; i < lContiene_barrioss.length; i++) {
				lContiene_barrioss[i].setEs_contenida_paradas(null);
			}
			Calle[] lContienes = barrio.contiene.toArray();
			for(int i = 0; i < lContienes.length; i++) {
				lContienes[i].setEs_contenida(null);
			}
			return delete(barrio);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Barrio barrio, org.orm.PersistentSession session)throws PersistentException {
		try {
			Parada[] lContiene_barrioss = barrio.contiene_barrios.toArray();
			for(int i = 0; i < lContiene_barrioss.length; i++) {
				lContiene_barrioss[i].setEs_contenida_paradas(null);
			}
			Calle[] lContienes = barrio.contiene.toArray();
			for(int i = 0; i < lContienes.length; i++) {
				lContienes[i].setEs_contenida(null);
			}
			try {
				session.delete(barrio);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Barrio barrio) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(barrio);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Barrio barrio) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(barrio);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Barrio loadBarrioByCriteria(BarrioCriteria barrioCriteria) {
		Barrio[] barrios = listBarrioByCriteria(barrioCriteria);
		if(barrios == null || barrios.length == 0) {
			return null;
		}
		return barrios[0];
	}
	
	public static Barrio[] listBarrioByCriteria(BarrioCriteria barrioCriteria) {
		return barrioCriteria.listBarrio();
	}
}
