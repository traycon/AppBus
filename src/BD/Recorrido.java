package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Recorrido")
public class Recorrido implements Serializable {
	public Recorrido() {
	}
	
	public Recorrido(int miOrden, java.util.Date miHora) {
		this.setHora(miHora);
		this.setOrden(miOrden);
	}

	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_RECORRIDO_PARADA) {
			this.parada = (Parada) owner;
		}
		
		else if (key == ORMConstants.KEY_RECORRIDO_LINEA) {
			this.linea = (Linea) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="ID", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="RECORRIDO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="RECORRIDO_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@Column(name="Hora", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date hora;
	
	@Column(name="Orden", nullable=false, length=11)	
	private int orden;
	
	@ManyToOne(targetEntity=Linea.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="LineaIdLinea", referencedColumnName="IdLinea", nullable=false) })	
	private Linea linea;
	
	@ManyToOne(targetEntity=Parada.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="ParadaIdParadas", referencedColumnName="IdParadas", nullable=false) })	
	private Parada parada;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setHora(java.util.Date value) {
		this.hora = value;
	}
	
	public java.util.Date getHora() {
		return hora;
	}
	
	public void setOrden(int value) {
		this.orden = value;
	}
	
	public int getOrden() {
		return orden;
	}
	
	public void setParada(Parada value) {
		if (parada != null) {
			parada.recorridos.remove(this);
		}
		if (value != null) {
			value.recorridos.add(this);
		}
	}
	
	public Parada getParada() {
		return parada;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Parada(Parada value) {
		this.parada = value;
	}
	
	private Parada getORM_Parada() {
		return parada;
	}
	
	public void setLinea(Linea value) {
		if (linea != null) {
			linea.recorridos.remove(this);
		}
		if (value != null) {
			value.recorridos.add(this);
		}
	}
	
	public Linea getLinea() {
		return linea;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Linea(Linea value) {
		this.linea = value;
	}
	
	private Linea getORM_Linea() {
		return linea;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
