package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UsuarioDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idUsuario;
	public final StringExpression nombre;
	public final StringExpression apellidos;
	public final StringExpression correo;
	public final StringExpression password;
	public final IntegerExpression telefono;
	public final StringExpression direccion;
	public final IntegerExpression cp;
	public final StringExpression idBono;
	public final DateExpression validezBono;
	public final BooleanExpression administrador;
	public final CollectionExpression contiene_usuario;
	public final IntegerExpression esta_vinculadoId;
	public final AssociationExpression esta_vinculado;
	
	public UsuarioDetachedCriteria() {
		super(Usuario.class, UsuarioCriteria.class);
		idUsuario = new IntegerExpression("idUsuario", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellidos = new StringExpression("apellidos", this.getDetachedCriteria());
		correo = new StringExpression("correo", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		telefono = new IntegerExpression("telefono", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
		cp = new IntegerExpression("cp", this.getDetachedCriteria());
		idBono = new StringExpression("idBono", this.getDetachedCriteria());
		validezBono = new DateExpression("validezBono", this.getDetachedCriteria());
		administrador = new BooleanExpression("administrador", this.getDetachedCriteria());
		contiene_usuario = new CollectionExpression("ORM_contiene_usuario", this.getDetachedCriteria());
		esta_vinculadoId = new IntegerExpression("esta_vinculado.idUsuario", this.getDetachedCriteria());
		esta_vinculado = new AssociationExpression("esta_vinculado", this.getDetachedCriteria());
	}
	
	public UsuarioDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, UsuarioCriteria.class);
		idUsuario = new IntegerExpression("idUsuario", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellidos = new StringExpression("apellidos", this.getDetachedCriteria());
		correo = new StringExpression("correo", this.getDetachedCriteria());
		password = new StringExpression("password", this.getDetachedCriteria());
		telefono = new IntegerExpression("telefono", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
		cp = new IntegerExpression("cp", this.getDetachedCriteria());
		idBono = new StringExpression("idBono", this.getDetachedCriteria());
		validezBono = new DateExpression("validezBono", this.getDetachedCriteria());
		administrador = new BooleanExpression("administrador", this.getDetachedCriteria());
		contiene_usuario = new CollectionExpression("ORM_contiene_usuario", this.getDetachedCriteria());
		esta_vinculadoId = new IntegerExpression("esta_vinculado.idUsuario", this.getDetachedCriteria());
		esta_vinculado = new AssociationExpression("esta_vinculado", this.getDetachedCriteria());
	}
	
	public Historial_SolucionDetachedCriteria createContiene_usuarioCriteria() {
		return new Historial_SolucionDetachedCriteria(createCriteria("ORM_contiene_usuario"));
	}
	
	public TarifaDetachedCriteria createEsta_vinculadoCriteria() {
		return new TarifaDetachedCriteria(createCriteria("esta_vinculado"));
	}
	
	public Usuario uniqueUsuario(PersistentSession session) {
		return (Usuario) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Usuario[] listUsuario(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Usuario[]) list.toArray(new Usuario[list.size()]);
	}
}

