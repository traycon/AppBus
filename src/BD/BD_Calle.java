package BD;

import java.util.ArrayList;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Calle {
	public BD_Principal _bd_prin_cal;
	public ArrayList<Calle> _cont_calles = new ArrayList<Calle>();

	public ArrayList<Calle> cargar_calles(int aIdBarrio ) throws PersistentException {
		ArrayList<Calle> calles=new ArrayList<Calle>();
		 Calle[] aux=null;
			 aux = CalleDAO.listCalleByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 if (aux[i].getEs_contenida().getORMID()==aIdBarrio){
				calles.add(aux[i]);
				 }
			}

		
		return calles;
	}

	public void addCalle(String aDireccion, int aBarrio) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 Barrio b=BarrioDAO.getBarrioByORMID(aBarrio);
			Calle as = CalleDAO.createCalle();
			as.setDireccion(aDireccion);
			as.setEs_contenida(b);
			CalleDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modCalle(int aIdCalle, String aDireccion) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Calle as = CalleDAO.getCalleByORMID(aIdCalle);
			as.setDireccion(aDireccion);
			CalleDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delCalle(int aIdCalle) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Calle as = CalleDAO.getCalleByORMID(aIdCalle);
			CalleDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
}