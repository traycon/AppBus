package BD;

import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Descarga {
	public BD_Principal _bd_prin_des;
	public ArrayList<Descarga> _cont_descargas = new ArrayList<Descarga>();

	public ArrayList<Descarga> Cargar_descargas() throws PersistentException {
		ArrayList<Descarga> descarga=new ArrayList<Descarga>();
		Descarga[] aux=null;
			 aux = DescargaDAO.listDescargaByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 descarga.add(aux[i]);
			}
		return descarga;
	}

	public Descarga descargar(int aIdDescargas) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Descarga as=null;
		try {
			 as=DescargaDAO.getDescargaByORMID(aIdDescargas);
		}
		catch (Exception e) {
			t.rollback();
		}	
		
		return as;
	}

	public void addDescarga(String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Descarga as = DescargaDAO.createDescarga();
			as.setNombre(aNombre);
			as.setDescripcion(aDescripcion);
			as.setTamanio(aTamanio);
			as.setEstado(aEstado);
			as.setVersion(aVersion);
			as.setFechaSubida(aFechaSubida);
			as.setUltimaModificacion(aUltimaModificación);
			DescargaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modDescarga(int aIdDescargas, String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Descarga as = DescargaDAO.getDescargaByORMID(aIdDescargas);
			as.setNombre(aNombre);
			as.setDescripcion(aDescripcion);
			as.setTamanio(aTamanio);
			as.setEstado(aEstado);
			as.setVersion(aVersion);
			as.setFechaSubida(aFechaSubida);
			as.setUltimaModificacion(aUltimaModificación);
			DescargaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delDescarga(int aIdDescargas) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Descarga as = DescargaDAO.getDescargaByORMID(aIdDescargas);
			DescargaDAO.delete(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
}