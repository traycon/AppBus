package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Tarifa")
public class Tarifa implements Serializable {
	public Tarifa() {
	}
	
	@Column(name="ID", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="TARIFA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="TARIFA_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@OneToOne(targetEntity=Usuario.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="UsuarioIdUsuario", referencedColumnName="IdUsuario") })	
	private Usuario vinculada_a;
	
	@Column(name="TipoTarifa", nullable=true, length=255)	
	private String tipoTarifa;
	
	@Column(name="Precio", nullable=false)	
	private double precio;
	
	@Column(name="Bono", nullable=false, length=1)	
	private boolean bono;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setTipoTarifa(String value) {
		this.tipoTarifa = value;
	}
	
	public String getTipoTarifa() {
		return tipoTarifa;
	}
	
	public void setPrecio(double value) {
		this.precio = value;
	}
	
	public double getPrecio() {
		return precio;
	}
	
	public void setBono(boolean value) {
		this.bono = value;
	}
	
	public boolean getBono() {
		return bono;
	}
	
	public void setVinculada_a(Usuario value) {
		if (this.vinculada_a != value) {
			Usuario lvinculada_a = this.vinculada_a;
			this.vinculada_a = value;
			if (value != null) {
				vinculada_a.setEsta_vinculado(this);
			}
			if (lvinculada_a != null && lvinculada_a.getEsta_vinculado() == this) {
				lvinculada_a.setEsta_vinculado(null);
			}
		}
	}
	
	public Usuario getVinculada_a() {
		return vinculada_a;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
