package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ParadaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idParadas;
	public final IntegerExpression es_contenida_paradasId;
	public final AssociationExpression es_contenida_paradas;
	public final StringExpression nombre;
	public final StringExpression direccion;
	public final StringExpression dirFoto;
	public final CollectionExpression recorridos;
	public final CollectionExpression parada_Evento;
	public final CollectionExpression contiene_paradas;
	
	public ParadaDetachedCriteria() {
		super(Parada.class, ParadaCriteria.class);
		idParadas = new IntegerExpression("idParadas", this.getDetachedCriteria());
		es_contenida_paradasId = new IntegerExpression("es_contenida_paradas.idBarrio", this.getDetachedCriteria());
		es_contenida_paradas = new AssociationExpression("es_contenida_paradas", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
		dirFoto = new StringExpression("dirFoto", this.getDetachedCriteria());
		recorridos = new CollectionExpression("ORM_recorridos", this.getDetachedCriteria());
		parada_Evento = new CollectionExpression("ORM_parada_Evento", this.getDetachedCriteria());
		contiene_paradas = new CollectionExpression("ORM_contiene_paradas", this.getDetachedCriteria());
	}
	
	public ParadaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, ParadaCriteria.class);
		idParadas = new IntegerExpression("idParadas", this.getDetachedCriteria());
		es_contenida_paradasId = new IntegerExpression("es_contenida_paradas.idBarrio", this.getDetachedCriteria());
		es_contenida_paradas = new AssociationExpression("es_contenida_paradas", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
		dirFoto = new StringExpression("dirFoto", this.getDetachedCriteria());
		recorridos = new CollectionExpression("ORM_recorridos", this.getDetachedCriteria());
		parada_Evento = new CollectionExpression("ORM_parada_Evento", this.getDetachedCriteria());
		contiene_paradas = new CollectionExpression("ORM_contiene_paradas", this.getDetachedCriteria());
	}
	
	public BarrioDetachedCriteria createEs_contenida_paradasCriteria() {
		return new BarrioDetachedCriteria(createCriteria("es_contenida_paradas"));
	}
	
	public RecorridoDetachedCriteria createRecorridosCriteria() {
		return new RecorridoDetachedCriteria(createCriteria("ORM_recorridos"));
	}
	
	public Parada_EventoDetachedCriteria createParada_EventoCriteria() {
		return new Parada_EventoDetachedCriteria(createCriteria("ORM_parada_Evento"));
	}
	
	public Punto_InteresDetachedCriteria createContiene_paradasCriteria() {
		return new Punto_InteresDetachedCriteria(createCriteria("ORM_contiene_paradas"));
	}
	
	public Parada uniqueParada(PersistentSession session) {
		return (Parada) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Parada[] listParada(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Parada[]) list.toArray(new Parada[list.size()]);
	}
}

