package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Punto_Interes")
public class Punto_Interes implements Serializable {
	public Punto_Interes() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_PUNTO_INTERES_CONTIENE) {
			return ORM_contiene;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_PUNTO_INTERES_SON_CONTENIDOS) {
			this.son_contenidos = (Parada) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="IdPuntoInteres", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="PUNTO_INTERES_IDPUNTOINTERES_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="PUNTO_INTERES_IDPUNTOINTERES_GENERATOR", strategy="native")	
	private int idPuntoInteres;
	
	@ManyToOne(targetEntity=Parada.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="ParadaIdParadas", referencedColumnName="IdParadas") })	
	private Parada son_contenidos;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Lugar", nullable=true, length=255)	
	private String lugar;
	
	@Column(name="Precio", nullable=false)	
	private double precio;
	
	@Column(name="HoraIni", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date horaIni;
	
	@Column(name="HoraFin", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date horaFin;
	
	@Column(name="Descripcion", nullable=true, length=255)	
	private String descripcion;
	
	@Column(name="DirFoto", nullable=true, length=255)	
	private String dirFoto;
	
	@OneToMany(mappedBy="son_contenidos", targetEntity=Evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contiene = new java.util.HashSet();
	
	private void setIdPuntoInteres(int value) {
		this.idPuntoInteres = value;
	}
	
	public int getIdPuntoInteres() {
		return idPuntoInteres;
	}
	
	public int getORMID() {
		return getIdPuntoInteres();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setLugar(String value) {
		this.lugar = value;
	}
	
	public String getLugar() {
		return lugar;
	}
	
	public void setPrecio(double value) {
		this.precio = value;
	}
	
	public double getPrecio() {
		return precio;
	}
	
	public void setHoraIni(java.util.Date value) {
		this.horaIni = value;
	}
	
	public java.util.Date getHoraIni() {
		return horaIni;
	}
	
	public void setHoraFin(java.util.Date value) {
		this.horaFin = value;
	}
	
	public java.util.Date getHoraFin() {
		return horaFin;
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDirFoto(String value) {
		this.dirFoto = value;
	}
	
	public String getDirFoto() {
		return dirFoto;
	}
	
	public void setSon_contenidos(Parada value) {
		if (son_contenidos != null) {
			son_contenidos.contiene_paradas.remove(this);
		}
		if (value != null) {
			value.contiene_paradas.add(this);
		}
	}
	
	public Parada getSon_contenidos() {
		return son_contenidos;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Son_contenidos(Parada value) {
		this.son_contenidos = value;
	}
	
	private Parada getORM_Son_contenidos() {
		return son_contenidos;
	}
	
	private void setORM_Contiene(java.util.Set value) {
		this.ORM_contiene = value;
	}
	
	private java.util.Set getORM_Contiene() {
		return ORM_contiene;
	}
	
	@Transient	
	public final EventoSetCollection contiene = new EventoSetCollection(this, _ormAdapter, ORMConstants.KEY_PUNTO_INTERES_CONTIENE, ORMConstants.KEY_EVENTO_SON_CONTENIDOS, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getIdPuntoInteres());
	}
	
}
