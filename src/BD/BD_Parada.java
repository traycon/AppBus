package BD;

import java.util.ArrayList;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Parada {
	public BD_Principal _bd_prin_par;
	public ArrayList<Parada> _cont_paradas = new ArrayList<Parada>();

	public void modParada(int aIdParada, String aNombre, String aDireccion, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.getParadaByORMID(aIdParada);
			as.setNombre(aNombre);
			as.setDireccion(aDireccion);
			as.setDirFoto(aDirFoto);
			ParadaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void addParada(String aNombre, String aDireccion, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.createParada();
			as.setNombre(aNombre);
			as.setDireccion(aDireccion);
			as.setDirFoto(aDirFoto);
			ParadaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarEventoParada(int aIdEvento, int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Evento as = EventoDAO.getEventoByORMID(aIdEvento);
			Parada parada=ParadaDAO.getParadaByORMID(aIdParada);
			Parada_Evento pev=Parada_EventoDAO.createParada_Evento();
			pev.setContienen_eventos(parada);
			pev.setEventoIdEventos(as);
			Parada_EventoDAO.save(pev);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarEventoParada(int aIdEvento, int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada_Evento[] pev=Parada_EventoDAO.listParada_EventoByQuery(null, null);
			for (int i = 0; i < pev.length; i++) {
				if(aIdParada==pev[i].getContienen_eventos().getORMID()
						&& aIdEvento==pev[i].getEventoIdEventos().getORMID()){
					Parada_EventoDAO.deleteAndDissociate(pev[i]);
				}
			}
			
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public ArrayList<Parada> Cargar_Paradas() throws PersistentException {
		ArrayList<Parada> Paradas=new ArrayList<Parada>();
		Parada[] aux=null;
			 aux = ParadaDAO.listParadaByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 Paradas.add(aux[i]);
			}
	
		
		return Paradas;
	}

	public void delParada(int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.getParadaByORMID(aIdParada);
			ParadaDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Parada Cargar_Parada(int aIdParada) throws PersistentException {
		Parada as=null;
			as = ParadaDAO.getParadaByORMID(aIdParada);
		
		return as;
	}

	public void subirFoto(int aIdParada, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.getParadaByORMID(aIdParada);
			as.setDirFoto(aDirFoto);
			ParadaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarLineasParada(int aIdParada, int aIdLinea, Recorrido aRecorrido) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
			/**Recorrido re=RecorridoDAO.createRecorrido();
			re.setLinea(as);
			re.setParada(aParada);**/
			as.addParada(aRecorrido, aParada);
			LineaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarLineasParada(int aIdParada, int aIdLinea) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Linea as = LineaDAO.getLineaByORMID(aIdLinea);
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
			Recorrido re=aParada.getRecorridoByLinea(as);
			RecorridoDAO.deleteAndDissociate(re);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarPuntosInteresParada(int aIdPuntoInteres, int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.getParadaByORMID(aIdParada);
			Punto_Interes pi=Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			pi.setSon_contenidos(as);
			Punto_InteresDAO.save(pi);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarPuntosInteresParada(int aIdPuntoInteres, int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Parada as = ParadaDAO.getParadaByORMID(aIdParada);
			Punto_Interes pi=Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			pi.setSon_contenidos(null);
			Punto_InteresDAO.save(pi);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public ArrayList<Linea> cargar_LineasParada(int ormid) throws PersistentException {
		
		ArrayList<Linea> Lineas=new ArrayList<Linea>();
		Linea[] aux=null;
		
			 aux = LineaDAO.listLineaByQuery(null, null);
			 for (int i = 0; i < aux.length; i++) {
				Parada[] aux1= aux[i].getParadas();
				for (int j = 0; j < aux1.length; j++) {
					if(aux1[j].getORMID()==ormid){
						Lineas.add(aux[i]);
					}
				}
			}
	return	Lineas;
	}

	public Evento[] cargar_EventosParada(int ormid) throws PersistentException {
		ArrayList<Evento> pa=new ArrayList<Evento>();
		Parada_Evento[] pev=Parada_EventoDAO.listParada_EventoByQuery(null, null);
		for (int i = 0; i < pev.length; i++) {
			if(ormid==pev[i].getContienen_eventos().getORMID()){
				pa.add(pev[i].getEventoIdEventos());
			}
		}
		Evento[] as=new Evento[pa.size()];
		for (int i = 0; i < pa.size(); i++) {
			as[i]=pa.get(i);
		}
		return as;
	}
	public Punto_Interes[] cargar_PuntosInteresParada(int ormid) throws PersistentException {
		ArrayList<Punto_Interes> pa=new ArrayList<Punto_Interes>();
		Punto_Interes[] pev=Punto_InteresDAO.listPunto_InteresByQuery(null, null);
		for (int i = 0; i < pev.length; i++) {
			if(pev[i].getSon_contenidos()!=null&&ormid==pev[i].getSon_contenidos().getORMID()){
				pa.add(pev[i]);
			}
		}
		Punto_Interes[] as=new Punto_Interes[pa.size()];
		for (int i = 0; i < pa.size(); i++) {
			as[i]=pa.get(i);
		}
		return as;
	}
}