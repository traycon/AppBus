package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;

public class LineaSetCollection extends org.orm.util.ORMSet {
	public LineaSetCollection(Object owner, org.orm.util.ORMAdapter adapter, int ownerKey, int targetKey, int collType) {
		super(owner, adapter, ownerKey, targetKey, true, collType);
	}
	
	public LineaSetCollection(Object owner, org.orm.util.ORMAdapter adapter, int ownerKey, int collType) {
		super(owner, adapter, ownerKey, -1, false, collType);
	}
	
	/**
	* Return an iterator over the persistent objects
	* @return The persisten objects iterator
	*/
	public java.util.Iterator getIterator() {
		return super.getIterator(_ownerAdapter);
	}
	
	/**
	 * Add the specified persistent object to ORMSet
	 * @param value the persistent object
	 */
	public void add(Linea value) {
		if (value != null) {
			super.add(value, value._ormAdapter);
		}
	}
	
	/**
	 * Remove the specified persistent object from ORMSet
	 * @param value the persistent object
	 */
	public void remove(Linea value) {
		super.remove(value, value._ormAdapter);
	}
	
	/**
	 * Return true if ORMSet contains the specified persistent object
	 * @param value the persistent object
	 * @return True if this contains the specified persistent object
	 */
	public boolean contains(Linea value) {
		return super.contains(value);
	}
	
	/**
	 * Return an array containing all of the persistent objects in ORMSet
	 * @return The persistent objects array
	 */
	public Linea[] toArray() {
		return (Linea[]) super.toArray(new Linea[size()]);
	}
	
	/**
	 * Return an sorted array containing all of the persistent objects in ORMSet
	 * @param propertyName Name of the property for sorting:<ul>
	 * <li>idLinea</li>
	 * <li>nombre</li>
	 * <li>numero</li>
	 * <li>paradaIni</li>
	 * <li>fechaIni</li>
	 * <li>nombreParadaIni</li>
	 * <li>paradaFin</li>
	 * <li>fechaFin</li>
	 * <li>nombreParadFin</li>
	 * <li>observaciones</li>
	 * <li>tarifa</li>
	 * <li>dirMapa</li>
	 * </ul>
	 * @return The persistent objects sorted array
	 */
	public Linea[] toArray(String propertyName) {
		return toArray(propertyName, true);
	}
	
	/**
	 * Return an sorted array containing all of the persistent objects in ORMSet
	 * @param propertyName Name of the property for sorting:<ul>
	 * <li>idLinea</li>
	 * <li>nombre</li>
	 * <li>numero</li>
	 * <li>paradaIni</li>
	 * <li>fechaIni</li>
	 * <li>nombreParadaIni</li>
	 * <li>paradaFin</li>
	 * <li>fechaFin</li>
	 * <li>nombreParadFin</li>
	 * <li>observaciones</li>
	 * <li>tarifa</li>
	 * <li>dirMapa</li>
	 * </ul>
	 * @param ascending true for ascending, false for descending
	 * @return The persistent objects sorted array
	 */
	public Linea[] toArray(String propertyName, boolean ascending) {
		return (Linea[]) super.toArray(new Linea[size()], propertyName, ascending);
	}
	
	protected PersistentManager getPersistentManager() throws PersistentException {
		return ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance();
	}
	
}

