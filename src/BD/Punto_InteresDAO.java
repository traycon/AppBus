package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Punto_InteresDAO {
	public static Punto_Interes loadPunto_InteresByORMID(int idPuntoInteres) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadPunto_InteresByORMID(session, idPuntoInteres);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes getPunto_InteresByORMID(int idPuntoInteres) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getPunto_InteresByORMID(session, idPuntoInteres);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByORMID(int idPuntoInteres, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadPunto_InteresByORMID(session, idPuntoInteres, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes getPunto_InteresByORMID(int idPuntoInteres, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getPunto_InteresByORMID(session, idPuntoInteres, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByORMID(PersistentSession session, int idPuntoInteres) throws PersistentException {
		try {
			return (Punto_Interes) session.load(Punto_Interes.class, new Integer(idPuntoInteres));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes getPunto_InteresByORMID(PersistentSession session, int idPuntoInteres) throws PersistentException {
		try {
			return (Punto_Interes) session.get(Punto_Interes.class, new Integer(idPuntoInteres));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByORMID(PersistentSession session, int idPuntoInteres, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Punto_Interes) session.load(Punto_Interes.class, new Integer(idPuntoInteres), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes getPunto_InteresByORMID(PersistentSession session, int idPuntoInteres, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Punto_Interes) session.get(Punto_Interes.class, new Integer(idPuntoInteres), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPunto_Interes(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryPunto_Interes(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPunto_Interes(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryPunto_Interes(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes[] listPunto_InteresByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listPunto_InteresByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes[] listPunto_InteresByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listPunto_InteresByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPunto_Interes(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Punto_Interes as Punto_Interes");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPunto_Interes(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Punto_Interes as Punto_Interes");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Punto_Interes", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes[] listPunto_InteresByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryPunto_Interes(session, condition, orderBy);
			return (Punto_Interes[]) list.toArray(new Punto_Interes[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes[] listPunto_InteresByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryPunto_Interes(session, condition, orderBy, lockMode);
			return (Punto_Interes[]) list.toArray(new Punto_Interes[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadPunto_InteresByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadPunto_InteresByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Punto_Interes[] punto_Intereses = listPunto_InteresByQuery(session, condition, orderBy);
		if (punto_Intereses != null && punto_Intereses.length > 0)
			return punto_Intereses[0];
		else
			return null;
	}
	
	public static Punto_Interes loadPunto_InteresByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Punto_Interes[] punto_Intereses = listPunto_InteresByQuery(session, condition, orderBy, lockMode);
		if (punto_Intereses != null && punto_Intereses.length > 0)
			return punto_Intereses[0];
		else
			return null;
	}
	
	public static java.util.Iterator iteratePunto_InteresByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iteratePunto_InteresByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePunto_InteresByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iteratePunto_InteresByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePunto_InteresByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Punto_Interes as Punto_Interes");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePunto_InteresByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Punto_Interes as Punto_Interes");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Punto_Interes", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes createPunto_Interes() {
		return new Punto_Interes();
	}
	
	public static boolean save(Punto_Interes punto_Interes) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(punto_Interes);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Punto_Interes punto_Interes) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(punto_Interes);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Punto_Interes punto_Interes)throws PersistentException {
		try {
			if (punto_Interes.getSon_contenidos() != null) {
				punto_Interes.getSon_contenidos().contiene_paradas.remove(punto_Interes);
			}
			
			Evento[] lContienes = punto_Interes.contiene.toArray();
			for(int i = 0; i < lContienes.length; i++) {
				lContienes[i].setSon_contenidos(null);
			}
			return delete(punto_Interes);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Punto_Interes punto_Interes, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (punto_Interes.getSon_contenidos() != null) {
				punto_Interes.getSon_contenidos().contiene_paradas.remove(punto_Interes);
			}
			
			Evento[] lContienes = punto_Interes.contiene.toArray();
			for(int i = 0; i < lContienes.length; i++) {
				lContienes[i].setSon_contenidos(null);
			}
			try {
				session.delete(punto_Interes);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Punto_Interes punto_Interes) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(punto_Interes);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Punto_Interes punto_Interes) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(punto_Interes);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Punto_Interes loadPunto_InteresByCriteria(Punto_InteresCriteria punto_InteresCriteria) {
		Punto_Interes[] punto_Intereses = listPunto_InteresByCriteria(punto_InteresCriteria);
		if(punto_Intereses == null || punto_Intereses.length == 0) {
			return null;
		}
		return punto_Intereses[0];
	}
	
	public static Punto_Interes[] listPunto_InteresByCriteria(Punto_InteresCriteria punto_InteresCriteria) {
		return punto_InteresCriteria.listPunto_Interes();
	}
}
