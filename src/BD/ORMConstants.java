package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_BARRIO_CONTIENE = 1145760959;
	
	final int KEY_BARRIO_CONTIENE_BARRIOS = -2006840978;
	
	final int KEY_CALLE_ES_CONTENIDA = 781521586;
	
	final int KEY_EVENTO_CONTIENEN_EVENTOS = -858670530;
	
	final int KEY_EVENTO_SON_CONTENIDOS = -121402575;
	
	final int KEY_HISTORIAL_SOLUCION_ES_CONTENIDA_HISTORIAL = -1332629329;
	
	final int KEY_LINEA_RECORRIDOS = 1360333264;
	
	final int KEY_PARADA_EVENTO_CONTIENEN_EVENTOS = 1622441920;
	
	final int KEY_PARADA_EVENTO_EVENTOIDEVENTOS = -454367802;
	
	final int KEY_PARADA_CONTIENE_PARADAS = 1436178174;
	
	final int KEY_PARADA_ES_CONTENIDA_PARADAS = -1232520685;
	
	final int KEY_PARADA_PARADA_EVENTO = -367413483;
	
	final int KEY_PARADA_RECORRIDOS = -921561568;
	
	final int KEY_PUNTO_INTERES_CONTIENE = -608801803;
	
	final int KEY_PUNTO_INTERES_SON_CONTENIDOS = 1517603031;
	
	final int KEY_RECORRIDO_LINEA = -1977534461;
	
	final int KEY_RECORRIDO_PARADA = -1066782201;
	
	final int KEY_TARIFA_VINCULADA_A = 925827361;
	
	final int KEY_USUARIO_CONTIENE_USUARIO = 547316741;
	
	final int KEY_USUARIO_ESTA_VINCULADO = 270327226;
	
}
