package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class ParadaDAO {
	public static Parada loadParadaByORMID(int idParadas) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadParadaByORMID(session, idParadas);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada getParadaByORMID(int idParadas) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getParadaByORMID(session, idParadas);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByORMID(int idParadas, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadParadaByORMID(session, idParadas, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada getParadaByORMID(int idParadas, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getParadaByORMID(session, idParadas, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByORMID(PersistentSession session, int idParadas) throws PersistentException {
		try {
			return (Parada) session.load(Parada.class, new Integer(idParadas));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada getParadaByORMID(PersistentSession session, int idParadas) throws PersistentException {
		try {
			return (Parada) session.get(Parada.class, new Integer(idParadas));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByORMID(PersistentSession session, int idParadas, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Parada) session.load(Parada.class, new Integer(idParadas), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada getParadaByORMID(PersistentSession session, int idParadas, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Parada) session.get(Parada.class, new Integer(idParadas), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParada(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryParada(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParada(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryParada(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada[] listParadaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listParadaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada[] listParadaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listParadaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParada(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Parada as Parada");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParada(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Parada as Parada");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Parada", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada[] listParadaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryParada(session, condition, orderBy);
			return (Parada[]) list.toArray(new Parada[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada[] listParadaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryParada(session, condition, orderBy, lockMode);
			return (Parada[]) list.toArray(new Parada[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadParadaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadParadaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Parada[] paradas = listParadaByQuery(session, condition, orderBy);
		if (paradas != null && paradas.length > 0)
			return paradas[0];
		else
			return null;
	}
	
	public static Parada loadParadaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Parada[] paradas = listParadaByQuery(session, condition, orderBy, lockMode);
		if (paradas != null && paradas.length > 0)
			return paradas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateParadaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateParadaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateParadaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateParadaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateParadaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Parada as Parada");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateParadaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Parada as Parada");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Parada", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada createParada() {
		return new Parada();
	}
	
	public static boolean save(Parada parada) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(parada);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Parada parada) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(parada);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Parada parada)throws PersistentException {
		try {
			if (parada.getEs_contenida_paradas() != null) {
				parada.getEs_contenida_paradas().contiene_barrios.remove(parada);
			}
			
			Recorrido[] lRecorridoss = parada.recorridos.toArray();
			for(int i = 0; i < lRecorridoss.length; i++) {
				lRecorridoss[i].setParada(null);
			}
			Parada_Evento[] lParada_Eventos = parada.parada_Evento.toArray();
			for(int i = 0; i < lParada_Eventos.length; i++) {
				lParada_Eventos[i].setContienen_eventos(null);
			}
			Punto_Interes[] lContiene_paradass = parada.contiene_paradas.toArray();
			for(int i = 0; i < lContiene_paradass.length; i++) {
				lContiene_paradass[i].setSon_contenidos(null);
			}
			return delete(parada);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Parada parada, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (parada.getEs_contenida_paradas() != null) {
				parada.getEs_contenida_paradas().contiene_barrios.remove(parada);
			}
			
			Recorrido[] lRecorridoss = parada.recorridos.toArray();
			for(int i = 0; i < lRecorridoss.length; i++) {
				lRecorridoss[i].setParada(null);
			}
			Parada_Evento[] lParada_Eventos = parada.parada_Evento.toArray();
			for(int i = 0; i < lParada_Eventos.length; i++) {
				lParada_Eventos[i].setContienen_eventos(null);
			}
			Punto_Interes[] lContiene_paradass = parada.contiene_paradas.toArray();
			for(int i = 0; i < lContiene_paradass.length; i++) {
				lContiene_paradass[i].setSon_contenidos(null);
			}
			try {
				session.delete(parada);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Parada parada) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(parada);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Parada parada) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(parada);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parada loadParadaByCriteria(ParadaCriteria paradaCriteria) {
		Parada[] paradas = listParadaByCriteria(paradaCriteria);
		if(paradas == null || paradas.length == 0) {
			return null;
		}
		return paradas[0];
	}
	
	public static Parada[] listParadaByCriteria(ParadaCriteria paradaCriteria) {
		return paradaCriteria.listParada();
	}
}
