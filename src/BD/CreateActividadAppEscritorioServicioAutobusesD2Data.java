package BD;
/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
public class CreateActividadAppEscritorioServicioAutobusesD2Data {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada parada = ParadaDAO.createParada();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : recorridos
			ParadaDAO.save(parada);
			Linea linea = LineaDAO.createLinea();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : recorridos, tarifa, paradaFin, paradaIni, numero
			LineaDAO.save(linea);
			Evento evento = EventoDAO.createEvento();
			// Initialize the properties of the persistent object here
			EventoDAO.save(evento);
			Punto_Interes punto_Interes = Punto_InteresDAO.createPunto_Interes();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : precio
			Punto_InteresDAO.save(punto_Interes);
			Historial_Solucion historial_Solucion = Historial_SolucionDAO.createHistorial_Solucion();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : es_contenida_historial
			Historial_SolucionDAO.save(historial_Solucion);
			Usuario usuario = UsuarioDAO.createUsuario();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : contiene_usuario, administrador, cp, telefono
			UsuarioDAO.save(usuario);
			Barrio barrio = BarrioDAO.createBarrio();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : contiene
			BarrioDAO.save(barrio);
			Descarga descarga = DescargaDAO.createDescarga();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tamanio
			DescargaDAO.save(descarga);
			Tarifa tarifa = TarifaDAO.createTarifa();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bono, precio
			TarifaDAO.save(tarifa);
			Calle calle = CalleDAO.createCalle();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : es_contenida
			CalleDAO.save(calle);
			Recorrido recorrido = RecorridoDAO.createRecorrido();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : parada, linea, orden
			RecorridoDAO.save(recorrido);
			Parada_Evento parada_Evento = Parada_EventoDAO.createParada_Evento();
			// Initialize the properties of the persistent object here
			Parada_EventoDAO.save(parada_Evento);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateActividadAppEscritorioServicioAutobusesD2Data createActividadAppEscritorioServicioAutobusesD2Data = new CreateActividadAppEscritorioServicioAutobusesD2Data();
			try {
				createActividadAppEscritorioServicioAutobusesD2Data.createTestData();
			}
			finally {
				ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
