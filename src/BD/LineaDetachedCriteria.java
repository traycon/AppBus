package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class LineaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idLinea;
	public final StringExpression nombre;
	public final IntegerExpression numero;
	public final IntegerExpression paradaIni;
	public final DateExpression fechaIni;
	public final StringExpression nombreParadaIni;
	public final IntegerExpression paradaFin;
	public final DateExpression fechaFin;
	public final StringExpression nombreParadFin;
	public final StringExpression observaciones;
	public final DoubleExpression tarifa;
	public final StringExpression dirMapa;
	public final CollectionExpression recorridos;
	
	public LineaDetachedCriteria() {
		super(Linea.class, LineaCriteria.class);
		idLinea = new IntegerExpression("idLinea", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		numero = new IntegerExpression("numero", this.getDetachedCriteria());
		paradaIni = new IntegerExpression("paradaIni", this.getDetachedCriteria());
		fechaIni = new DateExpression("fechaIni", this.getDetachedCriteria());
		nombreParadaIni = new StringExpression("nombreParadaIni", this.getDetachedCriteria());
		paradaFin = new IntegerExpression("paradaFin", this.getDetachedCriteria());
		fechaFin = new DateExpression("fechaFin", this.getDetachedCriteria());
		nombreParadFin = new StringExpression("nombreParadFin", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		tarifa = new DoubleExpression("tarifa", this.getDetachedCriteria());
		dirMapa = new StringExpression("dirMapa", this.getDetachedCriteria());
		recorridos = new CollectionExpression("ORM_recorridos", this.getDetachedCriteria());
	}
	
	public LineaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, LineaCriteria.class);
		idLinea = new IntegerExpression("idLinea", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		numero = new IntegerExpression("numero", this.getDetachedCriteria());
		paradaIni = new IntegerExpression("paradaIni", this.getDetachedCriteria());
		fechaIni = new DateExpression("fechaIni", this.getDetachedCriteria());
		nombreParadaIni = new StringExpression("nombreParadaIni", this.getDetachedCriteria());
		paradaFin = new IntegerExpression("paradaFin", this.getDetachedCriteria());
		fechaFin = new DateExpression("fechaFin", this.getDetachedCriteria());
		nombreParadFin = new StringExpression("nombreParadFin", this.getDetachedCriteria());
		observaciones = new StringExpression("observaciones", this.getDetachedCriteria());
		tarifa = new DoubleExpression("tarifa", this.getDetachedCriteria());
		dirMapa = new StringExpression("dirMapa", this.getDetachedCriteria());
		recorridos = new CollectionExpression("ORM_recorridos", this.getDetachedCriteria());
	}
	
	public RecorridoDetachedCriteria createRecorridosCriteria() {
		return new RecorridoDetachedCriteria(createCriteria("ORM_recorridos"));
	}
	
	public Linea uniqueLinea(PersistentSession session) {
		return (Linea) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Linea[] listLinea(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Linea[]) list.toArray(new Linea[list.size()]);
	}
}

