package BD;

import java.util.ArrayList;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Tarifa {
	public BD_Principal _bd_prin_tar;
	public ArrayList<Tarifa> _cont_tarifas = new ArrayList<Tarifa>();

	public ArrayList<Tarifa> Cargar_Tarifas() throws PersistentException {
		ArrayList<Tarifa> Tarifas=new ArrayList<Tarifa>();
		Tarifa[] aux=null;
			 aux = TarifaDAO.listTarifaByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 Tarifas.add(aux[i]);
			}
	
		
		return Tarifas;
	}

	public void addTarifa(String aTipoTarifa, double aPrecio, boolean aBono) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Tarifa as = TarifaDAO.createTarifa();
			as.setTipoTarifa(aTipoTarifa);
			as.setPrecio(aPrecio);
			as.setBono(aBono);
			TarifaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modTarifa(int aId, String aTipoTarifa, double aPrecio, boolean aBono) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Tarifa as = TarifaDAO.getTarifaByORMID(aId);
			as.setTipoTarifa(aTipoTarifa);
			as.setPrecio(aPrecio);
			as.setBono(aBono);
			TarifaDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delTarifa(int aIdTarifa) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Tarifa as = TarifaDAO.getTarifaByORMID(aIdTarifa);
			TarifaDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
}