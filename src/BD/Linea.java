package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Linea")
public class Linea implements Serializable {
	public Linea() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_LINEA_RECORRIDOS) {
			return ORM_recorridos;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="IdLinea", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="LINEA_IDLINEA_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="LINEA_IDLINEA_GENERATOR", strategy="native")	
	private int idLinea;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Numero", nullable=false, length=11)	
	private int numero;
	
	@Column(name="ParadaIni", nullable=false, length=11)	
	private int paradaIni;
	
	@Column(name="FechaIni", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaIni;
	
	@Column(name="NombreParadaIni", nullable=true, length=255)	
	private String nombreParadaIni;
	
	@Column(name="ParadaFin", nullable=false, length=11)	
	private int paradaFin;
	
	@Column(name="FechaFin", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaFin;
	
	@Column(name="NombreParadFin", nullable=true, length=255)	
	private String nombreParadFin;
	
	@Column(name="Observaciones", nullable=true, length=255)	
	private String observaciones;
	
	@Column(name="Tarifa", nullable=false)	
	private double tarifa;
	
	@Column(name="DirMapa", nullable=true, length=255)	
	private String dirMapa;
	
	@OneToMany(mappedBy="linea", targetEntity=Recorrido.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_recorridos = new java.util.HashSet();
	
	private void setIdLinea(int value) {
		this.idLinea = value;
	}
	
	public int getIdLinea() {
		return idLinea;
	}
	
	public int getORMID() {
		return getIdLinea();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNumero(int value) {
		this.numero = value;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setParadaIni(int value) {
		this.paradaIni = value;
	}
	
	public int getParadaIni() {
		return paradaIni;
	}
	
	public void setFechaIni(java.util.Date value) {
		this.fechaIni = value;
	}
	
	public java.util.Date getFechaIni() {
		return fechaIni;
	}
	
	public void setNombreParadaIni(String value) {
		this.nombreParadaIni = value;
	}
	
	public String getNombreParadaIni() {
		return nombreParadaIni;
	}
	
	public void setParadaFin(int value) {
		this.paradaFin = value;
	}
	
	public int getParadaFin() {
		return paradaFin;
	}
	
	public void setFechaFin(java.util.Date value) {
		this.fechaFin = value;
	}
	
	public java.util.Date getFechaFin() {
		return fechaFin;
	}
	
	public void setNombreParadFin(String value) {
		this.nombreParadFin = value;
	}
	
	public String getNombreParadFin() {
		return nombreParadFin;
	}
	
	public void setObservaciones(String value) {
		this.observaciones = value;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setTarifa(double value) {
		this.tarifa = value;
	}
	
	public double getTarifa() {
		return tarifa;
	}
	
	public void setDirMapa(String value) {
		this.dirMapa = value;
	}
	
	public String getDirMapa() {
		return dirMapa;
	}
	
	public Parada[] getParadas() {
		java.util.ArrayList lValues = new java.util.ArrayList(5);
		for(java.util.Iterator lIter = recorridos.getIterator();lIter.hasNext();) {
			lValues.add(((Recorrido)lIter.next()).getParada());
		}
		return (Parada[])lValues.toArray(new Parada[lValues.size()]);
	}
	
	public void removeParada(Parada aParada) {
		Recorrido[] lRecorridos = recorridos.toArray();
		for(int i = 0; i < lRecorridos.length; i++) {
			if(lRecorridos[i].getParada().equals(aParada)) {
				recorridos.remove(lRecorridos[i]);
			}
		}
	}
	
	public void addParada(Recorrido aRecorrido, Parada aParada) {
		aRecorrido.setParada(aParada);
		recorridos.add(aRecorrido);
	}
	
	public Recorrido getRecorridoByParada(Parada aParada) {
		Recorrido[] lRecorridos = recorridos.toArray();
		for(int i = 0; i < lRecorridos.length; i++) {
			if(lRecorridos[i].getParada().equals(aParada)) {
				return lRecorridos[i];
			}
		}
		return null;
	}
	
	private void setORM_Recorridos(java.util.Set value) {
		this.ORM_recorridos = value;
	}
	
	private java.util.Set getORM_Recorridos() {
		return ORM_recorridos;
	}
	
	@Transient	
	public final RecorridoSetCollection recorridos = new RecorridoSetCollection(this, _ormAdapter, ORMConstants.KEY_LINEA_RECORRIDOS, ORMConstants.KEY_RECORRIDO_LINEA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getIdLinea());
	}
	
}
