package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class LineaDAO {
	public static Linea loadLineaByORMID(int idLinea) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadLineaByORMID(session, idLinea);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea getLineaByORMID(int idLinea) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getLineaByORMID(session, idLinea);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByORMID(int idLinea, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadLineaByORMID(session, idLinea, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea getLineaByORMID(int idLinea, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getLineaByORMID(session, idLinea, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByORMID(PersistentSession session, int idLinea) throws PersistentException {
		try {
			return (Linea) session.load(Linea.class, new Integer(idLinea));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea getLineaByORMID(PersistentSession session, int idLinea) throws PersistentException {
		try {
			return (Linea) session.get(Linea.class, new Integer(idLinea));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByORMID(PersistentSession session, int idLinea, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Linea) session.load(Linea.class, new Integer(idLinea), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea getLineaByORMID(PersistentSession session, int idLinea, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Linea) session.get(Linea.class, new Integer(idLinea), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryLinea(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryLinea(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryLinea(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryLinea(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea[] listLineaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listLineaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea[] listLineaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listLineaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryLinea(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Linea as Linea");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryLinea(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Linea as Linea");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Linea", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea[] listLineaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryLinea(session, condition, orderBy);
			return (Linea[]) list.toArray(new Linea[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea[] listLineaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryLinea(session, condition, orderBy, lockMode);
			return (Linea[]) list.toArray(new Linea[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadLineaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadLineaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Linea[] lineas = listLineaByQuery(session, condition, orderBy);
		if (lineas != null && lineas.length > 0)
			return lineas[0];
		else
			return null;
	}
	
	public static Linea loadLineaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Linea[] lineas = listLineaByQuery(session, condition, orderBy, lockMode);
		if (lineas != null && lineas.length > 0)
			return lineas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateLineaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateLineaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateLineaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateLineaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateLineaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Linea as Linea");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateLineaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Linea as Linea");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Linea", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea createLinea() {
		return new Linea();
	}
	
	public static boolean save(Linea linea) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(linea);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Linea linea) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(linea);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Linea linea)throws PersistentException {
		try {
			Recorrido[] lRecorridoss = linea.recorridos.toArray();
			for(int i = 0; i < lRecorridoss.length; i++) {
				lRecorridoss[i].setLinea(null);
			}
			return delete(linea);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Linea linea, org.orm.PersistentSession session)throws PersistentException {
		try {
			Recorrido[] lRecorridoss = linea.recorridos.toArray();
			for(int i = 0; i < lRecorridoss.length; i++) {
				lRecorridoss[i].setLinea(null);
			}
			try {
				session.delete(linea);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Linea linea) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(linea);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Linea linea) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(linea);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Linea loadLineaByCriteria(LineaCriteria lineaCriteria) {
		Linea[] lineas = listLineaByCriteria(lineaCriteria);
		if(lineas == null || lineas.length == 0) {
			return null;
		}
		return lineas[0];
	}
	
	public static Linea[] listLineaByCriteria(LineaCriteria lineaCriteria) {
		return lineaCriteria.listLinea();
	}
}
