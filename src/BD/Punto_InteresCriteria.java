package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Punto_InteresCriteria extends AbstractORMCriteria {
	public final IntegerExpression idPuntoInteres;
	public final IntegerExpression son_contenidosId;
	public final AssociationExpression son_contenidos;
	public final StringExpression nombre;
	public final StringExpression lugar;
	public final DoubleExpression precio;
	public final DateExpression horaIni;
	public final DateExpression horaFin;
	public final StringExpression descripcion;
	public final StringExpression dirFoto;
	public final CollectionExpression contiene;
	
	public Punto_InteresCriteria(Criteria criteria) {
		super(criteria);
		idPuntoInteres = new IntegerExpression("idPuntoInteres", this);
		son_contenidosId = new IntegerExpression("son_contenidos.idParadas", this);
		son_contenidos = new AssociationExpression("son_contenidos", this);
		nombre = new StringExpression("nombre", this);
		lugar = new StringExpression("lugar", this);
		precio = new DoubleExpression("precio", this);
		horaIni = new DateExpression("horaIni", this);
		horaFin = new DateExpression("horaFin", this);
		descripcion = new StringExpression("descripcion", this);
		dirFoto = new StringExpression("dirFoto", this);
		contiene = new CollectionExpression("ORM_contiene", this);
	}
	
	public Punto_InteresCriteria(PersistentSession session) {
		this(session.createCriteria(Punto_Interes.class));
	}
	
	public Punto_InteresCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public ParadaCriteria createSon_contenidosCriteria() {
		return new ParadaCriteria(createCriteria("son_contenidos"));
	}
	
	public EventoCriteria createContieneCriteria() {
		return new EventoCriteria(createCriteria("ORM_contiene"));
	}
	
	public Punto_Interes uniquePunto_Interes() {
		return (Punto_Interes) super.uniqueResult();
	}
	
	public Punto_Interes[] listPunto_Interes() {
		java.util.List list = super.list();
		return (Punto_Interes[]) list.toArray(new Punto_Interes[list.size()]);
	}
}

