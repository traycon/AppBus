package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CalleCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression es_contenidaId;
	public final AssociationExpression es_contenida;
	public final StringExpression direccion;
	
	public CalleCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		es_contenidaId = new IntegerExpression("es_contenida.idBarrio", this);
		es_contenida = new AssociationExpression("es_contenida", this);
		direccion = new StringExpression("direccion", this);
	}
	
	public CalleCriteria(PersistentSession session) {
		this(session.createCriteria(Calle.class));
	}
	
	public CalleCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public BarrioCriteria createEs_contenidaCriteria() {
		return new BarrioCriteria(createCriteria("es_contenida"));
	}
	
	public Calle uniqueCalle() {
		return (Calle) super.uniqueResult();
	}
	
	public Calle[] listCalle() {
		java.util.List list = super.list();
		return (Calle[]) list.toArray(new Calle[list.size()]);
	}
}

