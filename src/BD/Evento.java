package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Evento")
public class Evento implements Serializable {
	public Evento() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_EVENTO_CONTIENEN_EVENTOS) {
			return ORM_contienen_eventos;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_EVENTO_SON_CONTENIDOS) {
			this.son_contenidos = (Punto_Interes) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="IdEventos", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="EVENTO_IDEVENTOS_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="EVENTO_IDEVENTOS_GENERATOR", strategy="native")	
	private int idEventos;
	
	@ManyToOne(targetEntity=Punto_Interes.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="Punto_InteresIdPuntoInteres", referencedColumnName="IdPuntoInteres") })	
	private Punto_Interes son_contenidos;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Lugar", nullable=true, length=255)	
	private String lugar;
	
	@Column(name="FechaIni", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaIni;
	
	@Column(name="FechaFin", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaFin;
	
	@Column(name="Descripcion", nullable=true, length=255)	
	private String descripcion;
	
	@Column(name="DirCartel", nullable=true, length=255)	
	private String dirCartel;
	
	@OneToMany(mappedBy="eventoIdEventos", targetEntity=Parada_Evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contienen_eventos = new java.util.HashSet();
	
	private void setIdEventos(int value) {
		this.idEventos = value;
	}
	
	public int getIdEventos() {
		return idEventos;
	}
	
	public int getORMID() {
		return getIdEventos();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setLugar(String value) {
		this.lugar = value;
	}
	
	public String getLugar() {
		return lugar;
	}
	
	public void setFechaIni(java.util.Date value) {
		this.fechaIni = value;
	}
	
	public java.util.Date getFechaIni() {
		return fechaIni;
	}
	
	public void setFechaFin(java.util.Date value) {
		this.fechaFin = value;
	}
	
	public java.util.Date getFechaFin() {
		return fechaFin;
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDirCartel(String value) {
		this.dirCartel = value;
	}
	
	public String getDirCartel() {
		return dirCartel;
	}
	
	public void setSon_contenidos(Punto_Interes value) {
		if (son_contenidos != null) {
			son_contenidos.contiene.remove(this);
		}
		if (value != null) {
			value.contiene.add(this);
		}
	}
	
	public Punto_Interes getSon_contenidos() {
		return son_contenidos;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Son_contenidos(Punto_Interes value) {
		this.son_contenidos = value;
	}
	
	private Punto_Interes getORM_Son_contenidos() {
		return son_contenidos;
	}
	
	private void setORM_Contienen_eventos(java.util.Set value) {
		this.ORM_contienen_eventos = value;
	}
	
	private java.util.Set getORM_Contienen_eventos() {
		return ORM_contienen_eventos;
	}
	
	@Transient	
	public final Parada_EventoSetCollection contienen_eventos = new Parada_EventoSetCollection(this, _ormAdapter, ORMConstants.KEY_EVENTO_CONTIENEN_EVENTOS, ORMConstants.KEY_PARADA_EVENTO_EVENTOIDEVENTOS, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getIdEventos());
	}
	
}
