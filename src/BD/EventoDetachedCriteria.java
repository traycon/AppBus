package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EventoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idEventos;
	public final IntegerExpression son_contenidosId;
	public final AssociationExpression son_contenidos;
	public final StringExpression nombre;
	public final StringExpression lugar;
	public final DateExpression fechaIni;
	public final DateExpression fechaFin;
	public final StringExpression descripcion;
	public final StringExpression dirCartel;
	public final CollectionExpression contienen_eventos;
	
	public EventoDetachedCriteria() {
		super(Evento.class, EventoCriteria.class);
		idEventos = new IntegerExpression("idEventos", this.getDetachedCriteria());
		son_contenidosId = new IntegerExpression("son_contenidos.idPuntoInteres", this.getDetachedCriteria());
		son_contenidos = new AssociationExpression("son_contenidos", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		lugar = new StringExpression("lugar", this.getDetachedCriteria());
		fechaIni = new DateExpression("fechaIni", this.getDetachedCriteria());
		fechaFin = new DateExpression("fechaFin", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		dirCartel = new StringExpression("dirCartel", this.getDetachedCriteria());
		contienen_eventos = new CollectionExpression("ORM_contienen_eventos", this.getDetachedCriteria());
	}
	
	public EventoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, EventoCriteria.class);
		idEventos = new IntegerExpression("idEventos", this.getDetachedCriteria());
		son_contenidosId = new IntegerExpression("son_contenidos.idPuntoInteres", this.getDetachedCriteria());
		son_contenidos = new AssociationExpression("son_contenidos", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		lugar = new StringExpression("lugar", this.getDetachedCriteria());
		fechaIni = new DateExpression("fechaIni", this.getDetachedCriteria());
		fechaFin = new DateExpression("fechaFin", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		dirCartel = new StringExpression("dirCartel", this.getDetachedCriteria());
		contienen_eventos = new CollectionExpression("ORM_contienen_eventos", this.getDetachedCriteria());
	}
	
	public Punto_InteresDetachedCriteria createSon_contenidosCriteria() {
		return new Punto_InteresDetachedCriteria(createCriteria("son_contenidos"));
	}
	
	public Parada_EventoDetachedCriteria createContienen_eventosCriteria() {
		return new Parada_EventoDetachedCriteria(createCriteria("ORM_contienen_eventos"));
	}
	
	public Evento uniqueEvento(PersistentSession session) {
		return (Evento) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Evento[] listEvento(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Evento[]) list.toArray(new Evento[list.size()]);
	}
}

