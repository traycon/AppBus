package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Parada_Evento")
public class Parada_Evento implements Serializable {
	public Parada_Evento() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_PARADA_EVENTO_CONTIENEN_EVENTOS) {
			this.contienen_eventos = (Parada) owner;
		}
		
		else if (key == ORMConstants.KEY_PARADA_EVENTO_EVENTOIDEVENTOS) {
			this.eventoIdEventos = (Evento) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@ManyToOne(targetEntity=Parada.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="ParadaIdParadas", referencedColumnName="IdParadas") })	
	private Parada contienen_eventos;
	
	@ManyToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="EventoIdEventos", referencedColumnName="IdEventos") })	
	private Evento eventoIdEventos;
	
	@Column(name="ID", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="PARADA_EVENTO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="PARADA_EVENTO_ID_GENERATOR", strategy="native")	
	private int ID;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setContienen_eventos(Parada value) {
		if (contienen_eventos != null) {
			contienen_eventos.parada_Evento.remove(this);
		}
		if (value != null) {
			value.parada_Evento.add(this);
		}
	}
	
	public Parada getContienen_eventos() {
		return contienen_eventos;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Contienen_eventos(Parada value) {
		this.contienen_eventos = value;
	}
	
	private Parada getORM_Contienen_eventos() {
		return contienen_eventos;
	}
	
	public void setEventoIdEventos(Evento value) {
		if (eventoIdEventos != null) {
			eventoIdEventos.contienen_eventos.remove(this);
		}
		if (value != null) {
			value.contienen_eventos.add(this);
		}
	}
	
	public Evento getEventoIdEventos() {
		return eventoIdEventos;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_EventoIdEventos(Evento value) {
		this.eventoIdEventos = value;
	}
	
	private Evento getORM_EventoIdEventos() {
		return eventoIdEventos;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
