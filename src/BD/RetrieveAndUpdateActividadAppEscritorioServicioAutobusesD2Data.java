package BD;
/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
public class RetrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada parada = ParadaDAO.loadParadaByQuery(null, null);
			// Update the properties of the persistent object
			ParadaDAO.save(parada);
			Linea linea = LineaDAO.loadLineaByQuery(null, null);
			// Update the properties of the persistent object
			LineaDAO.save(linea);
			Evento evento = EventoDAO.loadEventoByQuery(null, null);
			// Update the properties of the persistent object
			EventoDAO.save(evento);
			Punto_Interes punto_Interes = Punto_InteresDAO.loadPunto_InteresByQuery(null, null);
			// Update the properties of the persistent object
			Punto_InteresDAO.save(punto_Interes);
			Historial_Solucion historial_Solucion = Historial_SolucionDAO.loadHistorial_SolucionByQuery(null, null);
			// Update the properties of the persistent object
			Historial_SolucionDAO.save(historial_Solucion);
			Usuario usuario = UsuarioDAO.loadUsuarioByQuery(null, null);
			// Update the properties of the persistent object
			UsuarioDAO.save(usuario);
			Barrio barrio = BarrioDAO.loadBarrioByQuery(null, null);
			// Update the properties of the persistent object
			BarrioDAO.save(barrio);
			Descarga descarga = DescargaDAO.loadDescargaByQuery(null, null);
			// Update the properties of the persistent object
			DescargaDAO.save(descarga);
			Tarifa tarifa = TarifaDAO.loadTarifaByQuery(null, null);
			// Update the properties of the persistent object
			TarifaDAO.save(tarifa);
			Calle calle = CalleDAO.loadCalleByQuery(null, null);
			// Update the properties of the persistent object
			CalleDAO.save(calle);
			Recorrido recorrido = RecorridoDAO.loadRecorridoByQuery(null, null);
			// Update the properties of the persistent object
			RecorridoDAO.save(recorrido);
			Parada_Evento parada_Evento = Parada_EventoDAO.loadParada_EventoByQuery(null, null);
			// Update the properties of the persistent object
			Parada_EventoDAO.save(parada_Evento);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Parada by ParadaCriteria");
		ParadaCriteria paradaCriteria = new ParadaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//paradaCriteria.idParadas.eq();
		System.out.println(paradaCriteria.uniqueParada());
		
		System.out.println("Retrieving Linea by LineaCriteria");
		LineaCriteria lineaCriteria = new LineaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lineaCriteria.idLinea.eq();
		System.out.println(lineaCriteria.uniqueLinea());
		
		System.out.println("Retrieving Evento by EventoCriteria");
		EventoCriteria eventoCriteria = new EventoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//eventoCriteria.idEventos.eq();
		System.out.println(eventoCriteria.uniqueEvento());
		
		System.out.println("Retrieving Punto_Interes by Punto_InteresCriteria");
		Punto_InteresCriteria punto_InteresCriteria = new Punto_InteresCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//punto_InteresCriteria.idPuntoInteres.eq();
		System.out.println(punto_InteresCriteria.uniquePunto_Interes());
		
		System.out.println("Retrieving Historial_Solucion by Historial_SolucionCriteria");
		Historial_SolucionCriteria historial_SolucionCriteria = new Historial_SolucionCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//historial_SolucionCriteria.ID.eq();
		System.out.println(historial_SolucionCriteria.uniqueHistorial_Solucion());
		
		System.out.println("Retrieving Usuario by UsuarioCriteria");
		UsuarioCriteria usuarioCriteria = new UsuarioCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//usuarioCriteria.idUsuario.eq();
		System.out.println(usuarioCriteria.uniqueUsuario());
		
		System.out.println("Retrieving Barrio by BarrioCriteria");
		BarrioCriteria barrioCriteria = new BarrioCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//barrioCriteria.idBarrio.eq();
		System.out.println(barrioCriteria.uniqueBarrio());
		
		System.out.println("Retrieving Descarga by DescargaCriteria");
		DescargaCriteria descargaCriteria = new DescargaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//descargaCriteria.idDescargas.eq();
		System.out.println(descargaCriteria.uniqueDescarga());
		
		System.out.println("Retrieving Tarifa by TarifaCriteria");
		TarifaCriteria tarifaCriteria = new TarifaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//tarifaCriteria.ID.eq();
		System.out.println(tarifaCriteria.uniqueTarifa());
		
		System.out.println("Retrieving Calle by CalleCriteria");
		CalleCriteria calleCriteria = new CalleCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//calleCriteria.ID.eq();
		System.out.println(calleCriteria.uniqueCalle());
		
		System.out.println("Retrieving Recorrido by RecorridoCriteria");
		RecorridoCriteria recorridoCriteria = new RecorridoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//recorridoCriteria.ID.eq();
		System.out.println(recorridoCriteria.uniqueRecorrido());
		
		System.out.println("Retrieving Parada_Evento by Parada_EventoCriteria");
		Parada_EventoCriteria parada_EventoCriteria = new Parada_EventoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//parada_EventoCriteria.ID.eq();
		System.out.println(parada_EventoCriteria.uniqueParada_Evento());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data retrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data = new RetrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data();
			try {
				retrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data.retrieveAndUpdateTestData();
				//retrieveAndUpdateActividadAppEscritorioServicioAutobusesD2Data.retrieveByCriteria();
			}
			finally {
				ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
