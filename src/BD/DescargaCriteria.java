package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DescargaCriteria extends AbstractORMCriteria {
	public final IntegerExpression idDescargas;
	public final StringExpression nombre;
	public final StringExpression descripcion;
	public final DoubleExpression tamanio;
	public final BooleanExpression estado;
	public final StringExpression version;
	public final DateExpression fechaSubida;
	public final DateExpression ultimaModificacion;
	
	public DescargaCriteria(Criteria criteria) {
		super(criteria);
		idDescargas = new IntegerExpression("idDescargas", this);
		nombre = new StringExpression("nombre", this);
		descripcion = new StringExpression("descripcion", this);
		tamanio = new DoubleExpression("tamanio", this);
		estado = new BooleanExpression("estado", this);
		version = new StringExpression("version", this);
		fechaSubida = new DateExpression("fechaSubida", this);
		ultimaModificacion = new DateExpression("ultimaModificacion", this);
	}
	
	public DescargaCriteria(PersistentSession session) {
		this(session.createCriteria(Descarga.class));
	}
	
	public DescargaCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public Descarga uniqueDescarga() {
		return (Descarga) super.uniqueResult();
	}
	
	public Descarga[] listDescarga() {
		java.util.List list = super.list();
		return (Descarga[]) list.toArray(new Descarga[list.size()]);
	}
}

