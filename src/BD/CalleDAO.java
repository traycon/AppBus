package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class CalleDAO {
	public static Calle loadCalleByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadCalleByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle getCalleByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getCalleByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadCalleByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle getCalleByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getCalleByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Calle) session.load(Calle.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle getCalleByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Calle) session.get(Calle.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Calle) session.load(Calle.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle getCalleByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Calle) session.get(Calle.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryCalle(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryCalle(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryCalle(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryCalle(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle[] listCalleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listCalleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle[] listCalleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listCalleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryCalle(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Calle as Calle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryCalle(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Calle as Calle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Calle", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle[] listCalleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryCalle(session, condition, orderBy);
			return (Calle[]) list.toArray(new Calle[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle[] listCalleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryCalle(session, condition, orderBy, lockMode);
			return (Calle[]) list.toArray(new Calle[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadCalleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadCalleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Calle[] calles = listCalleByQuery(session, condition, orderBy);
		if (calles != null && calles.length > 0)
			return calles[0];
		else
			return null;
	}
	
	public static Calle loadCalleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Calle[] calles = listCalleByQuery(session, condition, orderBy, lockMode);
		if (calles != null && calles.length > 0)
			return calles[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateCalleByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateCalleByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateCalleByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateCalleByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateCalleByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Calle as Calle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateCalleByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Calle as Calle");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Calle", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle createCalle() {
		return new Calle();
	}
	
	public static boolean save(Calle calle) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(calle);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Calle calle) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(calle);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Calle calle)throws PersistentException {
		try {
			if (calle.getEs_contenida() != null) {
				calle.getEs_contenida().contiene.remove(calle);
			}
			
			return delete(calle);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Calle calle, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (calle.getEs_contenida() != null) {
				calle.getEs_contenida().contiene.remove(calle);
			}
			
			try {
				session.delete(calle);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Calle calle) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(calle);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Calle calle) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(calle);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Calle loadCalleByCriteria(CalleCriteria calleCriteria) {
		Calle[] calles = listCalleByCriteria(calleCriteria);
		if(calles == null || calles.length == 0) {
			return null;
		}
		return calles[0];
	}
	
	public static Calle[] listCalleByCriteria(CalleCriteria calleCriteria) {
		return calleCriteria.listCalle();
	}
}
