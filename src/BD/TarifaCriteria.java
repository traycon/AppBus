package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TarifaCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression vinculada_aId;
	public final AssociationExpression vinculada_a;
	public final StringExpression tipoTarifa;
	public final DoubleExpression precio;
	public final BooleanExpression bono;
	
	public TarifaCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		vinculada_aId = new IntegerExpression("vinculada_a.idUsuario", this);
		vinculada_a = new AssociationExpression("vinculada_a", this);
		tipoTarifa = new StringExpression("tipoTarifa", this);
		precio = new DoubleExpression("precio", this);
		bono = new BooleanExpression("bono", this);
	}
	
	public TarifaCriteria(PersistentSession session) {
		this(session.createCriteria(Tarifa.class));
	}
	
	public TarifaCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public UsuarioCriteria createVinculada_aCriteria() {
		return new UsuarioCriteria(createCriteria("vinculada_a"));
	}
	
	public Tarifa uniqueTarifa() {
		return (Tarifa) super.uniqueResult();
	}
	
	public Tarifa[] listTarifa() {
		java.util.List list = super.list();
		return (Tarifa[]) list.toArray(new Tarifa[list.size()]);
	}
}

