package BD;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface IInvitado extends Remote {
	public ArrayList<Descarga> Cargar_descargas()throws RemoteException;

	public Descarga descargar(int aIdDescargas)throws RemoteException;

	public Usuario Iniciar_Sesion(String aUsuario, String aContrasena)throws RemoteException;

	public ArrayList<Tarifa> Cargar_Tarifas()throws RemoteException;

	public Historial_Solucion cargar_consulta(String aCodigo)throws RemoteException;
	
	public ArrayList<Parada> Cargar_Paradas()throws RemoteException;

	public Parada Cargar_Parada(int aIdParada)throws RemoteException;

	public ArrayList<Linea> Cargar_lineas()throws RemoteException;

	public Linea Cargar_linea(int aIdLinea)throws RemoteException;

	public ArrayList<Punto_Interes> Cargar_PuntosInteres()throws RemoteException;

	public Punto_Interes Cargar_PuntosInteres(int aIdPuntoInteres)throws RemoteException;

	public ArrayList<Evento> Cargar_Eventos()throws RemoteException;

	public Evento Cargar_Evento(int aIdEvento)throws RemoteException;
}