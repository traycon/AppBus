package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class RecorridoDAO {
	public static Recorrido loadRecorridoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadRecorridoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido getRecorridoByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getRecorridoByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadRecorridoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido getRecorridoByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return getRecorridoByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Recorrido) session.load(Recorrido.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido getRecorridoByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Recorrido) session.get(Recorrido.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Recorrido) session.load(Recorrido.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido getRecorridoByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Recorrido) session.get(Recorrido.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecorrido(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryRecorrido(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecorrido(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return queryRecorrido(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido[] listRecorridoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listRecorridoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido[] listRecorridoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return listRecorridoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecorrido(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Recorrido as Recorrido");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRecorrido(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Recorrido as Recorrido");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Recorrido", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido[] listRecorridoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRecorrido(session, condition, orderBy);
			return (Recorrido[]) list.toArray(new Recorrido[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido[] listRecorridoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRecorrido(session, condition, orderBy, lockMode);
			return (Recorrido[]) list.toArray(new Recorrido[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadRecorridoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return loadRecorridoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Recorrido[] recorridos = listRecorridoByQuery(session, condition, orderBy);
		if (recorridos != null && recorridos.length > 0)
			return recorridos[0];
		else
			return null;
	}
	
	public static Recorrido loadRecorridoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Recorrido[] recorridos = listRecorridoByQuery(session, condition, orderBy, lockMode);
		if (recorridos != null && recorridos.length > 0)
			return recorridos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRecorridoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateRecorridoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecorridoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession();
			return iterateRecorridoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecorridoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Recorrido as Recorrido");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRecorridoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From Recorrido as Recorrido");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Recorrido", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido createRecorrido() {
		return new Recorrido();
	}
	
	public static boolean save(Recorrido recorrido) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().saveObject(recorrido);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(Recorrido recorrido) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().deleteObject(recorrido);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Recorrido recorrido)throws PersistentException {
		try {
			if (recorrido.getLinea() != null) {
				recorrido.getLinea().recorridos.remove(recorrido);
			}
			
			if (recorrido.getParada() != null) {
				recorrido.getParada().recorridos.remove(recorrido);
			}
			
			return delete(recorrido);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(Recorrido recorrido, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (recorrido.getLinea() != null) {
				recorrido.getLinea().recorridos.remove(recorrido);
			}
			
			if (recorrido.getParada() != null) {
				recorrido.getParada().recorridos.remove(recorrido);
			}
			
			try {
				session.delete(recorrido);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(Recorrido recorrido) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().refresh(recorrido);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(Recorrido recorrido) throws PersistentException {
		try {
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().evict(recorrido);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Recorrido loadRecorridoByCriteria(RecorridoCriteria recorridoCriteria) {
		Recorrido[] recorridos = listRecorridoByCriteria(recorridoCriteria);
		if(recorridos == null || recorridos.length == 0) {
			return null;
		}
		return recorridos[0];
	}
	
	public static Recorrido[] listRecorridoByCriteria(RecorridoCriteria recorridoCriteria) {
		return recorridoCriteria.listRecorrido();
	}
}
