package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Parada")
public class Parada implements Serializable {
	public Parada() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_PARADA_RECORRIDOS) {
			return ORM_recorridos;
		}
		else if (key == ORMConstants.KEY_PARADA_PARADA_EVENTO) {
			return ORM_parada_Evento;
		}
		else if (key == ORMConstants.KEY_PARADA_CONTIENE_PARADAS) {
			return ORM_contiene_paradas;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_PARADA_ES_CONTENIDA_PARADAS) {
			this.es_contenida_paradas = (Barrio) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="IdParadas", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="PARADA_IDPARADAS_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="PARADA_IDPARADAS_GENERATOR", strategy="native")	
	private int idParadas;
	
	@ManyToOne(targetEntity=Barrio.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="BarrioIdBarrio", referencedColumnName="IdBarrio") })	
	private Barrio es_contenida_paradas;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Direccion", nullable=true, length=255)	
	private String direccion;
	
	@Column(name="DirFoto", nullable=true, length=255)	
	private String dirFoto;
	
	@OneToMany(mappedBy="parada", targetEntity=Recorrido.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_recorridos = new java.util.HashSet();
	
	@OneToMany(mappedBy="contienen_eventos", targetEntity=Parada_Evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_parada_Evento = new java.util.HashSet();
	
	@OneToMany(mappedBy="son_contenidos", targetEntity=Punto_Interes.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contiene_paradas = new java.util.HashSet();
	
	private void setIdParadas(int value) {
		this.idParadas = value;
	}
	
	public int getIdParadas() {
		return idParadas;
	}
	
	public int getORMID() {
		return getIdParadas();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setDireccion(String value) {
		this.direccion = value;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDirFoto(String value) {
		this.dirFoto = value;
	}
	
	public String getDirFoto() {
		return dirFoto;
	}
	
	public void setEs_contenida_paradas(Barrio value) {
		if (es_contenida_paradas != null) {
			es_contenida_paradas.contiene_barrios.remove(this);
		}
		if (value != null) {
			value.contiene_barrios.add(this);
		}
	}
	
	public Barrio getEs_contenida_paradas() {
		return es_contenida_paradas;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Es_contenida_paradas(Barrio value) {
		this.es_contenida_paradas = value;
	}
	
	private Barrio getORM_Es_contenida_paradas() {
		return es_contenida_paradas;
	}
	
	public Linea[] getLineas() {
		java.util.ArrayList lValues = new java.util.ArrayList(5);
		for(java.util.Iterator lIter = recorridos.getIterator();lIter.hasNext();) {
			lValues.add(((Recorrido)lIter.next()).getLinea());
		}
		return (Linea[])lValues.toArray(new Linea[lValues.size()]);
	}
	
	public void removeLinea(Linea aLinea) {
		Recorrido[] lRecorridos = recorridos.toArray();
		for(int i = 0; i < lRecorridos.length; i++) {
			if(lRecorridos[i].getLinea().equals(aLinea)) {
				recorridos.remove(lRecorridos[i]);
			}
		}
	}
	
	public void addLinea(Recorrido aRecorrido, Linea aLinea) {
		aRecorrido.setLinea(aLinea);
		recorridos.add(aRecorrido);
	}
	
	public Recorrido getRecorridoByLinea(Linea aLinea) {
		Recorrido[] lRecorridos = recorridos.toArray();
		for(int i = 0; i < lRecorridos.length; i++) {
			if(lRecorridos[i].getLinea().equals(aLinea)) {
				return lRecorridos[i];
			}
		}
		return null;
	}
	
	private void setORM_Recorridos(java.util.Set value) {
		this.ORM_recorridos = value;
	}
	
	private java.util.Set getORM_Recorridos() {
		return ORM_recorridos;
	}
	
	@Transient	
	public final RecorridoSetCollection recorridos = new RecorridoSetCollection(this, _ormAdapter, ORMConstants.KEY_PARADA_RECORRIDOS, ORMConstants.KEY_RECORRIDO_PARADA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Parada_Evento(java.util.Set value) {
		this.ORM_parada_Evento = value;
	}
	
	private java.util.Set getORM_Parada_Evento() {
		return ORM_parada_Evento;
	}
	
	@Transient	
	public final Parada_EventoSetCollection parada_Evento = new Parada_EventoSetCollection(this, _ormAdapter, ORMConstants.KEY_PARADA_PARADA_EVENTO, ORMConstants.KEY_PARADA_EVENTO_CONTIENEN_EVENTOS, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Contiene_paradas(java.util.Set value) {
		this.ORM_contiene_paradas = value;
	}
	
	private java.util.Set getORM_Contiene_paradas() {
		return ORM_contiene_paradas;
	}
	
	@Transient	
	public final Punto_InteresSetCollection contiene_paradas = new Punto_InteresSetCollection(this, _ormAdapter, ORMConstants.KEY_PARADA_CONTIENE_PARADAS, ORMConstants.KEY_PUNTO_INTERES_SON_CONTENIDOS, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getIdParadas());
	}
	
}
