package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BarrioCriteria extends AbstractORMCriteria {
	public final IntegerExpression idBarrio;
	public final StringExpression nombre;
	public final CollectionExpression contiene_barrios;
	public final CollectionExpression contiene;
	
	public BarrioCriteria(Criteria criteria) {
		super(criteria);
		idBarrio = new IntegerExpression("idBarrio", this);
		nombre = new StringExpression("nombre", this);
		contiene_barrios = new CollectionExpression("ORM_contiene_barrios", this);
		contiene = new CollectionExpression("ORM_contiene", this);
	}
	
	public BarrioCriteria(PersistentSession session) {
		this(session.createCriteria(Barrio.class));
	}
	
	public BarrioCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public ParadaCriteria createContiene_barriosCriteria() {
		return new ParadaCriteria(createCriteria("ORM_contiene_barrios"));
	}
	
	public CalleCriteria createContieneCriteria() {
		return new CalleCriteria(createCriteria("ORM_contiene"));
	}
	
	public Barrio uniqueBarrio() {
		return (Barrio) super.uniqueResult();
	}
	
	public Barrio[] listBarrio() {
		java.util.List list = super.list();
		return (Barrio[]) list.toArray(new Barrio[list.size()]);
	}
}

