package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UsuarioCriteria extends AbstractORMCriteria {
	public final IntegerExpression idUsuario;
	public final StringExpression nombre;
	public final StringExpression apellidos;
	public final StringExpression correo;
	public final StringExpression password;
	public final IntegerExpression telefono;
	public final StringExpression direccion;
	public final IntegerExpression cp;
	public final StringExpression idBono;
	public final DateExpression validezBono;
	public final BooleanExpression administrador;
	public final CollectionExpression contiene_usuario;
	public final IntegerExpression esta_vinculadoId;
	public final AssociationExpression esta_vinculado;
	
	public UsuarioCriteria(Criteria criteria) {
		super(criteria);
		idUsuario = new IntegerExpression("idUsuario", this);
		nombre = new StringExpression("nombre", this);
		apellidos = new StringExpression("apellidos", this);
		correo = new StringExpression("correo", this);
		password = new StringExpression("password", this);
		telefono = new IntegerExpression("telefono", this);
		direccion = new StringExpression("direccion", this);
		cp = new IntegerExpression("cp", this);
		idBono = new StringExpression("idBono", this);
		validezBono = new DateExpression("validezBono", this);
		administrador = new BooleanExpression("administrador", this);
		contiene_usuario = new CollectionExpression("ORM_contiene_usuario", this);
		esta_vinculadoId = new IntegerExpression("esta_vinculado.idUsuario", this);
		esta_vinculado = new AssociationExpression("esta_vinculado", this);
	}
	
	public UsuarioCriteria(PersistentSession session) {
		this(session.createCriteria(Usuario.class));
	}
	
	public UsuarioCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public Historial_SolucionCriteria createContiene_usuarioCriteria() {
		return new Historial_SolucionCriteria(createCriteria("ORM_contiene_usuario"));
	}
	
	public TarifaCriteria createEsta_vinculadoCriteria() {
		return new TarifaCriteria(createCriteria("esta_vinculado"));
	}
	
	public Usuario uniqueUsuario() {
		return (Usuario) super.uniqueResult();
	}
	
	public Usuario[] listUsuario() {
		java.util.List list = super.list();
		return (Usuario[]) list.toArray(new Usuario[list.size()]);
	}
}

