package BD;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Bd_Usuario {
	public BD_Principal _bd_prin_usu;
	public ArrayList<Usuario> _cont_usuarios = new ArrayList<Usuario>();

	public Usuario Cargar_Configuracion(int aIdUsuario) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Usuario as=null;
		try {
			 
			as = UsuarioDAO.getUsuarioByORMID(aIdUsuario);
		}
		catch (Exception e) {
			t.rollback();
		}
		
		return as;
	}

	public void GuardarConfig(int aIdUsuario, String oldPass, String newPass) throws PersistentException, NoSuchAlgorithmException {

			PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
			try {
				 
				Usuario as = UsuarioDAO.getUsuarioByORMID(aIdUsuario);
				if(as.getPassword()==oldPass){
				as.setPassword(newPass);
				UsuarioDAO.save(as);
				t.commit();
				}
			}
			catch (Exception e) {
				t.rollback();
			}
		}

	public Usuario Iniciar_Sesion(String aUsuario, String aContrasena) throws NoSuchAlgorithmException, PersistentException {
		Usuario us=null;
		Usuario[] aux=null;
			 aux = UsuarioDAO.listUsuarioByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 if(aux[i].getPassword().equals(aContrasena) && aux[i].getIdBono().equals(aUsuario)){
				us=aux[i];
				break;
				 }
			}
		return us;
	}

	public ArrayList<Usuario> Cargar_Usuarios() throws PersistentException {
		ArrayList<Usuario> Usuarios=new ArrayList<Usuario>();
		Usuario[] aux;
			 aux = UsuarioDAO.listUsuarioByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 Usuarios.add(aux[i]);
			}
		
		
		return Usuarios;
	}

	public void addUsuario(String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Usuario as = UsuarioDAO.createUsuario();
			as.setNombre(aNombre);
			as.setApellidos(aApellidos);
			as.setCorreo(aCorreo);
			as.setPassword(aPassword);
			as.setTelefono(aTelefono);
			as.setDireccion(aDireccion);
			as.setCp(aCp);
			as.setIdBono(aIdBono);
			as.setValidezBono(aValidezBono);
			as.setAdministrador(aAdministrador);
			UsuarioDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modUsuario(int aIdUsuario, String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Usuario as = UsuarioDAO.getUsuarioByORMID(aIdUsuario);
			as.setNombre(aNombre);
			as.setApellidos(aApellidos);
			as.setCorreo(aCorreo);
			as.setPassword(aPassword);
			as.setTelefono(aTelefono);
			as.setDireccion(aDireccion);
			as.setCp(aCp);
			as.setIdBono(aIdBono);
			as.setValidezBono(aValidezBono);
			as.setAdministrador(aAdministrador);
			UsuarioDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delUsuario(int aIdUsuario) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Usuario as = UsuarioDAO.getUsuarioByORMID(aIdUsuario);
			UsuarioDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void Recargar(String aIdBono, double aCantidad) throws PersistentException {
		Usuario us;
		Usuario[] aux=null;
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 aux = UsuarioDAO.listUsuarioByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 if(aux[i].getIdBono().equals(aIdBono)){
				us=aux[i];
				Date fecha=new Date(System.currentTimeMillis());
				fecha.setMonth(fecha.getMonth()+1);
				cambiarValidezBono(us.getIdUsuario(), fecha);
				break;
				 }
			}
		}
		catch (Exception e) {
			t.rollback();
		}	
	}

	public void cambiarValidezBono(int aIdUsuario, Date aFecha) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Usuario as = UsuarioDAO.getUsuarioByORMID(aIdUsuario);
			as.setValidezBono(aFecha);
			UsuarioDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
}