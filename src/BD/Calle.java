package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Calle")
public class Calle implements Serializable {
	public Calle() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_CALLE_ES_CONTENIDA) {
			this.es_contenida = (Barrio) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="ID", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="CALLE_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="CALLE_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@ManyToOne(targetEntity=Barrio.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="BarrioIdBarrio", referencedColumnName="IdBarrio", nullable=false) })	
	private Barrio es_contenida;
	
	@Column(name="Direccion", nullable=true, length=255)	
	private String direccion;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setDireccion(String value) {
		this.direccion = value;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setEs_contenida(Barrio value) {
		if (es_contenida != null) {
			es_contenida.contiene.remove(this);
		}
		if (value != null) {
			value.contiene.add(this);
		}
	}
	
	public Barrio getEs_contenida() {
		return es_contenida;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Es_contenida(Barrio value) {
		this.es_contenida = value;
	}
	
	private Barrio getORM_Es_contenida() {
		return es_contenida;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
