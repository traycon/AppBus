package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ParadaCriteria extends AbstractORMCriteria {
	public final IntegerExpression idParadas;
	public final IntegerExpression es_contenida_paradasId;
	public final AssociationExpression es_contenida_paradas;
	public final StringExpression nombre;
	public final StringExpression direccion;
	public final StringExpression dirFoto;
	public final CollectionExpression recorridos;
	public final CollectionExpression parada_Evento;
	public final CollectionExpression contiene_paradas;
	
	public ParadaCriteria(Criteria criteria) {
		super(criteria);
		idParadas = new IntegerExpression("idParadas", this);
		es_contenida_paradasId = new IntegerExpression("es_contenida_paradas.idBarrio", this);
		es_contenida_paradas = new AssociationExpression("es_contenida_paradas", this);
		nombre = new StringExpression("nombre", this);
		direccion = new StringExpression("direccion", this);
		dirFoto = new StringExpression("dirFoto", this);
		recorridos = new CollectionExpression("ORM_recorridos", this);
		parada_Evento = new CollectionExpression("ORM_parada_Evento", this);
		contiene_paradas = new CollectionExpression("ORM_contiene_paradas", this);
	}
	
	public ParadaCriteria(PersistentSession session) {
		this(session.createCriteria(Parada.class));
	}
	
	public ParadaCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public BarrioCriteria createEs_contenida_paradasCriteria() {
		return new BarrioCriteria(createCriteria("es_contenida_paradas"));
	}
	
	public RecorridoCriteria createRecorridosCriteria() {
		return new RecorridoCriteria(createCriteria("ORM_recorridos"));
	}
	
	public Parada_EventoCriteria createParada_EventoCriteria() {
		return new Parada_EventoCriteria(createCriteria("ORM_parada_Evento"));
	}
	
	public Punto_InteresCriteria createContiene_paradasCriteria() {
		return new Punto_InteresCriteria(createCriteria("ORM_contiene_paradas"));
	}
	
	public Parada uniqueParada() {
		return (Parada) super.uniqueResult();
	}
	
	public Parada[] listParada() {
		java.util.List list = super.list();
		return (Parada[]) list.toArray(new Parada[list.size()]);
	}
}

