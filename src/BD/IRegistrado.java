package BD;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
public interface IRegistrado extends Remote {
	public ArrayList<Descarga> Cargar_descargas() throws RemoteException; 

	public Descarga descargar(int aIdDescargas) throws RemoteException;

	public Usuario Cargar_Configuracion(int aIdUsuario) throws RemoteException;

	public void GuardarConfig(int aIdUsuario, String oldPass,String newPass) throws RemoteException;

	public ArrayList<Usuario> Cargar_Usuarios() throws RemoteException;

	public void Recargar(String aIdBono, double aCantidad) throws RemoteException;

	public void cambiarValidezBono(int aIdUsuario, Date aFecha)throws RemoteException;

	public ArrayList<Tarifa> Cargar_Tarifas()throws RemoteException;

	public ArrayList<Historial_Solucion> Cargar_Historial(int aIdUsuario)throws RemoteException;
	
	public void addHistorial(String aOrigen, String aDestino, int idUsuario)throws RemoteException;
	
	public String compartir(int aId)throws RemoteException;

	public Historial_Solucion cargar_consulta(String aCodigo)throws RemoteException;
	
	public ArrayList<Parada> Cargar_Paradas()throws RemoteException;

	public Parada Cargar_Parada(int aIdParada)throws RemoteException;

	public ArrayList<Linea> Cargar_lineas()throws RemoteException;

	public Linea Cargar_linea(int aIdLinea)throws RemoteException;

	public ArrayList<Punto_Interes> Cargar_PuntosInteres()throws RemoteException;

	public Punto_Interes Cargar_PuntosInteres(int aIdPuntoInteres)throws RemoteException;

	public ArrayList<Evento> Cargar_Eventos()throws RemoteException;

	public Evento Cargar_Evento(int aIdEvento)throws RemoteException;
}