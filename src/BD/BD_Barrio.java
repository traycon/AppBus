package BD;

import java.util.ArrayList;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Barrio {
	public BD_Principal _bd_prin_bar;
	public ArrayList<Barrio> _cont_barrios = new ArrayList<Barrio>();

	public ArrayList<Barrio> cargar_Barrios() throws PersistentException {
		ArrayList<Barrio> Barrios=new ArrayList<Barrio>();
		 Barrio[] aux=null;
			 aux = BarrioDAO.listBarrioByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				Barrios.add(aux[i]);
			}	
		
		return Barrios;
	}

	public void modBarrio(int aIdBarrio, String aNombre) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Barrio as = BarrioDAO.getBarrioByORMID(aIdBarrio);
			as.setNombre(aNombre);
			BarrioDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void addBarrio(String aNombre) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Barrio as = BarrioDAO.createBarrio();
			as.setNombre(aNombre);
			BarrioDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delBarrio(int aIdBarrio) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Barrio as = BarrioDAO.getBarrioByORMID(aIdBarrio);
			BarrioDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Barrio cargar_Barrio(int aIdBarrio) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Barrio as=null;
		try {
			 as=BarrioDAO.getBarrioByORMID(aIdBarrio);
		}
		catch (Exception e) {
			t.rollback();
		}	
		
		return as;
	}

	public void agregarParadas_Barrio(int aIdBarrio, int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Barrio as = BarrioDAO.getBarrioByORMID(aIdBarrio);
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
			aParada.setEs_contenida_paradas(as);
			ParadaDAO.save(aParada);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarParadas_Barrio(int aIdParada) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada aParada=ParadaDAO.getParadaByORMID(aIdParada);
			aParada.setEs_contenida_paradas(null);
			ParadaDAO.save(aParada);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}
}