package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Parada_EventoCriteria extends AbstractORMCriteria {
	public final IntegerExpression contienen_eventosId;
	public final AssociationExpression contienen_eventos;
	public final IntegerExpression eventoIdEventosId;
	public final AssociationExpression eventoIdEventos;
	public final IntegerExpression ID;
	
	public Parada_EventoCriteria(Criteria criteria) {
		super(criteria);
		contienen_eventosId = new IntegerExpression("contienen_eventos.idParadas", this);
		contienen_eventos = new AssociationExpression("contienen_eventos", this);
		eventoIdEventosId = new IntegerExpression("eventoIdEventos.idEventos", this);
		eventoIdEventos = new AssociationExpression("eventoIdEventos", this);
		ID = new IntegerExpression("ID", this);
	}
	
	public Parada_EventoCriteria(PersistentSession session) {
		this(session.createCriteria(Parada_Evento.class));
	}
	
	public Parada_EventoCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public ParadaCriteria createContienen_eventosCriteria() {
		return new ParadaCriteria(createCriteria("contienen_eventos"));
	}
	
	public EventoCriteria createEventoIdEventosCriteria() {
		return new EventoCriteria(createCriteria("eventoIdEventos"));
	}
	
	public Parada_Evento uniqueParada_Evento() {
		return (Parada_Evento) super.uniqueResult();
	}
	
	public Parada_Evento[] listParada_Evento() {
		java.util.List list = super.list();
		return (Parada_Evento[]) list.toArray(new Parada_Evento[list.size()]);
	}
}

