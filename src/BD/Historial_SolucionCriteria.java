package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Historial_SolucionCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression es_contenida_historialId;
	public final AssociationExpression es_contenida_historial;
	public final StringExpression codigo;
	public final StringExpression origen;
	public final StringExpression destino;
	public final DateExpression fecha;
	
	public Historial_SolucionCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		es_contenida_historialId = new IntegerExpression("es_contenida_historial.idUsuario", this);
		es_contenida_historial = new AssociationExpression("es_contenida_historial", this);
		codigo = new StringExpression("codigo", this);
		origen = new StringExpression("origen", this);
		destino = new StringExpression("destino", this);
		fecha = new DateExpression("fecha", this);
	}
	
	public Historial_SolucionCriteria(PersistentSession session) {
		this(session.createCriteria(Historial_Solucion.class));
	}
	
	public Historial_SolucionCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public UsuarioCriteria createEs_contenida_historialCriteria() {
		return new UsuarioCriteria(createCriteria("es_contenida_historial"));
	}
	
	public Historial_Solucion uniqueHistorial_Solucion() {
		return (Historial_Solucion) super.uniqueResult();
	}
	
	public Historial_Solucion[] listHistorial_Solucion() {
		java.util.List list = super.list();
		return (Historial_Solucion[]) list.toArray(new Historial_Solucion[list.size()]);
	}
}

