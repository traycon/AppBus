package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecorridoCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final DateExpression hora;
	public final IntegerExpression orden;
	public final IntegerExpression lineaId;
	public final AssociationExpression linea;
	public final IntegerExpression paradaId;
	public final AssociationExpression parada;
	
	public RecorridoCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		hora = new DateExpression("hora", this);
		orden = new IntegerExpression("orden", this);
		lineaId = new IntegerExpression("ORM_Linea.null", this);
		linea = new AssociationExpression("ORM_Linea", this);
		paradaId = new IntegerExpression("ORM_Parada.null", this);
		parada = new AssociationExpression("ORM_Parada", this);
	}
	
	public RecorridoCriteria(PersistentSession session) {
		this(session.createCriteria(Recorrido.class));
	}
	
	public RecorridoCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public LineaCriteria createLineaCriteria() {
		return new LineaCriteria(createCriteria("ORM_Linea"));
	}
	
	public ParadaCriteria createParadaCriteria() {
		return new ParadaCriteria(createCriteria("ORM_Parada"));
	}
	
	public Recorrido uniqueRecorrido() {
		return (Recorrido) super.uniqueResult();
	}
	
	public Recorrido[] listRecorrido() {
		java.util.List list = super.list();
		return (Recorrido[]) list.toArray(new Recorrido[list.size()]);
	}
}

