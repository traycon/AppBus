package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Barrio")
public class Barrio implements Serializable {
	public Barrio() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_BARRIO_CONTIENE_BARRIOS) {
			return ORM_contiene_barrios;
		}
		else if (key == ORMConstants.KEY_BARRIO_CONTIENE) {
			return ORM_contiene;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="IdBarrio", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="BARRIO_IDBARRIO_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="BARRIO_IDBARRIO_GENERATOR", strategy="native")	
	private int idBarrio;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@OneToMany(mappedBy="es_contenida_paradas", targetEntity=Parada.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contiene_barrios = new java.util.HashSet();
	
	@OneToMany(mappedBy="es_contenida", targetEntity=Calle.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contiene = new java.util.HashSet();
	
	private void setIdBarrio(int value) {
		this.idBarrio = value;
	}
	
	public int getIdBarrio() {
		return idBarrio;
	}
	
	public int getORMID() {
		return getIdBarrio();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	private void setORM_Contiene_barrios(java.util.Set value) {
		this.ORM_contiene_barrios = value;
	}
	
	private java.util.Set getORM_Contiene_barrios() {
		return ORM_contiene_barrios;
	}
	
	@Transient	
	public final ParadaSetCollection contiene_barrios = new ParadaSetCollection(this, _ormAdapter, ORMConstants.KEY_BARRIO_CONTIENE_BARRIOS, ORMConstants.KEY_PARADA_ES_CONTENIDA_PARADAS, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Contiene(java.util.Set value) {
		this.ORM_contiene = value;
	}
	
	private java.util.Set getORM_Contiene() {
		return ORM_contiene;
	}
	
	@Transient	
	public final CalleSetCollection contiene = new CalleSetCollection(this, _ormAdapter, ORMConstants.KEY_BARRIO_CONTIENE, ORMConstants.KEY_CALLE_ES_CONTENIDA, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getIdBarrio());
	}
	
}
