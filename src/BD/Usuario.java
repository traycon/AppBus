package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Usuario")
public class Usuario implements Serializable {
	public Usuario() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == ORMConstants.KEY_USUARIO_CONTIENE_USUARIO) {
			return ORM_contiene_usuario;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="IdUsuario", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="USUARIO_IDUSUARIO_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="USUARIO_IDUSUARIO_GENERATOR", strategy="native")	
	private int idUsuario;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Apellidos", nullable=true, length=255)	
	private String apellidos;
	
	@Column(name="Correo", nullable=true, length=255)	
	private String correo;
	
	@Column(name="Password", nullable=true, length=255)	
	private String password;
	
	@Column(name="Telefono", nullable=false, length=11)	
	private int telefono;
	
	@Column(name="Direccion", nullable=true, length=255)	
	private String direccion;
	
	@Column(name="Cp", nullable=false, length=11)	
	private int cp;
	
	@Column(name="IdBono", nullable=true, length=255)	
	private String idBono;
	
	@Column(name="ValidezBono", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date validezBono;
	
	@Column(name="Administrador", nullable=false, length=1)	
	private boolean administrador;
	
	@OneToMany(mappedBy="es_contenida_historial", targetEntity=Historial_Solucion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_contiene_usuario = new java.util.HashSet();
	
	@OneToOne(mappedBy="vinculada_a", targetEntity=Tarifa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Tarifa esta_vinculado;
	
	private void setIdUsuario(int value) {
		this.idUsuario = value;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public int getORMID() {
		return getIdUsuario();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setApellidos(String value) {
		this.apellidos = value;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public void setCorreo(String value) {
		this.correo = value;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public void setPassword(String value) {
		this.password = value;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setTelefono(int value) {
		this.telefono = value;
	}
	
	public int getTelefono() {
		return telefono;
	}
	
	public void setDireccion(String value) {
		this.direccion = value;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setCp(int value) {
		this.cp = value;
	}
	
	public int getCp() {
		return cp;
	}
	
	public void setIdBono(String value) {
		this.idBono = value;
	}
	
	public String getIdBono() {
		return idBono;
	}
	
	public void setValidezBono(java.util.Date value) {
		this.validezBono = value;
	}
	
	public java.util.Date getValidezBono() {
		return validezBono;
	}
	
	public void setAdministrador(boolean value) {
		this.administrador = value;
	}
	
	public boolean getAdministrador() {
		return administrador;
	}
	
	private void setORM_Contiene_usuario(java.util.Set value) {
		this.ORM_contiene_usuario = value;
	}
	
	private java.util.Set getORM_Contiene_usuario() {
		return ORM_contiene_usuario;
	}
	
	@Transient	
	public final Historial_SolucionSetCollection contiene_usuario = new Historial_SolucionSetCollection(this, _ormAdapter, ORMConstants.KEY_USUARIO_CONTIENE_USUARIO, ORMConstants.KEY_HISTORIAL_SOLUCION_ES_CONTENIDA_HISTORIAL, ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void setEsta_vinculado(Tarifa value) {
		if (this.esta_vinculado != value) {
			Tarifa lesta_vinculado = this.esta_vinculado;
			this.esta_vinculado = value;
			if (value != null) {
				esta_vinculado.setVinculada_a(this);
			}
			if (lesta_vinculado != null && lesta_vinculado.getVinculada_a() == this) {
				lesta_vinculado.setVinculada_a(null);
			}
		}
	}
	
	public Tarifa getEsta_vinculado() {
		return esta_vinculado;
	}
	
	public String toString() {
		return String.valueOf(getIdUsuario());
	}
	
}
