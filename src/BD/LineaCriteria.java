package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class LineaCriteria extends AbstractORMCriteria {
	public final IntegerExpression idLinea;
	public final StringExpression nombre;
	public final IntegerExpression numero;
	public final IntegerExpression paradaIni;
	public final DateExpression fechaIni;
	public final StringExpression nombreParadaIni;
	public final IntegerExpression paradaFin;
	public final DateExpression fechaFin;
	public final StringExpression nombreParadFin;
	public final StringExpression observaciones;
	public final DoubleExpression tarifa;
	public final StringExpression dirMapa;
	public final CollectionExpression recorridos;
	
	public LineaCriteria(Criteria criteria) {
		super(criteria);
		idLinea = new IntegerExpression("idLinea", this);
		nombre = new StringExpression("nombre", this);
		numero = new IntegerExpression("numero", this);
		paradaIni = new IntegerExpression("paradaIni", this);
		fechaIni = new DateExpression("fechaIni", this);
		nombreParadaIni = new StringExpression("nombreParadaIni", this);
		paradaFin = new IntegerExpression("paradaFin", this);
		fechaFin = new DateExpression("fechaFin", this);
		nombreParadFin = new StringExpression("nombreParadFin", this);
		observaciones = new StringExpression("observaciones", this);
		tarifa = new DoubleExpression("tarifa", this);
		dirMapa = new StringExpression("dirMapa", this);
		recorridos = new CollectionExpression("ORM_recorridos", this);
	}
	
	public LineaCriteria(PersistentSession session) {
		this(session.createCriteria(Linea.class));
	}
	
	public LineaCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public RecorridoCriteria createRecorridosCriteria() {
		return new RecorridoCriteria(createCriteria("ORM_recorridos"));
	}
	
	public Linea uniqueLinea() {
		return (Linea) super.uniqueResult();
	}
	
	public Linea[] listLinea() {
		java.util.List list = super.list();
		return (Linea[]) list.toArray(new Linea[list.size()]);
	}
}

