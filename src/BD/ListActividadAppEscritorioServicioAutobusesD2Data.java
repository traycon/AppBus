package BD;
/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
public class ListActividadAppEscritorioServicioAutobusesD2Data {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Parada...");
		Parada[] paradas = ParadaDAO.listParadaByQuery(null, null);
		int length = Math.min(paradas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(paradas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Linea...");
		Linea[] lineas = LineaDAO.listLineaByQuery(null, null);
		length = Math.min(lineas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(lineas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Evento...");
		Evento[] eventos = EventoDAO.listEventoByQuery(null, null);
		length = Math.min(eventos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(eventos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Punto_Interes...");
		Punto_Interes[] punto_Intereses = Punto_InteresDAO.listPunto_InteresByQuery(null, null);
		length = Math.min(punto_Intereses.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(punto_Intereses[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Historial_Solucion...");
		Historial_Solucion[] historial_Solucions = Historial_SolucionDAO.listHistorial_SolucionByQuery(null, null);
		length = Math.min(historial_Solucions.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(historial_Solucions[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Usuario...");
		Usuario[] usuarios = UsuarioDAO.listUsuarioByQuery(null, null);
		length = Math.min(usuarios.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(usuarios[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Barrio...");
		Barrio[] barrios = BarrioDAO.listBarrioByQuery(null, null);
		length = Math.min(barrios.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(barrios[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Descarga...");
		Descarga[] descargas = DescargaDAO.listDescargaByQuery(null, null);
		length = Math.min(descargas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(descargas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Tarifa...");
		Tarifa[] tarifas = TarifaDAO.listTarifaByQuery(null, null);
		length = Math.min(tarifas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(tarifas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Calle...");
		Calle[] calles = CalleDAO.listCalleByQuery(null, null);
		length = Math.min(calles.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(calles[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Recorrido...");
		Recorrido[] recorridos = RecorridoDAO.listRecorridoByQuery(null, null);
		length = Math.min(recorridos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(recorridos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Parada_Evento...");
		Parada_Evento[] parada_Eventos = Parada_EventoDAO.listParada_EventoByQuery(null, null);
		length = Math.min(parada_Eventos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(parada_Eventos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Parada by Criteria...");
		ParadaCriteria paradaCriteria = new ParadaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//paradaCriteria.idParadas.eq();
		paradaCriteria.setMaxResults(ROW_COUNT);
		Parada[] paradas = paradaCriteria.listParada();
		int length =paradas== null ? 0 : Math.min(paradas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(paradas[i]);
		}
		System.out.println(length + " Parada record(s) retrieved."); 
		
		System.out.println("Listing Linea by Criteria...");
		LineaCriteria lineaCriteria = new LineaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lineaCriteria.idLinea.eq();
		lineaCriteria.setMaxResults(ROW_COUNT);
		Linea[] lineas = lineaCriteria.listLinea();
		length =lineas== null ? 0 : Math.min(lineas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(lineas[i]);
		}
		System.out.println(length + " Linea record(s) retrieved."); 
		
		System.out.println("Listing Evento by Criteria...");
		EventoCriteria eventoCriteria = new EventoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//eventoCriteria.idEventos.eq();
		eventoCriteria.setMaxResults(ROW_COUNT);
		Evento[] eventos = eventoCriteria.listEvento();
		length =eventos== null ? 0 : Math.min(eventos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(eventos[i]);
		}
		System.out.println(length + " Evento record(s) retrieved."); 
		
		System.out.println("Listing Punto_Interes by Criteria...");
		Punto_InteresCriteria punto_InteresCriteria = new Punto_InteresCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//punto_InteresCriteria.idPuntoInteres.eq();
		punto_InteresCriteria.setMaxResults(ROW_COUNT);
		Punto_Interes[] punto_Intereses = punto_InteresCriteria.listPunto_Interes();
		length =punto_Intereses== null ? 0 : Math.min(punto_Intereses.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(punto_Intereses[i]);
		}
		System.out.println(length + " Punto_Interes record(s) retrieved."); 
		
		System.out.println("Listing Historial_Solucion by Criteria...");
		Historial_SolucionCriteria historial_SolucionCriteria = new Historial_SolucionCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//historial_SolucionCriteria.ID.eq();
		historial_SolucionCriteria.setMaxResults(ROW_COUNT);
		Historial_Solucion[] historial_Solucions = historial_SolucionCriteria.listHistorial_Solucion();
		length =historial_Solucions== null ? 0 : Math.min(historial_Solucions.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(historial_Solucions[i]);
		}
		System.out.println(length + " Historial_Solucion record(s) retrieved."); 
		
		System.out.println("Listing Usuario by Criteria...");
		UsuarioCriteria usuarioCriteria = new UsuarioCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//usuarioCriteria.idUsuario.eq();
		usuarioCriteria.setMaxResults(ROW_COUNT);
		Usuario[] usuarios = usuarioCriteria.listUsuario();
		length =usuarios== null ? 0 : Math.min(usuarios.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(usuarios[i]);
		}
		System.out.println(length + " Usuario record(s) retrieved."); 
		
		System.out.println("Listing Barrio by Criteria...");
		BarrioCriteria barrioCriteria = new BarrioCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//barrioCriteria.idBarrio.eq();
		barrioCriteria.setMaxResults(ROW_COUNT);
		Barrio[] barrios = barrioCriteria.listBarrio();
		length =barrios== null ? 0 : Math.min(barrios.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(barrios[i]);
		}
		System.out.println(length + " Barrio record(s) retrieved."); 
		
		System.out.println("Listing Descarga by Criteria...");
		DescargaCriteria descargaCriteria = new DescargaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//descargaCriteria.idDescargas.eq();
		descargaCriteria.setMaxResults(ROW_COUNT);
		Descarga[] descargas = descargaCriteria.listDescarga();
		length =descargas== null ? 0 : Math.min(descargas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(descargas[i]);
		}
		System.out.println(length + " Descarga record(s) retrieved."); 
		
		System.out.println("Listing Tarifa by Criteria...");
		TarifaCriteria tarifaCriteria = new TarifaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//tarifaCriteria.ID.eq();
		tarifaCriteria.setMaxResults(ROW_COUNT);
		Tarifa[] tarifas = tarifaCriteria.listTarifa();
		length =tarifas== null ? 0 : Math.min(tarifas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(tarifas[i]);
		}
		System.out.println(length + " Tarifa record(s) retrieved."); 
		
		System.out.println("Listing Calle by Criteria...");
		CalleCriteria calleCriteria = new CalleCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//calleCriteria.ID.eq();
		calleCriteria.setMaxResults(ROW_COUNT);
		Calle[] calles = calleCriteria.listCalle();
		length =calles== null ? 0 : Math.min(calles.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(calles[i]);
		}
		System.out.println(length + " Calle record(s) retrieved."); 
		
		System.out.println("Listing Recorrido by Criteria...");
		RecorridoCriteria recorridoCriteria = new RecorridoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//recorridoCriteria.ID.eq();
		recorridoCriteria.setMaxResults(ROW_COUNT);
		Recorrido[] recorridos = recorridoCriteria.listRecorrido();
		length =recorridos== null ? 0 : Math.min(recorridos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(recorridos[i]);
		}
		System.out.println(length + " Recorrido record(s) retrieved."); 
		
		System.out.println("Listing Parada_Evento by Criteria...");
		Parada_EventoCriteria parada_EventoCriteria = new Parada_EventoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//parada_EventoCriteria.ID.eq();
		parada_EventoCriteria.setMaxResults(ROW_COUNT);
		Parada_Evento[] parada_Eventos = parada_EventoCriteria.listParada_Evento();
		length =parada_Eventos== null ? 0 : Math.min(parada_Eventos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(parada_Eventos[i]);
		}
		System.out.println(length + " Parada_Evento record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListActividadAppEscritorioServicioAutobusesD2Data listActividadAppEscritorioServicioAutobusesD2Data = new ListActividadAppEscritorioServicioAutobusesD2Data();
			try {
				listActividadAppEscritorioServicioAutobusesD2Data.listTestData();
				//listActividadAppEscritorioServicioAutobusesD2Data.listByCriteria();
			}
			finally {
				ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
