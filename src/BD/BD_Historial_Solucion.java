package BD;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Historial_Solucion {
	public BD_Principal _bd_prin_sol;
	public ArrayList<Historial_Solucion> _cont_historial = new ArrayList<Historial_Solucion>();

	public ArrayList<Historial_Solucion> Cargar_Historial(int aIdUsuario) throws PersistentException {
		ArrayList<Historial_Solucion> Historial_Solucion=new ArrayList<Historial_Solucion>();
		Historial_Solucion[] aux=null;
			 aux = Historial_SolucionDAO.listHistorial_SolucionByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 if (aux[i].getEs_contenida_historial().getIdUsuario()==aIdUsuario){
					 Historial_Solucion.add(aux[i]);
				 }
				 
			}
		
		return Historial_Solucion;
	}

	public void addHistorial(String aOrigen, String aDestino, int idUsuario) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Historial_Solucion as = Historial_SolucionDAO.createHistorial_Solucion();
			Usuario us=UsuarioDAO.getUsuarioByORMID(idUsuario);
			as.setEs_contenida_historial(us);
			MessageDigest md= MessageDigest.getInstance("MD5");
			as.setCodigo(md.digest((aOrigen+aDestino+idUsuario+as.getID()+new Date(System.currentTimeMillis())).getBytes()).toString());
			as.setOrigen(aOrigen);
			as.setDestino(aDestino);
			Date fecha=new Date(System.currentTimeMillis());
			as.setFecha(fecha);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public String compartir(int aId) throws PersistentException {
Historial_Solucion as = Historial_SolucionDAO.getHistorial_SolucionByORMID(aId);
return as.getCodigo();
	}

	public Historial_Solucion cargar_consulta(String aCodigo) throws PersistentException {
		Historial_Solucion Historial_Solucion=null;
		Historial_Solucion[] aux=null;
		
			 aux = Historial_SolucionDAO.listHistorial_SolucionByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				 if (aux[i].getCodigo().equals(aCodigo)){
					 Historial_Solucion=aux[i];
					 break;
				 }
				 
			}
		
		return Historial_Solucion;
	}
}