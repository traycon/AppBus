package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Descarga")
public class Descarga implements Serializable {
	public Descarga() {
	}
	
	@Column(name="IdDescargas", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="DESCARGA_IDDESCARGAS_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="DESCARGA_IDDESCARGAS_GENERATOR", strategy="native")	
	private int idDescargas;
	
	@Column(name="Nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="Descripcion", nullable=true, length=255)	
	private String descripcion;
	
	@Column(name="Tamanio", nullable=false)	
	private double tamanio;
	
	@Column(name="Estado", nullable=true, length=1)	
	private boolean estado;
	
	@Column(name="Version", nullable=true, length=255)	
	private String version;
	
	@Column(name="FechaSubida", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaSubida;
	
	@Column(name="UltimaModificacion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date ultimaModificacion;
	
	private void setIdDescargas(int value) {
		this.idDescargas = value;
	}
	
	public int getIdDescargas() {
		return idDescargas;
	}
	
	public int getORMID() {
		return getIdDescargas();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setTamanio(double value) {
		this.tamanio = value;
	}
	
	public double getTamanio() {
		return tamanio;
	}
	
	public void setEstado(boolean value) {
		this.estado = value;
	}
	
	public boolean getEstado() {
		return estado;
	}
	
	public void setVersion(String value) {
		this.version = value;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setFechaSubida(java.util.Date value) {
		this.fechaSubida = value;
	}
	
	public java.util.Date getFechaSubida() {
		return fechaSubida;
	}
	
	public void setUltimaModificacion(java.util.Date value) {
		this.ultimaModificacion = value;
	}
	
	public java.util.Date getUltimaModificacion() {
		return ultimaModificacion;
	}
	
	public String toString() {
		return String.valueOf(getIdDescargas());
	}
	
}
