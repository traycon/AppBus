package BD;
/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
public class DeleteActividadAppEscritorioServicioAutobusesD2Data {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Parada parada = ParadaDAO.loadParadaByQuery(null, null);
			// Delete the persistent object
			ParadaDAO.delete(parada);
			Linea linea = LineaDAO.loadLineaByQuery(null, null);
			// Delete the persistent object
			LineaDAO.delete(linea);
			Evento evento = EventoDAO.loadEventoByQuery(null, null);
			// Delete the persistent object
			EventoDAO.delete(evento);
			Punto_Interes punto_Interes = Punto_InteresDAO.loadPunto_InteresByQuery(null, null);
			// Delete the persistent object
			Punto_InteresDAO.delete(punto_Interes);
			Historial_Solucion historial_Solucion = Historial_SolucionDAO.loadHistorial_SolucionByQuery(null, null);
			// Delete the persistent object
			Historial_SolucionDAO.delete(historial_Solucion);
			Usuario usuario = UsuarioDAO.loadUsuarioByQuery(null, null);
			// Delete the persistent object
			UsuarioDAO.delete(usuario);
			Barrio barrio = BarrioDAO.loadBarrioByQuery(null, null);
			// Delete the persistent object
			BarrioDAO.delete(barrio);
			Descarga descarga = DescargaDAO.loadDescargaByQuery(null, null);
			// Delete the persistent object
			DescargaDAO.delete(descarga);
			Tarifa tarifa = TarifaDAO.loadTarifaByQuery(null, null);
			// Delete the persistent object
			TarifaDAO.delete(tarifa);
			Calle calle = CalleDAO.loadCalleByQuery(null, null);
			// Delete the persistent object
			CalleDAO.delete(calle);
			Recorrido recorrido = RecorridoDAO.loadRecorridoByQuery(null, null);
			// Delete the persistent object
			RecorridoDAO.delete(recorrido);
			Parada_Evento parada_Evento = Parada_EventoDAO.loadParada_EventoByQuery(null, null);
			// Delete the persistent object
			Parada_EventoDAO.delete(parada_Evento);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteActividadAppEscritorioServicioAutobusesD2Data deleteActividadAppEscritorioServicioAutobusesD2Data = new DeleteActividadAppEscritorioServicioAutobusesD2Data();
			try {
				deleteActividadAppEscritorioServicioAutobusesD2Data.deleteTestData();
			}
			finally {
				ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
