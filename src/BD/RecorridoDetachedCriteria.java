package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class RecorridoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final DateExpression hora;
	public final IntegerExpression orden;
	public final IntegerExpression lineaId;
	public final AssociationExpression linea;
	public final IntegerExpression paradaId;
	public final AssociationExpression parada;
	
	public RecorridoDetachedCriteria() {
		super(Recorrido.class, RecorridoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		hora = new DateExpression("hora", this.getDetachedCriteria());
		orden = new IntegerExpression("orden", this.getDetachedCriteria());
		lineaId = new IntegerExpression("ORM_Linea.null", this.getDetachedCriteria());
		linea = new AssociationExpression("ORM_Linea", this.getDetachedCriteria());
		paradaId = new IntegerExpression("ORM_Parada.null", this.getDetachedCriteria());
		parada = new AssociationExpression("ORM_Parada", this.getDetachedCriteria());
	}
	
	public RecorridoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, RecorridoCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		hora = new DateExpression("hora", this.getDetachedCriteria());
		orden = new IntegerExpression("orden", this.getDetachedCriteria());
		lineaId = new IntegerExpression("ORM_Linea.null", this.getDetachedCriteria());
		linea = new AssociationExpression("ORM_Linea", this.getDetachedCriteria());
		paradaId = new IntegerExpression("ORM_Parada.null", this.getDetachedCriteria());
		parada = new AssociationExpression("ORM_Parada", this.getDetachedCriteria());
	}
	
	public LineaDetachedCriteria createLineaCriteria() {
		return new LineaDetachedCriteria(createCriteria("linea"));
	}
	
	public ParadaDetachedCriteria createParadaCriteria() {
		return new ParadaDetachedCriteria(createCriteria("parada"));
	}
	
	public Recorrido uniqueRecorrido(PersistentSession session) {
		return (Recorrido) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Recorrido[] listRecorrido(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Recorrido[]) list.toArray(new Recorrido[list.size()]);
	}
}

