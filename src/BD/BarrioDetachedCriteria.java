package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BarrioDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idBarrio;
	public final StringExpression nombre;
	public final CollectionExpression contiene_barrios;
	public final CollectionExpression contiene;
	
	public BarrioDetachedCriteria() {
		super(Barrio.class, BarrioCriteria.class);
		idBarrio = new IntegerExpression("idBarrio", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		contiene_barrios = new CollectionExpression("ORM_contiene_barrios", this.getDetachedCriteria());
		contiene = new CollectionExpression("ORM_contiene", this.getDetachedCriteria());
	}
	
	public BarrioDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, BarrioCriteria.class);
		idBarrio = new IntegerExpression("idBarrio", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		contiene_barrios = new CollectionExpression("ORM_contiene_barrios", this.getDetachedCriteria());
		contiene = new CollectionExpression("ORM_contiene", this.getDetachedCriteria());
	}
	
	public ParadaDetachedCriteria createContiene_barriosCriteria() {
		return new ParadaDetachedCriteria(createCriteria("ORM_contiene_barrios"));
	}
	
	public CalleDetachedCriteria createContieneCriteria() {
		return new CalleDetachedCriteria(createCriteria("ORM_contiene"));
	}
	
	public Barrio uniqueBarrio(PersistentSession session) {
		return (Barrio) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Barrio[] listBarrio(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Barrio[]) list.toArray(new Barrio[list.size()]);
	}
}

