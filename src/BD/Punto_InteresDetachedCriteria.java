package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Punto_InteresDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idPuntoInteres;
	public final IntegerExpression son_contenidosId;
	public final AssociationExpression son_contenidos;
	public final StringExpression nombre;
	public final StringExpression lugar;
	public final DoubleExpression precio;
	public final DateExpression horaIni;
	public final DateExpression horaFin;
	public final StringExpression descripcion;
	public final StringExpression dirFoto;
	public final CollectionExpression contiene;
	
	public Punto_InteresDetachedCriteria() {
		super(Punto_Interes.class, Punto_InteresCriteria.class);
		idPuntoInteres = new IntegerExpression("idPuntoInteres", this.getDetachedCriteria());
		son_contenidosId = new IntegerExpression("son_contenidos.idParadas", this.getDetachedCriteria());
		son_contenidos = new AssociationExpression("son_contenidos", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		lugar = new StringExpression("lugar", this.getDetachedCriteria());
		precio = new DoubleExpression("precio", this.getDetachedCriteria());
		horaIni = new DateExpression("horaIni", this.getDetachedCriteria());
		horaFin = new DateExpression("horaFin", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		dirFoto = new StringExpression("dirFoto", this.getDetachedCriteria());
		contiene = new CollectionExpression("ORM_contiene", this.getDetachedCriteria());
	}
	
	public Punto_InteresDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, Punto_InteresCriteria.class);
		idPuntoInteres = new IntegerExpression("idPuntoInteres", this.getDetachedCriteria());
		son_contenidosId = new IntegerExpression("son_contenidos.idParadas", this.getDetachedCriteria());
		son_contenidos = new AssociationExpression("son_contenidos", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		lugar = new StringExpression("lugar", this.getDetachedCriteria());
		precio = new DoubleExpression("precio", this.getDetachedCriteria());
		horaIni = new DateExpression("horaIni", this.getDetachedCriteria());
		horaFin = new DateExpression("horaFin", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		dirFoto = new StringExpression("dirFoto", this.getDetachedCriteria());
		contiene = new CollectionExpression("ORM_contiene", this.getDetachedCriteria());
	}
	
	public ParadaDetachedCriteria createSon_contenidosCriteria() {
		return new ParadaDetachedCriteria(createCriteria("son_contenidos"));
	}
	
	public EventoDetachedCriteria createContieneCriteria() {
		return new EventoDetachedCriteria(createCriteria("ORM_contiene"));
	}
	
	public Punto_Interes uniquePunto_Interes(PersistentSession session) {
		return (Punto_Interes) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Punto_Interes[] listPunto_Interes(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Punto_Interes[]) list.toArray(new Punto_Interes[list.size()]);
	}
}

