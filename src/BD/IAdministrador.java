package BD;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;



public interface IAdministrador extends Remote {

	public ArrayList<Descarga> Cargar_descargas()throws RemoteException;

	public Descarga descargar(int aIdDescargas)throws RemoteException;

	public void addDescarga(String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación)throws RemoteException;

	public void modDescarga(int aIdDescargas, String aNombre, String aDescripcion, double aTamanio, boolean aEstado, String aVersion, Date aFechaSubida, Date aUltimaModificación)throws RemoteException;

	public void delDescarga(int aIdDescargas)throws RemoteException;

	public Usuario Cargar_Configuracion(int aIdUsuario)throws RemoteException;

	public void GuardarConfig(int aIdUsuario, String oldPass,String newPass)throws RemoteException;

	public ArrayList<Usuario> Cargar_Usuarios()throws RemoteException;

	public void addUsuario(String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador)throws RemoteException;

	public void modUsuario(int aIdUsuario, String aNombre, String aApellidos, String aCorreo, String aPassword, int aTelefono, String aDireccion, int aCp, String aIdBono, Date aValidezBono, boolean aAdministrador)throws RemoteException;

	public void delUsuario(int aIdUsuario)throws RemoteException;

	public void Recargar(String aIdBono, double aCantidad)throws RemoteException;

	public void cambiarValidezBono(int aIdUsuario, Date aFecha)throws RemoteException;

	public ArrayList<Tarifa> Cargar_Tarifas()throws RemoteException;

	public void addTarifa(String aTipoTarifa, double aPrecio, boolean aBono)throws RemoteException;

	public void modTarifa(int aId, String aTipoTarifa, double aPrecio, boolean aBono)throws RemoteException;

	public void delTarifa(int aIdTarifa)throws RemoteException;

	public ArrayList<Historial_Solucion> Cargar_Historial(int aIdUsuario)throws RemoteException;

	public void addHistorial(String aOrigen, String aDestino, int idUsuario)throws RemoteException;

	public String compartir(int aId)throws RemoteException;

	public Historial_Solucion cargar_consulta(String aCodigo)throws RemoteException;

	public ArrayList<Barrio> cargar_Barrios()throws RemoteException;

	public void modBarrio(int aIdBarrio, String aNombre)throws RemoteException;

	public void addBarrio(String aNombre)throws RemoteException;

	public void delBarrio(int aIdBarrio)throws RemoteException;

	public Barrio cargar_Barrio(int aIdBarrio)throws RemoteException;

	public void agregarParadas_Barrio(int aIdBarrio, int aIdParada)throws RemoteException;

	public void quitarParadas_Barrio(int aIdParada)throws RemoteException;

	public ArrayList<Calle> cargar_calles(int aIdBarrio)throws RemoteException;

	public void addCalle(String aDireccion, int aBarrio)throws RemoteException;

	public void modCalle(int aIdCalle, String aDireccion)throws RemoteException;

	public void delCalle(int aIdCalle)throws RemoteException;

	public void modParada(int aIdParada, String aNombre, String aDireccion, String aDirFoto)throws RemoteException;

	public void addParada(String aNombre, String aDireccion, String aDirFoto)throws RemoteException;

	public void agregarEventoParada(int aIdEvento, int aIdParada)throws RemoteException;

	public void quitarEventoParada(int aIdEvento, int aIdParada)throws RemoteException;

	public ArrayList<Parada> Cargar_Paradas()throws RemoteException;

	public void delParada(int aIdParada)throws RemoteException;

	public Parada Cargar_Parada(int aIdParada)throws RemoteException;

	public void subirFoto(int aIdParada, String aDirFoto)throws RemoteException;

	public void agregarLineasParada(int aIdParada, int aIdLinea, Recorrido aRecorrido)throws RemoteException;

	public void quitarLineasParada(int aIdParada, int aIdLinea)throws RemoteException;

	public void agregarPuntosInteresParada(int aIdPuntoInteres, int aIdParada)throws RemoteException;

	public void quitarPuntosInteresParada(int aIdPuntoInteres, int aIdParada)throws RemoteException;

	public ArrayList<Linea> Cargar_lineas()throws RemoteException;

	public void addLinea(String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa)throws RemoteException;

	public void modLinea(int aIdLinea, String aNombre, int aNumero, int aParadaIni, Date aFechaIni, String aNombreParadaIni, int aParadaFin, Date aFechaFin, String aNombreParadaFin, String aObservaciones, double aTarifa, String aDirMapa)throws RemoteException;

	public void delLinea(int aIdLinea)throws RemoteException;

	public Linea Cargar_linea(int aIdLinea)throws RemoteException;

	public void addMapa(String aDirMapa, int aIdLinea)throws RemoteException;

	public void agregarParadaLinea(int aIdParada, int aIdLinea, Recorrido aRecorrido)throws RemoteException;

	public void quitarParadaLinea(int aIdParada, int aIdLinea)throws RemoteException;

	public ArrayList<Punto_Interes> Cargar_PuntosInteres()throws RemoteException;

	public void addPunto_Interes(String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto)throws RemoteException;

	public void modPunto_Interes(int aIdPuntoInteres, String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto)throws RemoteException;

	public void delPunto_Interes(int aIdPuntoInteres)throws RemoteException;

	public Punto_Interes Cargar_PuntosInteres(int aIdPuntoInteres)throws RemoteException;

	public void subirFoto2(int aIdPuntoInteres, String aDirFoto)throws RemoteException;

	public void agregarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres)throws RemoteException;

	public void quitarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres)throws RemoteException;

	public ArrayList<Evento> Cargar_Eventos()throws RemoteException;

	public void addEvento(String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel)throws RemoteException;

	public void modEvento(int aIdEvento, String aNombre, String aLugar, Date aFechaIni, Date aFechaFin, String aDescripcion, String aDirCartel)throws RemoteException;

	public void delEvento(int aIdEvento)throws RemoteException;

	public Evento Cargar_Evento(int aIdEvento)throws RemoteException;

	public void agregarParada_LineaEvento(int aIdEvento, int aIdParadaLinea)throws RemoteException;

	public void quitarParada_LineaEvento(int aIdEvento, int aIdParadaLinea)throws RemoteException;

	public Parada[] cargar_ParadaLinea(int ormid) throws RemoteException;

	public Parada[] cargar_ParadasEvento(int ormid)throws RemoteException;

	public ArrayList<Linea> cargar_LineasParada(int ormid)throws RemoteException;

	public Evento[] cargar_Eventos_PuntosInteres(int ormid)throws RemoteException;

	public Evento[] cargar_EventosParada(int ormid)throws RemoteException;

	public Punto_Interes[] cargar_PuntosInteresParada(int ormid)throws RemoteException;
	
	public Recorrido recorridoParadaLinea(int idParada,Linea aLinea) throws RemoteException;
}
