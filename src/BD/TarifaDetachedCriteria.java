package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TarifaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression vinculada_aId;
	public final AssociationExpression vinculada_a;
	public final StringExpression tipoTarifa;
	public final DoubleExpression precio;
	public final BooleanExpression bono;
	
	public TarifaDetachedCriteria() {
		super(Tarifa.class, TarifaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		vinculada_aId = new IntegerExpression("vinculada_a.idUsuario", this.getDetachedCriteria());
		vinculada_a = new AssociationExpression("vinculada_a", this.getDetachedCriteria());
		tipoTarifa = new StringExpression("tipoTarifa", this.getDetachedCriteria());
		precio = new DoubleExpression("precio", this.getDetachedCriteria());
		bono = new BooleanExpression("bono", this.getDetachedCriteria());
	}
	
	public TarifaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, TarifaCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		vinculada_aId = new IntegerExpression("vinculada_a.idUsuario", this.getDetachedCriteria());
		vinculada_a = new AssociationExpression("vinculada_a", this.getDetachedCriteria());
		tipoTarifa = new StringExpression("tipoTarifa", this.getDetachedCriteria());
		precio = new DoubleExpression("precio", this.getDetachedCriteria());
		bono = new BooleanExpression("bono", this.getDetachedCriteria());
	}
	
	public UsuarioDetachedCriteria createVinculada_aCriteria() {
		return new UsuarioDetachedCriteria(createCriteria("vinculada_a"));
	}
	
	public Tarifa uniqueTarifa(PersistentSession session) {
		return (Tarifa) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Tarifa[] listTarifa(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Tarifa[]) list.toArray(new Tarifa[list.size()]);
	}
}

