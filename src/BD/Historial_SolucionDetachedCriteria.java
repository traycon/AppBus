package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Historial_SolucionDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression es_contenida_historialId;
	public final AssociationExpression es_contenida_historial;
	public final StringExpression codigo;
	public final StringExpression origen;
	public final StringExpression destino;
	public final DateExpression fecha;
	
	public Historial_SolucionDetachedCriteria() {
		super(Historial_Solucion.class, Historial_SolucionCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		es_contenida_historialId = new IntegerExpression("es_contenida_historial.idUsuario", this.getDetachedCriteria());
		es_contenida_historial = new AssociationExpression("es_contenida_historial", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		origen = new StringExpression("origen", this.getDetachedCriteria());
		destino = new StringExpression("destino", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
	}
	
	public Historial_SolucionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, Historial_SolucionCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		es_contenida_historialId = new IntegerExpression("es_contenida_historial.idUsuario", this.getDetachedCriteria());
		es_contenida_historial = new AssociationExpression("es_contenida_historial", this.getDetachedCriteria());
		codigo = new StringExpression("codigo", this.getDetachedCriteria());
		origen = new StringExpression("origen", this.getDetachedCriteria());
		destino = new StringExpression("destino", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
	}
	
	public UsuarioDetachedCriteria createEs_contenida_historialCriteria() {
		return new UsuarioDetachedCriteria(createCriteria("es_contenida_historial"));
	}
	
	public Historial_Solucion uniqueHistorial_Solucion(PersistentSession session) {
		return (Historial_Solucion) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Historial_Solucion[] listHistorial_Solucion(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Historial_Solucion[]) list.toArray(new Historial_Solucion[list.size()]);
	}
}

