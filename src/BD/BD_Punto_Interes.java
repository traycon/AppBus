package BD;

import java.util.ArrayList;
import java.util.Date;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class BD_Punto_Interes {
	public BD_Principal _bd_prin_pi;
	public ArrayList<Punto_Interes> _cont_P_Interes = new ArrayList<Punto_Interes>();

	public ArrayList<Punto_Interes> Cargar_PuntosInteres() throws PersistentException {
		ArrayList<Punto_Interes> puntos=new ArrayList<Punto_Interes>();
		Punto_Interes[] aux=null;

			 aux = Punto_InteresDAO.listPunto_InteresByQuery(null,null);
			 for (int i = 0; i < aux.length; i++) {
				puntos.add(aux[i]);
			}	
		
		return puntos;
	}

	public void addPunto_Interes(String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Punto_Interes as = Punto_InteresDAO.createPunto_Interes();
			as.setNombre(aNombre);
			as.setLugar(aLugar);
			as.setPrecio(aPrecio);
			as.setHoraIni(aHoraIni);
			as.setHoraFin(aHoraFin);
			as.setDescripcion(aDescripcion);
			as.setDirFoto(aDirFoto);
			Punto_InteresDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void modPunto_Interes(int aIdPuntoInteres, String aNombre, String aLugar, double aPrecio, Date aHoraIni, Date aHoraFin, String aDescripcion, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Punto_Interes as = Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			as.setNombre(aNombre);
			as.setLugar(aLugar);
			as.setPrecio(aPrecio);
			as.setHoraIni(aHoraIni);
			as.setHoraFin(aHoraFin);
			as.setDescripcion(aDescripcion);
			as.setDirFoto(aDirFoto);
			Punto_InteresDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void delPunto_Interes(int aIdPuntoInteres) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Punto_Interes as = Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			Punto_InteresDAO.deleteAndDissociate(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Punto_Interes Cargar_PuntosInteres(int aIdPuntoInteres) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		Punto_Interes as=null;
		try {
			 
			as = Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
		}
		catch (Exception e) {
			t.rollback();
		}
		
		return as;
	}

	public void subirFoto(int aIdPuntoInteres, String aDirFoto) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Punto_Interes as = Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			as.setDirFoto(aDirFoto);
			Punto_InteresDAO.save(as);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void agregarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			 
			Punto_Interes as = Punto_InteresDAO.getPunto_InteresByORMID(aIdPuntoInteres);
			Evento ev=EventoDAO.getEventoByORMID(aIdEvento);
			ev.setSon_contenidos(as);
			EventoDAO.save(ev);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public void quitarEventoPunto_Interes(int aIdEvento, int aIdPuntoInteres) throws PersistentException {
		PersistentTransaction t = ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession().beginTransaction();
		try {
			Evento ev=EventoDAO.getEventoByORMID(aIdEvento);
			ev.setSon_contenidos(null);
			EventoDAO.save(ev);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
	}

	public Evento[] cargar_Eventos_PuntosInteres(int ormid) {
		ArrayList<Evento> events=new ArrayList<Evento>();
		try {
			Evento[] as=EventoDAO.listEventoByQuery(null, null);
			for (int i = 0; i < as.length; i++) {
				if (as[i].getSon_contenidos()!=null){
				if(as[i].getSon_contenidos().getORMID()==ormid){
					events.add(as[i]);
				}
				}
			}
			as=new Evento[events.size()];
			for (int i = 0; i < events.size(); i++) {
				as[i]=events.get(i);
			}
			return as;
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}