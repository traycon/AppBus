package BD;
/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.orm.*;
public class CreateActividadAppEscritorioServicioAutobusesD2DatabaseSchema {
	public static void main(String[] args) {
		try {
			ORMDatabaseInitiator.createSchema(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance());
			ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().disposePersistentManager();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
