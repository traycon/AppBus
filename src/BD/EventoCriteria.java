package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EventoCriteria extends AbstractORMCriteria {
	public final IntegerExpression idEventos;
	public final IntegerExpression son_contenidosId;
	public final AssociationExpression son_contenidos;
	public final StringExpression nombre;
	public final StringExpression lugar;
	public final DateExpression fechaIni;
	public final DateExpression fechaFin;
	public final StringExpression descripcion;
	public final StringExpression dirCartel;
	public final CollectionExpression contienen_eventos;
	
	public EventoCriteria(Criteria criteria) {
		super(criteria);
		idEventos = new IntegerExpression("idEventos", this);
		son_contenidosId = new IntegerExpression("son_contenidos.idPuntoInteres", this);
		son_contenidos = new AssociationExpression("son_contenidos", this);
		nombre = new StringExpression("nombre", this);
		lugar = new StringExpression("lugar", this);
		fechaIni = new DateExpression("fechaIni", this);
		fechaFin = new DateExpression("fechaFin", this);
		descripcion = new StringExpression("descripcion", this);
		dirCartel = new StringExpression("dirCartel", this);
		contienen_eventos = new CollectionExpression("ORM_contienen_eventos", this);
	}
	
	public EventoCriteria(PersistentSession session) {
		this(session.createCriteria(Evento.class));
	}
	
	public EventoCriteria() throws PersistentException {
		this(ActividadAppEscritorioServicioAutobusesD2PersistentManager.instance().getSession());
	}
	
	public Punto_InteresCriteria createSon_contenidosCriteria() {
		return new Punto_InteresCriteria(createCriteria("son_contenidos"));
	}
	
	public Parada_EventoCriteria createContienen_eventosCriteria() {
		return new Parada_EventoCriteria(createCriteria("ORM_contienen_eventos"));
	}
	
	public Evento uniqueEvento() {
		return (Evento) super.uniqueResult();
	}
	
	public Evento[] listEvento() {
		java.util.List list = super.list();
		return (Evento[]) list.toArray(new Evento[list.size()]);
	}
}

