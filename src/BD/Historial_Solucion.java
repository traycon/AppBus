package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="Historial_Solucion")
public class Historial_Solucion implements Serializable {
	public Historial_Solucion() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == ORMConstants.KEY_HISTORIAL_SOLUCION_ES_CONTENIDA_HISTORIAL) {
			this.es_contenida_historial = (Usuario) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="ID", nullable=false, length=11)	
	@Id	
	@GeneratedValue(generator="HISTORIAL_SOLUCION_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="HISTORIAL_SOLUCION_ID_GENERATOR", strategy="native")	
	private int ID;
	
	@ManyToOne(targetEntity=Usuario.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="UsuarioIdUsuario", referencedColumnName="IdUsuario", nullable=false) })	
	private Usuario es_contenida_historial;
	
	@Column(name="Codigo", nullable=true, length=255)	
	private String codigo;
	
	@Column(name="Origen", nullable=true, length=255)	
	private String origen;
	
	@Column(name="Destino", nullable=true, length=255)	
	private String destino;
	
	@Column(name="Fecha", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fecha;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setCodigo(String value) {
		this.codigo = value;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setOrigen(String value) {
		this.origen = value;
	}
	
	public String getOrigen() {
		return origen;
	}
	
	public void setDestino(String value) {
		this.destino = value;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setFecha(java.util.Date value) {
		this.fecha = value;
	}
	
	public java.util.Date getFecha() {
		return fecha;
	}
	
	public void setEs_contenida_historial(Usuario value) {
		if (es_contenida_historial != null) {
			es_contenida_historial.contiene_usuario.remove(this);
		}
		if (value != null) {
			value.contiene_usuario.add(this);
		}
	}
	
	public Usuario getEs_contenida_historial() {
		return es_contenida_historial;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Es_contenida_historial(Usuario value) {
		this.es_contenida_historial = value;
	}
	
	private Usuario getORM_Es_contenida_historial() {
		return es_contenida_historial;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
