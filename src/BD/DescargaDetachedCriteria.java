package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DescargaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression idDescargas;
	public final StringExpression nombre;
	public final StringExpression descripcion;
	public final DoubleExpression tamanio;
	public final BooleanExpression estado;
	public final StringExpression version;
	public final DateExpression fechaSubida;
	public final DateExpression ultimaModificacion;
	
	public DescargaDetachedCriteria() {
		super(Descarga.class, DescargaCriteria.class);
		idDescargas = new IntegerExpression("idDescargas", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		tamanio = new DoubleExpression("tamanio", this.getDetachedCriteria());
		estado = new BooleanExpression("estado", this.getDetachedCriteria());
		version = new StringExpression("version", this.getDetachedCriteria());
		fechaSubida = new DateExpression("fechaSubida", this.getDetachedCriteria());
		ultimaModificacion = new DateExpression("ultimaModificacion", this.getDetachedCriteria());
	}
	
	public DescargaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, DescargaCriteria.class);
		idDescargas = new IntegerExpression("idDescargas", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		tamanio = new DoubleExpression("tamanio", this.getDetachedCriteria());
		estado = new BooleanExpression("estado", this.getDetachedCriteria());
		version = new StringExpression("version", this.getDetachedCriteria());
		fechaSubida = new DateExpression("fechaSubida", this.getDetachedCriteria());
		ultimaModificacion = new DateExpression("ultimaModificacion", this.getDetachedCriteria());
	}
	
	public Descarga uniqueDescarga(PersistentSession session) {
		return (Descarga) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Descarga[] listDescarga(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Descarga[]) list.toArray(new Descarga[list.size()]);
	}
}

