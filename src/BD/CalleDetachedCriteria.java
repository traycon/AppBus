package BD;
/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CalleDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression es_contenidaId;
	public final AssociationExpression es_contenida;
	public final StringExpression direccion;
	
	public CalleDetachedCriteria() {
		super(Calle.class, CalleCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		es_contenidaId = new IntegerExpression("es_contenida.idBarrio", this.getDetachedCriteria());
		es_contenida = new AssociationExpression("es_contenida", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
	}
	
	public CalleDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, CalleCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		es_contenidaId = new IntegerExpression("es_contenida.idBarrio", this.getDetachedCriteria());
		es_contenida = new AssociationExpression("es_contenida", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
	}
	
	public BarrioDetachedCriteria createEs_contenidaCriteria() {
		return new BarrioDetachedCriteria(createCriteria("es_contenida"));
	}
	
	public Calle uniqueCalle(PersistentSession session) {
		return (Calle) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Calle[] listCalle(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Calle[]) list.toArray(new Calle[list.size()]);
	}
}

