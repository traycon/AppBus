import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Modelo_asignacion {
	private JTable tablaTotal;
	private JTable tablaFiltrada;
	private JButton agregarB;
	private JButton quitarB;
	private JScrollPane scrollPaneT;
	private JScrollPane scrollPaneF;
	public Modelo_asignacion(){
		scrollPaneT=new JScrollPane();
		tablaTotal=new JTable();
		tablaTotal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneT.setViewportView(tablaTotal);
		
		scrollPaneF=new JScrollPane();
		tablaFiltrada=new JTable();
		tablaFiltrada.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneF.setViewportView(tablaFiltrada);
		
		agregarB= new JButton("/\\");
		agregarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregar();
			}
		});
		
		quitarB = new JButton("\\/");
		quitarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitar();
			}
		});
		
	}
	public void agregar() {
	}

	public void quitar() {
	}
	public JTable getTablaTotal() {
		return tablaTotal;
	}
	public void setTablaTotal(JTable tablaTotal) {
		this.tablaTotal = tablaTotal;
	}
	public JTable getTablaFiltrada() {
		return tablaFiltrada;
	}
	public void setTablaFiltrada(JTable tablaFiltrada) {
		this.tablaFiltrada = tablaFiltrada;
	}
	public JButton getAgregarB() {
		return agregarB;
	}
	public void setAgregarB(JButton agregarB) {
		this.agregarB = agregarB;
	}
	public JButton getQuitarB() {
		return quitarB;
	}
	public void setQuitarB(JButton quitarB) {
		this.quitarB = quitarB;
	}
	public JScrollPane getScrollPaneT() {
		return scrollPaneT;
	}
	public void setScrollPaneT(JScrollPane scrollPaneT) {
		this.scrollPaneT = scrollPaneT;
	}
	public JScrollPane getScrollPaneF() {
		return scrollPaneF;
	}
	public void setScrollPaneF(JScrollPane scrollPaneF) {
		this.scrollPaneF = scrollPaneF;
	}
}