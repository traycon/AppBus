import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BD.Barrio;
import BD.IAdministrador;
import BD.Parada;

public class Zona_Paradas_Barrios extends Modelo_asignacion {
	private ArrayList<Parada> us;
	private ArrayList<Parada> paradasBarrio;
	public Zona_Paradas_Barrios() {
		this.getAgregarB().setBounds(54, 217, 41, 26);
		this.getQuitarB().setBounds(103, 217, 41, 26);
		this.getScrollPaneF().setBounds(12, 76, 189, 132);
		this.getScrollPaneT().setBounds(12, 253, 189, 132);
			actualizarTT();
		for (int c = 0; c < this.getTablaTotal().getColumnCount(); c++)
		{
		     Class col_class = this.getTablaTotal().getColumnClass(c);
		     this.getTablaTotal().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
			actualizarTF();
		for (int c = 0; c < this.getTablaFiltrada().getColumnCount(); c++)
		{
		     Class col_class = this.getTablaFiltrada().getColumnClass(c);
		     this.getTablaFiltrada().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
	}
	public void actualizarTF(){
		Object[][] usTable = new Object[0][1];
		if(Barrios__2.BarrioOrmIndex().getSelectedIndex()>-1){
			ArrayList<Parada> paradas=new ArrayList<Parada>();
			try {
				paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 paradasBarrio=new ArrayList<Parada>();
			for (int i = 0; i < paradas.size(); i++) {
				if(paradas.get(i).getEs_contenida_paradas()!=null&&paradas.get(i).getEs_contenida_paradas().getORMID()==Barrios__2.getUsClone().get(Barrios__2.BarrioOrmIndex().getSelectedIndex()).getORMID()){
					paradasBarrio.add(paradas.get(i));
				}
			}
				if(!paradasBarrio.isEmpty()){
					usTable=new Object[paradasBarrio.size()][1];
						for (int i = 0; i < paradasBarrio.size(); i++) {
							
							usTable[i][0]=paradasBarrio.get(i).getDireccion();
							
						}
					}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(
			usTable,
			new String[] {
				"Paradas"
			}
		));
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
	}
public void actualizarTT(){
	Object[][] usTable = new Object[0][1];
	if(Barrios__2.BarrioOrmIndex().getSelectedIndex()>-1){
	try {
		us = MainAplicacion.getBdAdministrador().Cargar_Paradas();
		if (us!=null){
			ArrayList<Parada> paradas = us;
			for (int i = 0; i < paradas.size(); i++) {
				if(paradas.get(i).getEs_contenida_paradas()!=null&&paradas.get(i).getEs_contenida_paradas().getORMID()==Barrios__2.getUsClone().get(Barrios__2.BarrioOrmIndex().getSelectedIndex()).getORMID()){
					us.remove(paradas.get(i));
				}
			}
		usTable=new Object[us.size()][1];
		for (int i = 0; i < us.size(); i++) {
			
			usTable[i][0]=us.get(i).getDireccion();
			
		}
		}
	} catch (RemoteException e) {
		e.printStackTrace();
	}
	}
	this.getTablaTotal().setModel(new DefaultTableModel(
		usTable,
		new String[] {
			"Paradas"
		}
	));
	
	this.getScrollPaneT().setViewportView(this.getTablaTotal());
	this.getScrollPaneT().revalidate();
	this.getScrollPaneT().repaint();
	}
	@Override
	public void agregar(){
		try {
			MainAplicacion.getBdAdministrador().agregarParadas_Barrio(Barrios__2.getUsClone().get(Barrios__2.BarrioOrmIndex().getSelectedIndex()).getORMID(), us.get(this.getTablaTotal().getSelectedRow()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
	@Override
	public void quitar(){
		try {
			MainAplicacion.getBdAdministrador().quitarParadas_Barrio(paradasBarrio.get(this.getTablaFiltrada().getSelectedRow()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
}