import javax.swing.*;

public class idConsulta {
	private JLabel idL;
	private JTextField idTF;
	public idConsulta(){
		this.idL = new JLabel("C\u00F3digo Consulta :");
		this.idL.setBounds(40, 30, 100, 16);
		this.idTF = new JTextField();
		this.idTF.setBounds(158, 28, 265, 20);
		this.idTF.setColumns(10);
	}
	public JLabel getIdL() {
		return idL;
	}
	public void setIdL(JLabel idL) {
		this.idL = idL;
	}
	public JTextField getIdTF() {
		return idTF;
	}
	public void setIdTF(JTextField idTF) {
		this.idTF = idTF;
	}
}