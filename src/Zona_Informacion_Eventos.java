import java.awt.Image;
import java.awt.SystemColor;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Linea;
import BD.Parada;

public class Zona_Informacion_Eventos {
	private JEditorPane cartel;
	private JTable lineasParadasTable;
	private JLabel descripcionL;
	private JTextArea descripcionTA;
	private JLabel cartelL;
	private JScrollPane scrollPane;
	public String sourceCartel;
	public Zona_Informacion_Eventos(){
		cartelL = new JLabel("Cartel :");
		cartelL.setBounds(644, 17, 55, 16);
		sourceCartel="";//a modo de ejemplo, los carteles los tomaremos de la ruta en la BD
		cartel = new JEditorPane("text/html",
	            "<html><img src='"+sourceCartel+"' width=268height=161></img>");
		cartel.setBounds(644, 36, 268, 161);
		cartel.setEditable(false);
		
		descripcionL= new JLabel("Descripci\u00F3n :");
		descripcionL.setBounds(644, 209, 88, 16);
		
		descripcionTA = new JTextArea();
		descripcionTA.setBounds(651, 229, 260, 56);
		descripcionTA.setEditable(false);
		descripcionTA.setOpaque(false);
		
		
		scrollPane= new JScrollPane();
		scrollPane.setBounds(644, 300, 268, 190);
	}
	public JEditorPane getCartel() {
		return cartel;
	}
	public void setCartel(JEditorPane cartel) {
		this.cartel = cartel;
	}
	public JTable getLineasParadasTable() {
		return lineasParadasTable;
	}
	public void setLineasParadasTable(JTable lineasParadasTable) {
		this.lineasParadasTable = lineasParadasTable;
	}
	public JLabel getDescripcionL() {
		return descripcionL;
	}
	public void setDescripcionL(JLabel descripcionL) {
		this.descripcionL = descripcionL;
	}
	public JTextArea getDescripcionTA() {
		return descripcionTA;
	}
	public void setDescripcionTA(JTextArea descripcionTA) {
		this.descripcionTA = descripcionTA;
	}
	public JLabel getCartelL() {
		return cartelL;
	}
	public void setCartelL(JLabel cartelL) {
		this.cartelL = cartelL;
	}
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	public void actualizar(boolean b) {
		Object[][] usTable=new Object[1][3];
		if(Informacion_Eventos.EventoOrmIndex()!=null){
			Parada[] paradas = null;
			try {
				paradas = MainAplicacion.getBdAdministrador().cargar_ParadasEvento(Informacion_Eventos.EventoOrmIndex().getORMID());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(paradas!=null){
					usTable=new Object[paradas.length][3];
						for (int i = 0; i < paradas.length; i++) {
							ArrayList<Linea> lineas=null;
							try {
								lineas=MainAplicacion.getBdAdministrador().cargar_LineasParada(paradas[i].getORMID());
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (lineas!=null){
								for (int j = 0; j < lineas.size(); j++) {
									usTable[i][0]=paradas[i].getORMID();
									usTable[i][1]=paradas[i].getDireccion();
									usTable[i][2]="Linea "+lineas.get(j).getORMID();
								}
							}
							
						}
					}
		}
		lineasParadasTable = new JTable();
		lineasParadasTable.setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Direccion","Linea"}));
		if (b){
		for (int c = 0; c <
				 lineasParadasTable.getColumnCount(); c++) 
		{
			Class col_class =
			lineasParadasTable.getColumnClass(c);
			lineasParadasTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}
		lineasParadasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(lineasParadasTable);
		scrollPane.revalidate();
		scrollPane.repaint();
		
	}
}