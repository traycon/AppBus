import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import maps.java.Route;

import BD.Parada;
import BD.Recorrido;

public class Zona_Informacion_Lineas_admin extends Modelo_asignacion {
	
	public Zona_Informacion_Lineas_admin(){
		
		this.getAgregarB().setText("<");
		this.getAgregarB().setBounds(680, 383, 48, 23);
		this.getQuitarB().setText(">");
		this.getQuitarB().setBounds(680, 417, 48, 23);
		
		this.getScrollPaneF().setBounds(491, 301, 179, 192);
		this.getScrollPaneT().setBounds(738, 301, 179, 192);
		actualizarTT();
		actualizarTF();
	}
public int getIndexLinea(){
	if(Informacion_Lineas_admin.line.getLineasCB().getSelectedIndex()>-1){
	StringTokenizer linea=new StringTokenizer(Informacion_Lineas_admin.line.getLineasCB().getSelectedItem().toString(), " ");
	linea.nextToken();
	return Integer.parseInt(linea.nextToken());
	}
	return -1;
}
	void actualizarTF() {
		Object[][] usTable=new Object[0][3];
		if(getIndexLinea()>-1){
			Parada[] paradas = null;
			try {
				paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(getIndexLinea());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(paradas!=null){
					usTable=new Object[paradas.length][3];
						for (int i = 0; i < paradas.length; i++) {
							
							usTable[i][0]=paradas[i].getORMID();
							usTable[i][1]=paradas[i].getNombre();
							usTable[i][2]=paradas[i].getDireccion();
						}
					}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Nombre", "Localizaci\u00F3n" }));
	
		for (int c = 0; c < this.getTablaFiltrada().getColumnCount(); c++)
		{
		     Class col_class = this.getTablaFiltrada().getColumnClass(c);
		     this.getTablaFiltrada().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		
		this.getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
	}
	void actualizarTT() {
		Object[][] usTable=new Object[1][3];
			ArrayList<Parada> aux = new ArrayList<Parada>();
			try {
				aux = MainAplicacion.getBdAdministrador().Cargar_Paradas();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(getIndexLinea()>-1){
				Parada[] paradas = null;
				try {
					
					paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(getIndexLinea());
					if (paradas!=null){
						for (int i = 0; i < paradas.length; i++) {
							for (int j = 0; j < aux.size(); j++) {
								if (aux.get(j)!=null){
								if(paradas[i].getORMID()==aux.get(j).getORMID()){
									aux.set(j, null);
								}
								}
							}
							
						}
						while(aux.contains(null)){
							aux.remove(null);
						}
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
				
				if(!aux.isEmpty()){
					usTable=new Object[aux.size()][3];
						for (int i = 0; i < aux.size(); i++) {
							if (aux.get(i)!=null){
							usTable[i][0]=aux.get(i).getORMID();
							usTable[i][1]=aux.get(i).getNombre();
							usTable[i][2]=aux.get(i).getDireccion();
							}
						}
					}
		
		this.getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Nombre", "Localizaci\u00F3n" }));
	
		for (int c = 0; c < this.getTablaTotal().getColumnCount(); c++)
		{
		     Class col_class = this.getTablaTotal().getColumnClass(c);
		     this.getTablaTotal().setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		
		this.getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneT().setViewportView(this.getTablaTotal());
		this.getScrollPaneT().revalidate();
		this.getScrollPaneT().repaint();
	}
	@Override
	public void agregar() {
		if(getIndexLinea()>-1){
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
	try {
MainAplicacion.getBdAdministrador().agregarParadaLinea(Integer.parseInt(tabla.getValueAt(fila, 0).toString()),getIndexLinea() ,new Recorrido());

	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (RemoteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	actualizarTF();
	actualizarTT();
	Informacion_Lineas_admin.line.actualizar();
		}
	}
	@Override
	public void quitar() {
		if(getIndexLinea()>-1){
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
	try {
		MainAplicacion.getBdAdministrador().quitarParadaLinea(Integer.parseInt(tabla.getValueAt(fila, 0).toString()),getIndexLinea());
	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (RemoteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	actualizarTF();
	actualizarTT();
	Informacion_Lineas_admin.line.actualizar();
		}
	}
}