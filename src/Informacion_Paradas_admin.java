import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BD.Evento;
import BD.Linea;
import BD.Punto_Interes;

public class Informacion_Paradas_admin extends Zona_Admin_Botones {
	private JPanel paradasAdmin;
	public Zona_Informacion_Paradas_admin zona_Informacion_Paradas_admin;
	public static Informacion_Paradas par;
	private static BD.Parada index;
	public Informacion_Paradas_admin(){
		par=new Informacion_Paradas();
		paradasAdmin=par.getParadas();
		paradasAdmin.remove(par.zona_Informacion_Paradas.zona_informacion_EventosParada.getScrollPaneEventos());
		paradasAdmin.remove(par.zona_Informacion_Paradas.zona_informacion_LineasParada.getScrollPaneLineas());
		paradasAdmin.remove(par.zona_Informacion_Paradas.zona_informacion_PuntosInteresParada.getScrollPanePInteres());
		
		
		paradasAdmin.add(getA�adirB());
		getA�adirB().setBounds(12, 476, 89, 23);
		paradasAdmin.add(getModificarB());
		getModificarB().setBounds(111, 476, 89, 23);
		paradasAdmin.add(getBorrarB());
		getBorrarB().setBounds(210, 476, 89, 23);
		
		zona_Informacion_Paradas_admin=new Zona_Informacion_Paradas_admin();
		paradasAdmin.add(zona_Informacion_Paradas_admin.getSubirFotoB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.getAgregarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.getQuitarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.getScrollPaneF());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.getScrollPaneT());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.getAgregarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.getQuitarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.getScrollPaneF());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.getScrollPaneT());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.getAgregarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.getQuitarB());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.getScrollPaneF());
		paradasAdmin.add(zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.getScrollPaneT());
		
		par.zona_Informacion_Paradas.getDireccionTF().setEditable(true);
		par.zona_Informacion_Paradas.getNombreTF().setEditable(true);
		
		par.getParadasCB().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(par.getParadasCB().getSelectedIndex()>-1 && !par.us.isEmpty()){
				aux();
				par.actualizarDetallesParada(false);
				zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.actualizarTF();
				zona_Informacion_Paradas_admin.zona_informacion_EventosParada_admin.actualizarTT();
				zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.actualizarTF();
				zona_Informacion_Paradas_admin.zona_informacion_LineasParada_admin.actualizarTT();
				zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.actualizarTF();
				zona_Informacion_Paradas_admin.zona_informacion_PuntosInteresParada_admin.actualizarTT();
				}
			}
			
		});
	}
	protected void aux() {
		if (par.getParadasCB().getSelectedIndex()>-1 && !par.us.isEmpty())
		index=par.us.get(par.getParadasCB().getSelectedIndex());
		
	}
	
	public static BD.Parada ParadaOrmIndex(){
		return index;
	}
	
	public JPanel getParadasAdmin() {
		return paradasAdmin;
	}
	public void setParadasAdmin(JPanel paradasAdmin) {
		this.paradasAdmin = paradasAdmin;
	}
	@Override
	public void a�adir() {
try {
	MainAplicacion.getBdAdministrador().addParada("","" , "");
} catch (RemoteException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
par.actualizar();
par.getParadasCB().setSelectedIndex(par.getParadasCB().getItemCount()-1);
	}
	@Override
	public void modificar() {
		try {
			MainAplicacion.getBdAdministrador().modParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID(), par.zona_Informacion_Paradas.getNombreTF().getText(),par.zona_Informacion_Paradas.getDireccionTF().getText() , "");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		par.actualizar();
	}
	@Override
	public void borrar() {
		try {
			ArrayList<Linea> LP = MainAplicacion.getBdAdministrador().cargar_LineasParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
			if(LP!=null){
			for (int i = 0; i < LP.size(); i++) {
				MainAplicacion.getBdAdministrador().quitarLineasParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID(), LP.get(i).getORMID());
			}
			}
			Evento[] EP = MainAplicacion.getBdAdministrador().cargar_EventosParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
			if(EP!=null){
			for (int i = 0; i < EP.length; i++) {
				MainAplicacion.getBdAdministrador().quitarEventoParada(EP[i].getORMID(), par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
			}
			}
			Punto_Interes[] PIP = MainAplicacion.getBdAdministrador().cargar_PuntosInteresParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
			if(PIP!=null){
			for (int i = 0; i < PIP.length; i++) {
				MainAplicacion.getBdAdministrador().quitarPuntosInteresParada(PIP[i].getORMID(),par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
			}
			}
			MainAplicacion.getBdAdministrador().delParada(par.us.get(par.getParadasCB().getSelectedIndex()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		par.actualizar();
	}
}