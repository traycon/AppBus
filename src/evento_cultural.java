import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.*;

import BD.Barrio;
import BD.Evento;
import BD.Parada;

public class evento_cultural {
	private JComboBox eventosCB;
	private ArrayList<Evento> us;
	private static Evento index=null;
	
	public evento_cultural(){
		this.eventosCB=new JComboBox();
		actualizar();
		eventosCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
	public JComboBox getEventosCB() {
		return eventosCB;
	}
	public void setEventosCB(JComboBox eventosCB) {
		this.eventosCB = eventosCB;
	}
	
	public void actualizar(){
		eventosCB.removeAllItems();
		try {
			us = MainAplicacion.getBdInvitado().Cargar_Eventos();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				eventosCB.addItem(us.get(i).getNombre());
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		eventosCB.revalidate();
		eventosCB.repaint();
	}
	
	public Evento getIndex(){
		return index;
	}
	
	public void aux(){
		index=us.get(eventosCB.getSelectedIndex());
	}
	public ArrayList<Evento> getUs() {
		return us;
	}
	public void setUs(ArrayList<Evento> us) {
		this.us = us;
	}
}