import java.awt.Color;
import java.awt.Image;
import java.awt.SystemColor;
import java.rmi.RemoteException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Parada;

public class Zona_Informacion_Puntos_Interes {
	private JEditorPane foto;
	private JTable eventosTable;
	private JLabel descripcionL;
	private JTextArea descripcionTA;
	private JLabel fotoL;
	private JScrollPane scrollPane;
	public Zona_Informacion_Puntos_Interes(){
		
		
		fotoL = new JLabel("Foto :");
		fotoL.setBounds(644, 17, 55, 16);
		String sourcefoto="";//a modo de ejemplo, las fotos las tomaremos de la ruta en la BD
		foto = new JEditorPane("text/html",
	            "<html><img src='"+sourcefoto+"' width=268height=161></img>");
		foto.setBounds(644, 36, 268, 161);
		
		descripcionL= new JLabel("Descripci\u00F3n :");
		descripcionL.setBounds(644, 209, 88, 16);
		
		descripcionTA = new JTextArea("");
		descripcionTA.setBounds(651, 229, 260, 56);
		descripcionTA.setEditable(false);
		descripcionTA.setOpaque(false);
		
		
		scrollPane= new JScrollPane();
		scrollPane.setBounds(644, 300, 268, 190);
		
	}
	public JEditorPane getFoto() {
		return foto;
	}
	public void setFoto(JEditorPane foto) {
		this.foto = foto;
	}
	public JTable getEventosTable() {
		return eventosTable;
	}
	public void setEventosTable(JTable eventosTable) {
		this.eventosTable = eventosTable;
	}
	public JLabel getDescripcionL() {
		return descripcionL;
	}
	public void setDescripcionL(JLabel descripcionL) {
		this.descripcionL = descripcionL;
	}
	public JTextArea getDescripcionTA() {
		return descripcionTA;
	}
	public void setDescripcionTA(JTextArea descripcionTA) {
		this.descripcionTA = descripcionTA;
	}
	public JLabel getFotoL() {
		return fotoL;
	}
	public void setFotoL(JLabel fotoL) {
		this.fotoL = fotoL;
	}
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
	public void actualizar(boolean b){
	Object[][] usTable=new Object[1][2];
	if(Informacion_Puntos_Interes.PuntoOrmIndex()!=null){
		BD.Evento[] eventos = null;
		try {
			eventos = MainAplicacion.getBdAdministrador().cargar_Eventos_PuntosInteres(Informacion_Puntos_Interes.PuntoOrmIndex().getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			if(eventos!=null){
				usTable=new Object[eventos.length][2];
					for (int i = 0; i < eventos.length; i++) {
								usTable[i][0]=eventos[i].getORMID();
								usTable[i][1]=eventos[i].getNombre();
					}
				}
	}
	eventosTable = new JTable();
	eventosTable.setModel(new DefaultTableModel(usTable, new String[] {
			"Numero","Evento"}));
	if (b){
	for (int c = 0; c <
			 eventosTable.getColumnCount(); c++) 
	{
		Class col_class =
		eventosTable.getColumnClass(c);
		eventosTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
	}
	}
	eventosTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	scrollPane.setViewportView(eventosTable);
	scrollPane.revalidate();
	scrollPane.repaint();
}
}