import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import BD.IRegistrado;

public class Configuracion {
	private JButton guardarB;
	private JButton cancelarB;
	public Zona_Configuracion zona_Configuracion;
	private JPanel config;
	private IRegistrado bd;
	public Configuracion(){
		zona_Configuracion=new Zona_Configuracion();
		config=new JPanel();
		config.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		config.setForeground(Color.CYAN);
		config.setLayout(null);
		
		guardarB = new JButton("Guardar");
		guardarB.setBounds(355, 244, 98, 26);
		guardarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(zona_Configuracion.getNewPassword2TF().getText().equals(zona_Configuracion.getNewPasswordTF().getText())){
				try {
					bd.GuardarConfig(Registrado.getCurrent().getORMID(), zona_Configuracion.getOldPasswordTF().getText(), zona_Configuracion.getNewPasswordTF().getText());
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					Registrado.setCurrent(bd.Cargar_Configuracion(Registrado.getCurrent().getORMID()));
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				}else{
					cancelarB.doClick();
				}
			}
		});
		config.add(guardarB);
		
		cancelarB = new JButton("Cancelar");
		cancelarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zona_Configuracion.getNewPassword2TF().setText("");
				zona_Configuracion.getNewPasswordTF().setText("");
				zona_Configuracion.getOldPasswordTF().setText("");
			}
		});
		cancelarB.setBounds(465, 244, 98, 26);
		config.add(cancelarB);
		config.add(zona_Configuracion.getBonoL());
		zona_Configuracion.getBonoTF().setText(Registrado.getCurrent().getIdBono());
		config.add(zona_Configuracion.getBonoTF());
		config.add(zona_Configuracion.getNewPassword2L());
		config.add(zona_Configuracion.getNewPassword2TF());
		config.add(zona_Configuracion.getNewPasswordL());
		config.add(zona_Configuracion.getNewPasswordTF());
		config.add(zona_Configuracion.getNombreL());
		zona_Configuracion.getNombreTF().setText(Registrado.getCurrent().getNombre()+" "+Registrado.getCurrent().getApellidos());
		config.add(zona_Configuracion.getNombreTF());
		config.add(zona_Configuracion.getOldPasswordL());
		config.add(zona_Configuracion.getOldPasswordTF());
	}
	public JButton getGuardarB() {
		return guardarB;
	}
	public void setGuardarB(JButton guardarB) {
		this.guardarB = guardarB;
	}
	public JButton getCancelarB() {
		return cancelarB;
	}
	public void setCancelarB(JButton cancelarB) {
		this.cancelarB = cancelarB;
	}
	public JPanel getConfig() {
		return config;
	}
	public void setConfig(JPanel config) {
		this.config = config;
	}
}