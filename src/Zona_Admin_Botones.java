import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Zona_Admin_Botones {
	private JButton a�adirB;
	private JButton modificarB;
	private JButton borrarB;
	
	public Zona_Admin_Botones(){
		a�adirB=new JButton("A�adir");
		a�adirB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a�adir();
			}
		});
		modificarB=new JButton("Modificar");
		modificarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modificar();
			}
		});
		borrarB=new JButton("Borrar");
		borrarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
	}

	public void a�adir() {
	}

	public void modificar() {
	}

	public void borrar() {
	}

	public JButton getA�adirB() {
		return a�adirB;
	}

	public void setA�adirB(JButton a�adirB) {
		this.a�adirB = a�adirB;
	}

	public JButton getModificarB() {
		return modificarB;
	}

	public void setModificarB(JButton modificarB) {
		this.modificarB = modificarB;
	}

	public JButton getBorrarB() {
		return borrarB;
	}

	public void setBorrarB(JButton borrarB) {
		this.borrarB = borrarB;
	}
}