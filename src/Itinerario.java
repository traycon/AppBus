import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class Itinerario {
	private JEditorPane solucionesED;
	
	public Itinerario(){
		
		solucionesED = new JEditorPane();
		solucionesED.setEditable(false);
		solucionesED.setContentType("text/html");
	}
	public JEditorPane getSolucionesT() {
		return solucionesED;
	}
	public void setSolucionesED(JEditorPane solucionesED) {
		this.solucionesED = solucionesED;
	}
	
	public void escribirRuta(String solucion){
		solucionesED.setText(solucion);
	}
	
}