import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JComboBox;

import BD.Barrio;
import BD.Parada;

public class barrios__1 {
	public ArrayList<Barrio> us;
	private JComboBox barriosCB;
	private static Barrio index=null;
	
	public barrios__1(){
		this.barriosCB=new JComboBox();
		actualizar();
		barriosCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
	
	public JComboBox getBarriosCB() {
		return barriosCB;
	}
	public void setBarriosCB(JComboBox barriosCB) {
		this.barriosCB = barriosCB;
	}
	
	public void actualizar(){
		barriosCB.removeAllItems();
		try {
			us = MainAplicacion.getBdAdministrador().cargar_Barrios();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				barriosCB.addItem(us.get(i).getNombre());
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		barriosCB.revalidate();
		barriosCB.repaint();
	}
	
	public Barrio getIndex(){
		return index;
	}
	
	public void aux(){
		index=us.get(barriosCB.getSelectedIndex());
	}

	public ArrayList<Barrio> getUs() {
		return us;
	}

	public void setUs(ArrayList<Barrio> us) {
		this.us = us;
	}
}