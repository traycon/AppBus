import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.*;

import BD.IAdministrador;
import BD.IInvitado;
import BD.IRegistrado;
import BD.Usuario;

public class Registrado extends General {
	private JButton cerrarB;
	public Historial historial;
	public Recarga recarga;
	public Configuracion configuracion;
	public JFrame frame;
	private static Usuario current;
	public Registrado(JFrame frame){
		this.frame=frame;
		initialize();
	}
	
	private void initialize() {
		historial=new Historial();
		tabbedPane.addTab("Historial", null, historial.getHistorial(), null);
		recarga=new Recarga();
		tabbedPane.addTab("Recarga", null, recarga.getRecargar(), null);
		configuracion=new Configuracion();
		tabbedPane.addTab("Configuración", null, configuracion.getConfig(), null);
		
		frame.getContentPane().add(tabbedPane);
		cerrarB = new JButton("Cerrar Sesi\u00F3n");
		cerrarB.setBounds(796, 12, 122, 26);
		cerrarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.remove(cerrarB);
				frame.remove(tabbedPane);
				Invitado inv=new Invitado(frame);
			}
		});
		frame.getContentPane().add(cerrarB);
		frame.revalidate();
		frame.repaint();
		frame.setVisible(true);
		
	}

	public void cerrarSesion() {
		current=null;
		throw new UnsupportedOperationException();
	}

	public static Usuario getCurrent() {
		return current;
	}

	public static void setCurrent(Usuario current) {
		Registrado.current = current;
	}
}