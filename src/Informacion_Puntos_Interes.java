import java.awt.Color;
import java.awt.SystemColor;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import BD.Evento;
import BD.Punto_Interes;

public class Informacion_Puntos_Interes {
	private static Punto_Interes index;
	private JTable puntos_interesTable;
	private JLabel puntos_interesL;
	public Zona_Informacion_Puntos_Interes zona_Informacion_Puntos_Interes;
	private JPanel puntosInteres;
	private JScrollPane scrollPane_3;
	public ArrayList<Punto_Interes> us;
	public Informacion_Puntos_Interes(){
		zona_Informacion_Puntos_Interes=new Zona_Informacion_Puntos_Interes();
		puntosInteres=new JPanel();
		puntosInteres.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		puntosInteres.setForeground(Color.CYAN);
		puntosInteres.setLayout(null);
		
		puntos_interesL = new JLabel("Puntos de Interes :");
		puntos_interesL.setBounds(11, 17, 118, 16);
		puntosInteres.add(puntos_interesL);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(12, 40, 621, 427);
		puntosInteres.add(scrollPane_3);
		
		puntosInteres.add(zona_Informacion_Puntos_Interes.getScrollPane());
		puntosInteres.add(zona_Informacion_Puntos_Interes.getDescripcionL());
		puntosInteres.add(zona_Informacion_Puntos_Interes.getDescripcionTA());
		puntosInteres.add(zona_Informacion_Puntos_Interes.getFoto());
		puntosInteres.add(zona_Informacion_Puntos_Interes.getFotoL());
		puntos_interesTable = new JTable();
		puntos_interesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

		    public void valueChanged(ListSelectionEvent lse) {
		        if(puntos_interesTable.getSelectedRow()>-1&& us!=null){
		        	aux();
		        	if (us.get(puntos_interesTable.getSelectedRow())!=null)
		        	actualizarDetallesPinter(true);
		        }
		    }
		});
		actualizar(true);
		if (puntos_interesTable.getSelectionModel().getSelectionMode()!=ListSelectionModel.SINGLE_SELECTION){
			puntos_interesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
	}
	protected void actualizarDetallesPinter(boolean b) {
		zona_Informacion_Puntos_Interes.getDescripcionTA().setText(""+us.get(puntos_interesTable.getSelectedRow()).getDescripcion());
		String sourceCartel=""+us.get(puntos_interesTable.getSelectedRow()).getDirFoto();
		zona_Informacion_Puntos_Interes.setFoto(new JEditorPane("text/html",
	            "<html><img src='"+sourceCartel+"' width=268height=161></img>"));
		zona_Informacion_Puntos_Interes.getFoto().revalidate();
		zona_Informacion_Puntos_Interes.getFoto().repaint();
		if(b){
			zona_Informacion_Puntos_Interes.actualizar(true);
		}
	}

	protected void actualizar(boolean b) {
		Object[][] desTable = new Object[0][5];
		try {
			us = MainAplicacion.getBdInvitado().Cargar_PuntosInteres();
			if (us != null) {
				desTable = new Object[us.size()][5];
				for (int i = 0; i < us.size(); i++) {
					desTable[i][0] = us.get(i).getNombre();
					desTable[i][1] = us.get(i).getLugar();
					SimpleDateFormat horaFormat = new SimpleDateFormat("hh:mm");
					desTable[i][3] = horaFormat.format(us.get(i).getHoraIni());
					desTable[i][4] = horaFormat.format(us.get(i).getHoraFin());
					desTable[i][2]=us.get(i).getPrecio();
				}
				
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		puntos_interesTable.setModel(new DefaultTableModel(desTable, new String[] {
				"Nombre", "Lugar","Precio", "Hora Inicio", "Hora Fin"}));
		if (b){
		for (int c = 0; c <
				puntos_interesTable.getColumnCount(); c++) 
		{
			Class col_class =
					puntos_interesTable.getColumnClass(c);
			puntos_interesTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}else{
			JTable a=new JTable();
			a.setTableHeader(puntos_interesTable.getTableHeader());
			for (int c = 0; c < puntos_interesTable.getColumnCount(); c++)
			{
			     Class col_class = puntos_interesTable.getColumnClass(c);
			     Class col_class_a = puntos_interesTable.getColumnClass(c);
			     puntos_interesTable.setDefaultEditor(col_class, a.getDefaultEditor(col_class_a));        // Hace que se pueda editar
			}
		}
		puntos_interesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_3.setViewportView(puntos_interesTable);
		scrollPane_3.revalidate();
		scrollPane_3.repaint();
	}
	protected void aux() {
		if (us.size()>puntos_interesTable.getSelectedRow())
		index=us.get(puntos_interesTable.getSelectedRow());
		
	}
	
	public static Punto_Interes PuntoOrmIndex(){
		return index;
	}
	public JPanel getPuntosInteres() {
		return puntosInteres;
	}
	public void setPuntosInteres(JPanel puntosInteres) {
		this.puntosInteres = puntosInteres;
	}
	public JTable getPuntos_interesTable() {
		return puntos_interesTable;
	}
	public void setPuntos_interesTable(JTable puntos_interesTable) {
		this.puntos_interesTable = puntos_interesTable;
	}
	public JLabel getPuntos_interesL() {
		return puntos_interesL;
	}
	public void setPuntos_interesL(JLabel puntos_interesL) {
		this.puntos_interesL = puntos_interesL;
	}
}