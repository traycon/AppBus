import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import BD.Barrio;
import BD.Calle;
import BD.IAdministrador;
import BD.IRegistrado;
import BD.Parada;

public class Barrios__2 extends Zona_Admin_Botones {
	private JLabel barrioL;
	private JComboBox barrioCB;
	public Zona_Informacion_Barrios zona_Informacion_Barrios;
	public Zona_calle zona_calle;
	private JPanel barrios;
	private ArrayList<Barrio> us;
	private static JComboBox index=null;
	private static ArrayList<Barrio> usClone;
	public Barrios__2(){
		barrios= new JPanel();
		barrios.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		barrios.setForeground(Color.CYAN);
		barrios.setLayout(null);
		
		getA�adirB().setBounds(12, 476, 89, 23);
		barrios.add(getA�adirB());
		getModificarB().setBounds(111, 476, 89, 23);
		barrios.add(getModificarB());
		getBorrarB().setBounds(210, 476, 89, 23);
		barrios.add(getBorrarB());
		barrioL = new JLabel("Barrios :");
		barrioL.setBounds(12, 16, 50, 14);
		barrios.add(barrioL);
		
		barrioCB = new JComboBox();
		barrioCB.setBounds(68, 13, 126, 20);
		actualizar();
		barrios.add(barrioCB);
		
		zona_calle=new Zona_calle();
		barrios.add(zona_calle.getA�adirB());
		barrios.add(zona_calle.getBorrarB());
		barrios.add(zona_calle.getCalleL());
		barrios.add(zona_calle.getModificarB());
		barrios.add(zona_calle.getScrollPane_calle());
		
		zona_Informacion_Barrios=new Zona_Informacion_Barrios();
		barrios.add(zona_Informacion_Barrios.getNombreL());
		barrios.add(zona_Informacion_Barrios.getParadasL());
		barrios.add(zona_Informacion_Barrios.getNombreTF());
		barrios.add(zona_Informacion_Barrios.zona_Paradas_Barrios.getAgregarB());
		barrios.add(zona_Informacion_Barrios.zona_Paradas_Barrios.getQuitarB());
		barrios.add(zona_Informacion_Barrios.zona_Paradas_Barrios.getScrollPaneF());
		barrios.add(zona_Informacion_Barrios.zona_Paradas_Barrios.getScrollPaneT());
		
		barrioCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (barrioCB.getSelectedIndex()>-1 && !us.isEmpty()) {

				zona_Informacion_Barrios.getNombreTF().setText(us.get(barrioCB.getSelectedIndex()).getNombre());
				zona_Informacion_Barrios.zona_Paradas_Barrios.actualizarTF();
				zona_Informacion_Barrios.zona_Paradas_Barrios.actualizarTT();
				zona_calle.actualizar();
				}
			}
		});
	}
	public void actualizar(){
		int barrioActual = barrioCB.getSelectedIndex();
		barrioCB.removeAllItems();
		try {
			us = MainAplicacion.getBdAdministrador().cargar_Barrios();
			if (us!=null){
			for (int i = 0; i < us.size(); i++) {
				barrioCB.addItem(us.get(i).getNombre());
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		barrioCB.revalidate();
		barrioCB.repaint();
		if(us.size()>barrioActual){
			barrioCB.setSelectedIndex(barrioActual);
		}
		index=barrioCB;
		usClone=us;
	}
	public JLabel getBarrioL() {
		return barrioL;
	}
	public void setBarrioL(JLabel barrioL) {
		this.barrioL = barrioL;
	}
	public JComboBox getBarrioCB() {
		return barrioCB;
	}
	public void setBarrioCB(JComboBox barrioCB) {
		this.barrioCB = barrioCB;
	}
	public JPanel getBarrios() {
		return barrios;
	}
	public void setBarrios(JPanel barrios) {
		this.barrios = barrios;
	}
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addBarrio("");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
		barrioCB.setSelectedIndex(barrioCB.getItemCount()-1);
	}
	@Override
	public void modificar(){
		try {
			MainAplicacion.getBdAdministrador().modBarrio(us.get(barrioCB.getSelectedIndex()).getORMID(), zona_Informacion_Barrios.getNombreTF().getText());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
	}
	@Override
	public void borrar(){
		 ArrayList<Parada> paradasBarrio = new ArrayList<Parada>();
		try {
			if(barrioCB.getSelectedIndex()>-1){
				ArrayList<Parada> paradas=new ArrayList<Parada>();
				try {
					paradas = MainAplicacion.getBdAdministrador().Cargar_Paradas();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 paradasBarrio = new ArrayList<Parada>();
				for (int i = 0; i < paradas.size(); i++) {
					if(paradas.get(i).getEs_contenida_paradas()!=null&&paradas.get(i).getEs_contenida_paradas().getORMID()==us.get(barrioCB.getSelectedIndex()).getORMID()){
						paradasBarrio.add(paradas.get(i));
					}
				}
			}
			if(paradasBarrio.size()>0){
				for (int i = 0; i < paradasBarrio.size(); i++) {
					MainAplicacion.getBdAdministrador().quitarParadas_Barrio(paradasBarrio.get(i).getORMID());
				}
			}
			ArrayList<Calle> callesBarrio = MainAplicacion.getBdAdministrador().cargar_calles(us.get(barrioCB.getSelectedIndex()).getORMID());
			if(callesBarrio.size()>0){
				for (int i = 0; i < callesBarrio.size(); i++) {
					MainAplicacion.getBdAdministrador().delCalle(callesBarrio.get(i).getORMID());
				}
			}
			MainAplicacion.getBdAdministrador().delBarrio(us.get(barrioCB.getSelectedIndex()).getORMID());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizar();
	}
	public static JComboBox BarrioOrmIndex(){
		return index;
	}
	public static ArrayList<Barrio> getUsClone() {
		return usClone;
	}
}