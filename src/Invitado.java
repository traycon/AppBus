import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import BD.IInvitado;

public class Invitado extends General {
	public inicia_sesion iniciar_sesion;
	public JFrame frame;
	public Invitado(JFrame frame){
		this.frame=frame;
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame.getContentPane().add(tabbedPane);
		final JButton btnIniciarSesin = new JButton("Iniciar Sesi\u00F3n");
		btnIniciarSesin.setBounds(796, 12, 122, 26);
		btnIniciarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iniciar_sesion=new inicia_sesion(tabbedPane,btnIniciarSesin,frame);
				iniciar_sesion.ventanaIniSesion.setVisible(true);
			}
		});
		frame.getContentPane().add(btnIniciarSesin);
		frame.revalidate();
		frame.repaint();
		frame.setVisible(true);
		
		
		}

}