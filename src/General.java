import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JTabbedPane;

import BD.IInvitado;

public class General {
	public Aplicacion aplicacion;
	public Zona_descargas zona_descargas;
	public Informacion_Paradas informacion_paradas;
	public Informacion_Eventos informacion_eventos;
	public Informacion_Tarifas informacion_tarifas;
	public Informacion_Lineas informacion_lineas;
	public Informacion_Puntos_Interes informacion_puntos_interes;
	public JTabbedPane tabbedPane;
	
	public General(){
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 52, 930, 534);
		
		aplicacion=new Aplicacion();
		tabbedPane.addTab("Busqueda", null, aplicacion.getAplicacion(), null);
		
		informacion_lineas=new Informacion_Lineas();
		tabbedPane.addTab("Lineas", null, informacion_lineas.getLineas(), null);
		
		informacion_paradas=new Informacion_Paradas();
		tabbedPane.addTab("Paradas", null, informacion_paradas.getParadas(), null);
		
		informacion_eventos=new Informacion_Eventos();
		tabbedPane.addTab("Eventos", null, informacion_eventos.getEventos(), null);
		
		informacion_puntos_interes=new Informacion_Puntos_Interes();
		tabbedPane.addTab("Puntos de Interes", null, informacion_puntos_interes.getPuntosInteres(), null);
		
		informacion_tarifas=new Informacion_Tarifas();
		tabbedPane.addTab("Tarifas", null, informacion_tarifas.getInformacionTarifas(), null);
		
		zona_descargas=new Zona_descargas();
		tabbedPane.addTab("Descargas", null, zona_descargas.getZona_descargas(), null);
		
	}
}