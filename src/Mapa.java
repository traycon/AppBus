import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.swing.JApplet;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class Mapa extends JApplet {
	public googleMaps unnamed_googleMaps_;
	public googleMaps unnamed_googleMaps_2;
	public JEditorPane posicionMapa;
	public String origen = "";
	public String destino = "";
	
	
	public Mapa(){
		posicionMapa = new JEditorPane();
		unnamed_googleMaps_ = new googleMaps();
		//String ruta = unnamed_googleMaps_.getRoutePoli("Almeria, " + "Javier Verdejo", "Almeria," + "Padre Mendez"  );
		//String imagen = unnamed_googleMaps_.getStaticMapRoute(ruta);
		posicionMapa.setContentType("text/html"); 
		//posicionMapa.setText("<html><img src='"+imagen+"></img></html>");
		posicionMapa.setAutoscrolls(true);
		posicionMapa.setEditable(false);
		posicionMapa.setBounds(488, 58, 425, 207);
	}
	
	public void crearContenido() throws MalformedURLException, UnsupportedEncodingException{

		try {
			unnamed_googleMaps_.setRecorrido("Almeria, " + origen, "Almeria, " + destino);
			String ruta = unnamed_googleMaps_.getRoutePoli("Almeria, " + origen, "Almeria, " + destino);
			String imagen = unnamed_googleMaps_.getStaticMapRoute(ruta);
			posicionMapa.setText("<html><img src='"+imagen+"></img></html>");
			posicionMapa.revalidate();
			posicionMapa.repaint();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void crearContenidoRuta(ArrayList<String> ruta2) throws MalformedURLException, UnsupportedEncodingException{

		try {
			unnamed_googleMaps_.setRecorrido(ruta2);
			String ruta = unnamed_googleMaps_.getRoutePoliLinea();
			String imagen = unnamed_googleMaps_.getStaticMapRoute(ruta);
			posicionMapa.setText("<html><img src='"+imagen+"></img></html>");
			posicionMapa.revalidate();
			posicionMapa.repaint();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void crearContenidoLinea(int idLinea) throws MalformedURLException, UnsupportedEncodingException{

		try {
			unnamed_googleMaps_.setRecorridoLinea(idLinea);
			String ruta = unnamed_googleMaps_.getRoutePoliLinea();
			String imagen = unnamed_googleMaps_.getStaticMapRoute(ruta);
			posicionMapa.setText("<html><img src='"+imagen+"></img></html>");
			posicionMapa.revalidate();
			posicionMapa.repaint();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}