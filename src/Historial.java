import java.awt.Color;
import java.awt.SystemColor;
import java.awt.datatransfer.Clipboard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import BD.Historial_Solucion;
import BD.IAdministrador;
import BD.IRegistrado;

public class Historial {
	public static JButton compartirB;
	private JTable consultasTable;
	private JPanel historial;
	private ArrayList<Historial_Solucion> us;
	private JScrollPane scrollPane_8;
	

	public Historial(){
		historial=new JPanel();
		historial.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		historial.setForeground(Color.CYAN);
		historial.setLayout(null);
		
		JLabel lblHistorial = new JLabel("Historial :");
		lblHistorial.setBounds(11, 17, 55, 16);
		historial.add(lblHistorial);
		
		scrollPane_8 = new JScrollPane();
		scrollPane_8.setBounds(12, 40, 901, 454);
		historial.add(scrollPane_8);
		
		
		 compartirB = new JButton("Compartir C\u00F3digo");
		compartirB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compartir();
			}
		});
		compartirB.setBounds(780, 7, 133, 26);
		historial.add(compartirB);
		actualizarTabla();
	}
	
	public void compartir() {
		if(consultasTable!=null&&consultasTable.getSelectedRow()>-1){
		new Clipboard(us.get(consultasTable.getSelectedRow()).getCodigo());
		}
		actualizarTabla();
	}

	public JButton getCompartirB() {
		return compartirB;
	}

	public void setCompartirB(JButton compartirB) {
		this.compartirB = compartirB;
	}

	public JTable getConsultasTable() {
		return consultasTable;
	}

	public void setConsultasTable(JTable consultasTable) {
		this.consultasTable = consultasTable;
	}

	public JPanel getHistorial() {
		return historial;
	}

	public void setHistorial(JPanel historial) {
		this.historial = historial;
	}
	public void actualizarTabla() {
		Object[][] usTable = new Object[0][4];
		if (Registrado.getCurrent()!=null){
		try {
			us = MainAplicacion.getBdRegistrado().Cargar_Historial(Registrado.getCurrent().getORMID());
			if (us!=null){
			usTable=new Object[us.size()][4];
			for (int i = 0; i < us.size(); i++) {
				usTable[i][0]=us.get(i).getCodigo();
				usTable[i][1]=us.get(i).getOrigen();
				usTable[i][2]=us.get(i).getDestino();
				usTable[i][3]=us.get(i).getFecha();
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		}
		consultasTable = new JTable();
		consultasTable.setModel(new DefaultTableModel(
			usTable,
			new String[] {
					"C\u00F3digo", "Origen", "Destino", "Fecha"
			}
		));
		
		for (int c = 0; c < consultasTable.getColumnCount(); c++)
		{
		     Class col_class = consultasTable.getColumnClass(c);
		     consultasTable.setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		consultasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_8.setViewportView(consultasTable);
		scrollPane_8.revalidate();
		scrollPane_8.repaint();
	}
}