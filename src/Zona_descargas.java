import java.awt.Color;
import java.awt.SystemColor;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableModel;

import BD.Descarga;

public class Zona_descargas {
	private JLabel descargasL;
	private JButton descargasB;
	private JTable descargasTable;
	private JPanel zona_descargas;
	public ArrayList<Descarga> us;
	public JScrollPane scrollPane_6;

	public Zona_descargas() {

		zona_descargas = new JPanel();
		zona_descargas.setBorder(new CompoundBorder(new BevelBorder(
				BevelBorder.LOWERED, null, null, null, null), null));
		zona_descargas.setForeground(Color.CYAN);
		zona_descargas.setLayout(null);

		descargasL = new JLabel("Descargas");
		descargasL.setBounds(12, 12, 84, 16);
		zona_descargas.add(descargasL);

		scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(12, 40, 901, 454);
		zona_descargas.add(scrollPane_6);

		/*
		 * Object[][] lista = new Object[][] { {"Tarifas", "Todos los precios",
		 * "11KB", "Disponible", "1.0.0", "04-12-14", "04-12-14"}, {"Mapa",
		 * "Mapa de la ciudad de Almeria", "1MB", "No Disponible", "1.5.1",
		 * "04-12-14", null}};
		 * 
		 * String[] cabecera = new String[] { "Nombre", "Descripci\u00F3n",
		 * "Tama\u00F1o", "Estado", "Versi\u00F3n", "Fecha Subida",
		 * "Ultima Modificaci\u00F3n" }; descargasTable = new
		 * JTable(lista,cabecera); for (int c = 0; c <
		 * descargasTable.getColumnCount(); c++) { Class col_class =
		 * descargasTable.getColumnClass(c);
		 * descargasTable.setDefaultEditor(col_class, null); // Hace que no se
		 * pueda editar }
		 * descargasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		 * scrollPane_6.setViewportView(descargasTable);
		 */
		  descargasB = new JButton("Descargar"); descargasB.setBounds(815, 7,
		  98, 26); zona_descargas.add(descargasB);
		 
		actualizar(true);
		scrollPane_6.setViewportView(descargasTable);
	}

	protected void actualizar(boolean b) {
		Object[][] desTable = new Object[0][7];
		try {
			us = MainAplicacion.getBdInvitado().Cargar_descargas();
			if (us != null) {
				desTable = new Object[us.size()][7];
				for (int i = 0; i < us.size(); i++) {
					desTable[i][0] = us.get(i).getNombre();
					desTable[i][1] = us.get(i).getDescripcion();
					desTable[i][2] = us.get(i).getTamanio();
					desTable[i][3] = us.get(i).getEstado();
					desTable[i][4] = us.get(i).getVersion();
					SimpleDateFormat fechaFormat = new SimpleDateFormat("yyyy-MM-dd");
					StringTokenizer fechaSubida=new StringTokenizer(fechaFormat.format(us.get(i).getFechaSubida()), "-");
					int y=Integer.parseInt(fechaSubida.nextToken()) ;
					int m=Integer.parseInt(fechaSubida.nextToken()) ;
					int d=Integer.parseInt(fechaSubida.nextToken()) ;
					StringTokenizer fechaModi=new StringTokenizer(fechaFormat.format(us.get(i).getUltimaModificacion()), "-");
					int y1=Integer.parseInt(fechaModi.nextToken()) ;
					int m1=Integer.parseInt(fechaModi.nextToken()) ;
					int d1=Integer.parseInt(fechaModi.nextToken()) ;
					desTable[i][5] = d+"/"+m+"/"+y;
					desTable[i][6] = d1+"/"+m1+"/"+y1;
				}
				
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		descargasTable = new JTable();
		descargasTable.setModel(new DefaultTableModel(desTable, new String[] {
				"Nombre", "Descripci\u00F3n", "Tama\u00F1o", "Estado",
				"Versi\u00F3n", "Fecha Subida", "Ultima Modificaci\u00F3n" }));
		if (b){
		for (int c = 0; c <
				 descargasTable.getColumnCount(); c++) 
		{
			Class col_class =
			descargasTable.getColumnClass(c);
			descargasTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}
		descargasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_6.setViewportView(descargasTable);
		scrollPane_6.revalidate();
		scrollPane_6.repaint();
	}

	public void descargar() {
		throw new UnsupportedOperationException();
	}

	public JPanel getZona_descargas() {
		return zona_descargas;
	}

	public void setZona_descargas(JPanel zona_descargas) {
		this.zona_descargas = zona_descargas;
	}

	public JTable getDescargasTable() {
		return descargasTable;
	}

	public void setDescargasTable(JTable descargasTable) {
		this.descargasTable = descargasTable;
	}
}