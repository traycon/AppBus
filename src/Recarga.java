import java.awt.Color;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

import BD.IRegistrado;

public class Recarga {
	private JButton pagarB;
	public Paypal paypal;
	private JPanel recargar;
	private IRegistrado bd;
public Recarga(){
	try {
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        String nombre = "Servidor2";
        Registry registry = LocateRegistry.getRegistry(1099);
        bd = (IRegistrado)registry.lookup(nombre);
         
        
    } catch (Exception e) {
        System.err.println("Servidor no arrancado:");
        e.printStackTrace();
    }
	paypal=new Paypal();
	recargar=new JPanel();
	recargar.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
	recargar.setForeground(Color.CYAN);
	recargar.setLayout(null);
	
	pagarB = new JButton("Recargar Bono");
	pagarB.setBounds(386, 252, 119, 26);
	pagarB.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			recargarBono();
		}
	});
	recargar.add(pagarB);
	
	Label label = new Label("Servicio proporcionado por Paypal");
	label.setFont(null);
	label.setBounds(714, 488, 201, 23);
	recargar.add(label);
}
	public void recargarBono() {

		try {
			bd.Recargar(Registrado.getCurrent().getIdBono(), 12);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public JButton getPagarB() {
		return pagarB;
	}
	public void setPagarB(JButton pagarB) {
		this.pagarB = pagarB;
	}
	public JPanel getRecargar() {
		return recargar;
	}
	public void setRecargar(JPanel recargar) {
		this.recargar = recargar;
	}
}