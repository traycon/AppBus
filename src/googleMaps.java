import java.awt.Dimension;
import java.awt.Image;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import BD.Linea;
import BD.Parada;
import maps.java.*;

import java.util.Arrays;

public class googleMaps {
	private ArrayList<String> recorrido;
	private Parada[] us;
	private ArrayList<Linea> lineas;
	
	public googleMaps(){
		//MapsJava.setKey(key);
		MapsJava.setLanguage("es");
		MapsJava.setRegion("es");
		MapsJava.setSensor(false);
		//comprobarKey();

	}
	
	//Metodo que calcula el itinerario de una linea
	public void setRecorrido(String origen, String destino){
		int idLinea = -1;
		
		try {
			lineas = MainAplicacion.getBdInvitado().Cargar_lineas();
		}catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(lineas != null){
			for(int i = 0; i < lineas.size(); i++){
				if((origen.equalsIgnoreCase(lineas.get(i).getNombreParadaIni()) && (destino.equalsIgnoreCase(lineas.get(i).getNombreParadFin()) ))){
					idLinea = i;
					break;
				}
			}
			if(idLinea != -1)
				cargarParadaLinea(idLinea);
			/*else
				System.out.println("No encontrado");*/
		}
	}
	
	//metodo que cargar las paradasLinea
	private void cargarParadaLinea(int idLinea){
		try{
			us = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(idLinea);
		}
		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		us = null;
		if(us != null)
			for(int i = 0; i < us.length; i++){
				recorrido.add(us[i].getDireccion());	
			}
	}
	
	//metodo para rellenar el recorrido
	public void setRecorridoLinea(int idLinea){
		us = null;
		recorrido=new ArrayList<String>();
		try{
			us = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(idLinea);
		}
		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(us != null && us.length>1){
			for(int i = 0; i < us.length; i++){
				recorrido.add("Almeria, "+us[i].getDireccion());	
			}
		}
	}
	
	/*private void comprobarKey(){
		System.out.println("Clave ok? " + MapsJava.APIkeyCheck(MapsJava.getKey()) );
	}*/
	
    public static void error(String funcionError){
        System.err.println("Algo ha ocurrido, no se pudo ejecutar la función: " + funcionError);
    }


	/*public String getStaticMap(String direccion) throws MalformedURLException, UnsupportedEncodingException{
        StaticMaps ObjStatMap=new StaticMaps();
        String resultado = null;
        try {
            Image resultadoMapa=ObjStatMap.getStaticMap(direccion, 14,new Dimension(418, 201),
                    1, StaticMaps.Format.png, StaticMaps.Maptype.roadmap);
            resultado = MapsJava.getLastRequestURL();
            System.out.println(resultado);
        } catch (Exception e) {
            error("Mapas estáticos");
            
        }

		return resultado;
	}*/
	
    //Metodo que genera el mapa de la ruta
	public String getStaticMapRoute(String direccion) throws MalformedURLException, UnsupportedEncodingException{
        StaticMaps ObjStatMap=new StaticMaps();
        String resultado = null;
        try {
            ObjStatMap.getStaticMapRoute(new Dimension(418, 201), 14, StaticMaps.Format.png,
            		StaticMaps.Maptype.roadmap, direccion);
            resultado = MapsJava.getLastRequestURL();
            //System.out.println(resultado);
        } catch (Exception e) {
            error("Mapas estáticos");
            
        }

		return resultado;
	}

	//Metodo que de vuelve el itinerario
	public String getRoute(String direccionInicio, String direccionFin) throws MalformedURLException, UnsupportedEncodingException{
		Route ObjRout=new Route();
		String resultado = "";
		String[][] resultadoRuta=ObjRout.getRoute("Almeria, " + direccionInicio,"Almeria, " + direccionFin, recorrido, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		for(int i=0;i< resultadoRuta.length;i++){
		   resultado = resultado + "<b>Tramo " + i + ": </b>";
		   //System.out.println("<b>Tramo " + i + ":</b> ");
		   for(int j=0;j< resultadoRuta [0].length;j++){
			   resultado = resultado + resultadoRuta[i][j] + "<br/>";
			   //System.out.println(resultadoRuta[i][j] + "<br/>");
		   }
		}
		return resultado;
	}
	
	//Metodo que realiza la polilinea
	public String getRoutePoli(String origen, String destino) throws MalformedURLException, UnsupportedEncodingException{
		Route ObjRout=new Route();
		ObjRout.getRoute(origen, destino,  recorrido, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		return ObjRout.getGeneralPolyline();
	}
	//Metodo que realiza la polilinea
	public String getRoutePoliLinea() throws MalformedURLException, UnsupportedEncodingException{
		Route ObjRout=new Route();
		if(recorrido!=null&&recorrido.size()>1){
		String origen=recorrido.get(0);
		recorrido.remove(0);
		String destino=recorrido.get(recorrido.size()-1);
		recorrido.remove(recorrido.size()-1);
		
		ObjRout.getRoute(origen, destino,  recorrido, Boolean.TRUE, Route.mode.driving, Route.avoids.nothing);
		}
		return ObjRout.getGeneralPolyline();
	}

	public void setRecorrido(ArrayList<String> ruta2) {
		recorrido=ruta2;
		
	}
}
