import java.awt.Color;
import java.awt.SystemColor;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import BD.Barrio;
import BD.IAdministrador;
import BD.IInvitado;
import BD.Parada;

public class Origen extends Parametros_busqueda {
	private JLabel paramOrigenL;
	private JRadioButton calleOrigenRB;
	private JRadioButton paradaOrigenRB;
	private JRadioButton barrioOrigenRB;
	private JTextField calleOrig;
	private JComboBox paradaOrig;
	private JComboBox barrioOrig;
	public ArrayList<Barrio> auxBarrio;
	
	public Origen(){
		calleOrig= getCalle().getCallesTF();
		calleOrig.setBounds(40, 141, 165, 20);
		calleOrig.setColumns(10);
		paradaOrig = getParada().getParadasCB();
		paradaOrig.setBounds(40, 201, 165, 25);
		
		auxBarrio=getBarrio().us;
		barrioOrig = getBarrio().getBarriosCB();
		barrioOrig.setBounds(40, 266, 165, 25);
		this.paramOrigenL=new JLabel("Origen :");
		this.paramOrigenL.setBounds(40, 75, 125, 16);
		this.calleOrigenRB = new JRadioButton("Calle :");
		this.calleOrigenRB.setBounds(19, 109, 121, 24);
		this.calleOrigenRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(calleOrigenRB.isSelected()){
					calleOrig.setEnabled(true);
					paradaOrig.setEnabled(false);
					barrioOrig.setEnabled(false);
					getParada().actualizar();
					getBarrio().actualizar();
				}
			}
		});
		this.paradaOrigenRB= new JRadioButton("Parada :");
		this.paradaOrigenRB.setBounds(19, 169, 121, 24);
		this.paradaOrigenRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(paradaOrigenRB.isSelected()){
					calleOrig.setEnabled(false);
					paradaOrig.setEnabled(true);
					barrioOrig.setEnabled(false);
					getBarrio().actualizar();
				}
			}
		});
		this.barrioOrigenRB = new JRadioButton("Barrio :");
		this.barrioOrigenRB.setBounds(19, 234, 121, 24);	
		this.barrioOrigenRB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(barrioOrigenRB.isSelected()){
					calleOrig.setEnabled(false);
					paradaOrig.setEnabled(false);
					barrioOrig.setEnabled(true);
					
					getParada().actualizar();
				}
			}
		});
		ButtonGroup menuRadios=new ButtonGroup();
		menuRadios.add(calleOrigenRB);
		menuRadios.add(paradaOrigenRB);
		menuRadios.add(barrioOrigenRB);
		calleOrigenRB.setSelected(true);
	}
	public JLabel getParamOrigenL() {
		return paramOrigenL;
	}
	public void setParamOrigenL(JLabel paramOrigenL) {
		this.paramOrigenL = paramOrigenL;
	}
	public JRadioButton getCalleOrigenRB() {
		return calleOrigenRB;
	}
	public void setCalleOrigenRB(JRadioButton calleOrigenRB) {
		this.calleOrigenRB = calleOrigenRB;
	}
	public JRadioButton getParadaOrigenRB() {
		return paradaOrigenRB;
	}
	public void setParadaOrigenRB(JRadioButton paradaOrigenRB) {
		this.paradaOrigenRB = paradaOrigenRB;
	}
	public JRadioButton getBarrioOrigenRB() {
		return barrioOrigenRB;
	}
	public void setBarrioOrigenRB(JRadioButton barrioOrigenRB) {
		this.barrioOrigenRB = barrioOrigenRB;
	}
	public JTextField getCalleOrig() {
		return calleOrig;
	}
	public void setCalleOrig(JTextField calleOrig) {
		this.calleOrig = calleOrig;
	}
	public JComboBox getParadaOrig() {
		return paradaOrig;
	}
	public void setParadaOrig(JComboBox paradaOrig) {
		this.paradaOrig = paradaOrig;
	}
	public JComboBox getBarrioOrig() {
		return barrioOrig;
	}
	public void setBarrioOrig(JComboBox barrioOrig) {
		this.barrioOrig = barrioOrig;
	}
	
}