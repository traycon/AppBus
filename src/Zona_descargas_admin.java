import java.awt.Component;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import BD.Descarga;
import BD.IAdministrador;

public class Zona_descargas_admin extends Zona_Admin_Botones {


	private JPanel descargasAdmin;
	private Zona_descargas descar;

	public Zona_descargas_admin(){
		
		descar=new Zona_descargas();
		descargasAdmin=descar.getZona_descargas();
				
		descargasAdmin.add(getA�adirB());
		descargasAdmin.add(getModificarB());
		descargasAdmin.add(getBorrarB());
		getA�adirB().setBounds(523, 9, 89, 23);
		getModificarB().setBounds(622, 9, 89, 23);
		getBorrarB().setBounds(721, 9, 89, 23);
		
		descar.actualizar(false);
	}
	
	

	public JPanel getDescargasAdmin() {
		return descargasAdmin;
	}

	public void setDescargasAdmin(JPanel descargasAdmin) {
		this.descargasAdmin = descargasAdmin;
	}
	
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addDescarga("","",0, false, "",new Date() , new Date());
			} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		descar.actualizar(false);
		}
	@Override
	public void modificar(){
		JTable tabla = descar.getDescargasTable();
		int fila = tabla.getSelectedRow();
		try {
			StringTokenizer fechaSubida=new StringTokenizer((String)tabla.getValueAt(fila, 5), "/");
			int d=Integer.parseInt(fechaSubida.nextToken()) ;
			int m=Integer.parseInt(fechaSubida.nextToken()) ;
			int y=Integer.parseInt(fechaSubida.nextToken()) ;
			StringTokenizer fechaModi=new StringTokenizer((String)tabla.getValueAt(fila, 6), "/");
			int d1=Integer.parseInt(fechaModi.nextToken()) ;
			int m1=Integer.parseInt(fechaModi.nextToken()) ;
			int y1=Integer.parseInt(fechaModi.nextToken()) ;
			MainAplicacion.getBdAdministrador().modDescarga(descar.us.get(fila).getORMID(), (String)tabla.getValueAt(fila, 0), (String)tabla.getValueAt(fila, 1), Double.parseDouble(tabla.getValueAt(fila, 2).toString()), Boolean.parseBoolean(tabla.getValueAt(fila, 3).toString()), (String)tabla.getValueAt(fila, 4), new Date(y+"/"+m+"/"+d) , new Date(y1+"/"+m1+"/"+d1));
			} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		descar.actualizar(false);
		}
	@Override
	public void borrar(){
		JTable tabla = descar.getDescargasTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().delDescarga(descar.us.get(fila).getORMID());	
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		descar.actualizar(false);
		
	}
}