import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.TabableView;

import org.hibernate.property.Getter;

import BD.IAdministrador;
import BD.Usuario;

public class Usuarios extends Zona_Admin_Botones {
	private JLabel usuariosL;
	private JTable usuariosTable;
	private JPanel usuarios;
	private ArrayList<Usuario> us;
	private JScrollPane scrollPane_18;
	public Usuarios(){
		usuarios= new JPanel();
		usuarios.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null), null));
		usuarios.setForeground(Color.CYAN);
		usuarios.setLayout(null);
		
		usuariosL = new JLabel("Usuarios :");
		usuariosL.setBounds(11, 17, 67, 14);
		usuarios.add(usuariosL);
		
		usuarios.add(getA�adirB());
		getA�adirB().setBounds(622, 13, 89, 23);
		
		usuarios.add(getModificarB());
		getModificarB().setBounds(723, 13, 89, 23);
		
		usuarios.add(getBorrarB());
		getBorrarB().setBounds(824, 13, 89, 23);
		
		scrollPane_18 = new JScrollPane();
		scrollPane_18.setBounds(12, 40, 901, 454);
		usuarios.add(scrollPane_18);
		actualizarTabla();
		scrollPane_18.setViewportView(usuariosTable);
	}
	protected void actualizarTabla() {
		Object[][] usTable = new Object[0][10];
		try {
			us = MainAplicacion.getBdAdministrador().Cargar_Usuarios();
			if (us!=null){
			usTable=new Object[us.size()][10];
			for (int i = 0; i < us.size(); i++) {
				usTable[i][0]=us.get(i).getORMID();
				usTable[i][1]=us.get(i).getNombre();
				usTable[i][2]=us.get(i).getApellidos();
				usTable[i][3]=us.get(i).getCorreo();
				usTable[i][4]=us.get(i).getTelefono();
				usTable[i][5]=us.get(i).getDireccion();
				usTable[i][6]=us.get(i).getCp();
				usTable[i][7]=us.get(i).getIdBono();
				usTable[i][8]=us.get(i).getPassword();
				usTable[i][9]=us.get(i).getAdministrador();
			}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		usuariosTable = new JTable();
		usuariosTable.setModel(new DefaultTableModel(
			usTable,
			new String[] {
				"Id", "Nombre", "Apellidos", "E-mail", "Tel\u00E9fono", "Direcci\u00F3n", "C.P.", "Id Bono","Password","Administrador"
			}
		));
		
		scrollPane_18.setViewportView(usuariosTable);
		scrollPane_18.revalidate();
		scrollPane_18.repaint();
	}
	public JLabel getUsuariosL() {
		return usuariosL;
	}
	public void setUsuariosL(JLabel usuariosL) {
		this.usuariosL = usuariosL;
	}
	public JTable getUsuariosTable() {
		return usuariosTable;
	}
	public void setUsuariosTable(JTable usuariosTable) {
		this.usuariosTable = usuariosTable;
	}
	public JPanel getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(JPanel usuarios) {
		this.usuarios = usuarios;
	}
	@Override
	public void a�adir(){
		try {
			MainAplicacion.getBdAdministrador().addUsuario("","","","",000000000,"",00000,"", new java.util.Date(), false);
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		actualizarTabla();
	}
	@Override
	public void modificar(){
		JTable tabla = getUsuariosTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().modUsuario((Integer)tabla.getValueAt(fila, 0), (String)tabla.getValueAt(fila, 1),(String) tabla.getValueAt(fila, 2), (String)tabla.getValueAt(fila, 3), (String)tabla.getValueAt(fila, 8), Integer.parseInt(tabla.getValueAt(fila, 4).toString()), (String)tabla.getValueAt(fila, 5), Integer.parseInt(tabla.getValueAt(fila, 6).toString()), (String)tabla.getValueAt(fila, 7), new Date(System.currentTimeMillis()), Boolean.parseBoolean(tabla.getValueAt(fila, 9).toString()));
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		actualizarTabla();
	}
	@Override
	public void borrar(){
		JTable tabla = getUsuariosTable();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().delUsuario((Integer)tabla.getValueAt(fila, 0));
		} catch (RemoteException e1) {
		// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		actualizarTabla();
	}
}