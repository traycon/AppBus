import java.awt.Image;
import java.awt.Label;

import javax.swing.*;

public class Zona_Informacion_Paradas {
	private JLabel nombreL;
	private JTextField nombreTF;
	private JLabel direccionL;
	private JTextField direccionTF;
	private JEditorPane fotoI;
	private JLabel fotoL;
	public Zona_informacion_EventosParada zona_informacion_EventosParada;
	public Zona_informacion_LineasParada zona_informacion_LineasParada;
	public Zona_informacion_PuntosInteresParada zona_informacion_PuntosInteresParada;
	public Zona_Informacion_Paradas(){
		zona_informacion_EventosParada=new Zona_informacion_EventosParada();
		zona_informacion_LineasParada=new Zona_informacion_LineasParada();
		zona_informacion_PuntosInteresParada=new Zona_informacion_PuntosInteresParada();
		
		 nombreL = new JLabel("\uF0B7 Nombre Parada :");
		nombreL.setBounds(40, 81, 110, 16);
		
		 direccionL = new JLabel("\uF0B7 Direcci\u00F3n :");
		direccionL.setBounds(360, 81, 85, 16);
		
		String sourceFoto="";//a modo de ejemplo, las fotos las tomaremos de la ruta en la BD
		 fotoI = new JEditorPane("text/html",
		            "<html><img src='"+sourceFoto+"' width=319height=312></img>");
		fotoI.setBounds(594, 161, 319, 312);
		fotoI.setEditable(false);
		
		 fotoL = new JLabel("\uF0B7 Foto :");
		fotoL.setBounds(594, 137, 55, 16);
		
		 nombreTF = new JTextField("Centro");
		nombreTF.setBounds(159, 81, 180, 16);
		nombreTF.setEditable(false);
		
		 direccionTF = new JTextField("C/ Inventada 5");
		direccionTF.setBounds(445, 81, 400, 16);
		direccionTF.setEditable(false);
	}
	protected void actualizar(boolean b) {
		if (b){
		zona_informacion_EventosParada.actualizar();
		zona_informacion_LineasParada.actualizar();
		zona_informacion_PuntosInteresParada.actualizar();
		}
	}
	public JLabel getNombreL() {
		return nombreL;
	}
	public void setNombreL(JLabel nombreL) {
		this.nombreL = nombreL;
	}
	public JTextField getNombreTF() {
		return nombreTF;
	}
	public void setNombreTF(JTextField nombreTF) {
		this.nombreTF = nombreTF;
	}
	public JLabel getDireccionL() {
		return direccionL;
	}
	public void setDireccionL(JLabel direccionL) {
		this.direccionL = direccionL;
	}
	public JTextField getDireccionTF() {
		return direccionTF;
	}
	public void setDireccionTF(JTextField direccionTF) {
		this.direccionTF = direccionTF;
	}
	public JEditorPane getFotoI() {
		return fotoI;
	}
	public void setFotoI(JEditorPane fotoI) {
		this.fotoI = fotoI;
	}
	public JLabel getFotoL() {
		return fotoL;
	}
	public void setFotoL(JLabel fotoL) {
		this.fotoL = fotoL;
	}
}