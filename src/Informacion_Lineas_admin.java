import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.JPanel;
import javax.swing.JTable;

import BD.Linea;
import BD.Parada;

public class Informacion_Lineas_admin extends Zona_Admin_Botones {
	public Zona_Informacion_Lineas_admin zona_Informacion_Lineas_admin;
	private JPanel lineasAdmin;
	public static Informacion_Lineas line;
	private static Linea index;
	public Informacion_Lineas_admin(){
		line=new Informacion_Lineas();
		lineasAdmin=line.getLineas();
		
		lineasAdmin.remove(line.zona_Informacion_Lineas.getScrollPane());
		lineasAdmin.add(getA�adirB());
		getA�adirB().setBounds(12, 476, 89, 23);
		lineasAdmin.add(getModificarB());
		getModificarB().setBounds(111, 476, 89, 23);
		lineasAdmin.add(getBorrarB());
		getBorrarB().setBounds(210, 476, 89, 23);
		
		zona_Informacion_Lineas_admin=new Zona_Informacion_Lineas_admin();
		lineasAdmin.add(zona_Informacion_Lineas_admin.getAgregarB());
		lineasAdmin.add(zona_Informacion_Lineas_admin.getQuitarB());
		lineasAdmin.add(zona_Informacion_Lineas_admin.getScrollPaneF());
		lineasAdmin.add(zona_Informacion_Lineas_admin.getScrollPaneT());
		
		line.zona_Informacion_Lineas.getHoraIniTF().setEditable(true);
		line.zona_Informacion_Lineas.getHoraEstimadaTF().setEditable(false);
		line.zona_Informacion_Lineas.getNombreLineaTF().setEditable(true);
		line.zona_Informacion_Lineas.getNombreParadaFinTF().setEditable(false);
		line.zona_Informacion_Lineas.getNombreParadaIniTF().setEditable(false);
		line.zona_Informacion_Lineas.getNumeroLineaTF().setEditable(false);
		line.zona_Informacion_Lineas.getObservacionesTF().setEditable(true);
		line.zona_Informacion_Lineas.getParadaFinTF().setEditable(false);
		line.zona_Informacion_Lineas.getParadaIniTF().setEditable(false);
		line.zona_Informacion_Lineas.getTarifaTF().setEditable(true);
		line.getLineasCB().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(line.getLineasCB().getSelectedIndex()>-1 && !line.us.isEmpty()){
				aux();
				line.actualizarDetallesLinea(false);
				zona_Informacion_Lineas_admin.actualizarTF();
				zona_Informacion_Lineas_admin.actualizarTT();
				}
			}
			
		});
	}
	protected void aux() {
		if (line.getLineasCB().getSelectedIndex()>-1 && !line.us.isEmpty())
		index=line.us.get(line.getLineasCB().getSelectedIndex());
		
	}
	
	public static Linea LineaOrmIndex(){
		return index;
	}
	public JPanel getLineasAdmin() {
		return lineasAdmin;
	}
	public void setLineasAdmin(JPanel lineasAdmin) {
		this.lineasAdmin = lineasAdmin;
	}
	
	@Override
	public void a�adir() {
		try {
			/**MainAplicacion.getBdAdministrador().addLinea(
					line.zona_Informacion_Lineas.getNombreLineaTF().getText(), 
					Integer.parseInt(line.zona_Informacion_Lineas.getNumeroLineaTF().getText()), 
					Integer.parseInt(line.zona_Informacion_Lineas.getParadaIniTF().getText()), 
					new Date("2000/02/02 "+line.zona_Informacion_Lineas.getHoraIniTF().getText()+":00"), 
					line.zona_Informacion_Lineas.getNombreParadaIniTF().getText(), 
					Integer.parseInt(line.zona_Informacion_Lineas.getParadaFinTF().getText()), 
					new Date("2000/02/02 "+line.zona_Informacion_Lineas.getHoraEstimadaTF().getText()+":00"), 
					line.zona_Informacion_Lineas.getNombreParadaFinTF().getText(), 
					line.zona_Informacion_Lineas.getObservacionesTF().getText(), 
					Double.parseDouble(line.zona_Informacion_Lineas.getTarifaTF().getText()), 
					"");**/
			MainAplicacion.getBdAdministrador().addLinea(
					"", 
					0, 
					0, 
					new Date("2000/02/02 00:00:00"), 
					"", 
					0, 
					new Date("2000/02/02 00:00:00"), 
					"", 
					"", 
					0, 
					"");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		line.actualizar();
		line.getLineasCB().setSelectedIndex(line.getLineasCB().getItemCount()-1);
	}
	@Override
	public void modificar() {
		try {
			MainAplicacion.getBdAdministrador().modLinea(
					Informacion_Lineas.LineaOrmIndex().getORMID(),
					line.zona_Informacion_Lineas.getNombreLineaTF().getText(), 
					Integer.parseInt(line.zona_Informacion_Lineas.getNumeroLineaTF().getText()), 
					Integer.parseInt(line.zona_Informacion_Lineas.getParadaIniTF().getText()), 
					new Date("2000/02/02 "+line.zona_Informacion_Lineas.getHoraIniTF().getText()+":00"), 
					line.zona_Informacion_Lineas.getNombreParadaIniTF().getText(), 
					Integer.parseInt(line.zona_Informacion_Lineas.getParadaFinTF().getText()), 
					new Date("2000/02/02 "+line.zona_Informacion_Lineas.getHoraEstimadaTF().getText()+":00"), 
					line.zona_Informacion_Lineas.getNombreParadaFinTF().getText(), 
					line.zona_Informacion_Lineas.getObservacionesTF().getText(), 
					Double.parseDouble(line.zona_Informacion_Lineas.getTarifaTF().getText()), 
					"");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		line.actualizar();
	}
	@Override
	public void borrar() {
		try {
			Parada[] paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(line.us.get(line.getLineasCB().getSelectedIndex()).getORMID());
			if(paradas!=null){
			for (int i = 0; i < paradas.length; i++) {
				MainAplicacion.getBdAdministrador().quitarParadaLinea(paradas[i].getORMID(), line.us.get(line.getLineasCB().getSelectedIndex()).getORMID());
			}
			}
			MainAplicacion.getBdAdministrador().delLinea(line.us.get(line.getLineasCB().getSelectedIndex()).getORMID());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		line.actualizar();
	}
}