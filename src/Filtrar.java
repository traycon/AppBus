import javax.swing.*;

public class Filtrar {
	private JComboBox filtrosCB;
	private JLabel filtrosL;
	public Filtrar() {
		filtrosL = new JLabel("Filtro Resultado :");
		filtrosL.setBounds(40, 303, 125, 16);
		filtrosCB = new JComboBox();
		filtrosCB.setModel(new DefaultComboBoxModel(new String[] {"Menor Coste", "Menor Tiempo", "Transbordos"}));
		filtrosCB.setBounds(40, 331, 165, 25);
		
	}

	public void menorTiempo() {
		throw new UnsupportedOperationException();
	}

	public void menorCoste() {
		throw new UnsupportedOperationException();
	}

	public void menosTransbordos() {
		throw new UnsupportedOperationException();
	}

	public JComboBox getFiltrosCB() {
		return filtrosCB;
	}

	public void setFiltrosCB(JComboBox filtrosCB) {
		this.filtrosCB = filtrosCB;
	}

	public JLabel getFiltrosL() {
		return filtrosL;
	}

	public void setFiltrosL(JLabel filtrosL) {
		this.filtrosL = filtrosL;
	}
}