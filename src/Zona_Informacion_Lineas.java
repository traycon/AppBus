import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Linea;
import BD.Parada;

public class Zona_Informacion_Lineas {
	private JLabel paradasL;
	private JTable paradasTable;
	private JLabel numeroLineaL;
	private JTextField numeroLineaTF;
	private JLabel detallesL;
	private JLabel nombreLineaL;
	private JTextField nombreLineaTF;
	private JLabel paradaIniL;
	private JTextField paradaIniTF;
	private JLabel nombreParadaIniL;
	private JTextField nombreParadaIniTF;
	private JLabel horaIniL;
	private JTextField horaIniTF;
	private JLabel paradaFinL;
	private JTextField paradaFinTF;
	private JLabel nombreParadaFinL;
	private JTextField nombreParadaFinTF;
	private JLabel horaEstimadaL;
	private JTextField horaEstimadaTF;
	private JLabel observacionesL;
	private JTextField observacionesTF;
	private JLabel tarifaL;
	private JTextField tarifaTF;
	private JLabel mapaL;
	public Mapa mapa;
	private JEditorPane panelMapa;
	public JScrollPane scrollPane;
	
	public Zona_Informacion_Lineas(){
		
		detallesL = new JLabel("Detalles :");
		detallesL.setBounds(33, 81, 55, 16);
		mapa=new Mapa();
		panelMapa=mapa.posicionMapa;
		panelMapa.setEditable(false);
		panelMapa.setBounds(489, 57, 424, 205);
		try {
			//mapa.origen = paradaIniTF.getText();
			//mapa.destino = paradaFinTF.getText();
			mapa.crearContenido();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		
		mapaL = new JLabel("Mapa :");
		mapaL.setBounds(489, 29, 55, 16);
		
		numeroLineaL = new JLabel("\uF0B7 N\u00FAmero de l\u00EDnea :");
		numeroLineaL.setBounds(48, 121, 127, 16);
		
		nombreLineaL = new JLabel("\uF0B7 Nombre de la l\u00EDnea :");
		nombreLineaL.setBounds(48, 149, 127, 16);
		
		paradaIniL = new JLabel("\uF0B7 Parada inicial :");
		paradaIniL.setBounds(48, 177, 112, 16);
		
		nombreParadaIniL = new JLabel("\uF0B7 Nombre de la parada inicial :");
		nombreParadaIniL.setBounds(48, 205, 179, 16);
		
	 horaIniL = new JLabel("\uF0B7 Hora de inicio de la expedici\u00F3n :");
		horaIniL.setBounds(48, 233, 197, 16);
		
		 paradaFinL = new JLabel("\uF0B7 Parada final :");
		paradaFinL.setBounds(48, 261, 112, 16);
		
		
		 nombreParadaFinL = new JLabel("\uF0B7 Nombre de la parada final :");
		nombreParadaFinL.setBounds(48, 289, 179, 16);
		
		
		 horaEstimadaL = new JLabel("\uF0B7 Hora estimada de llegada a la parada final :");
		horaEstimadaL.setBounds(48, 317, 269, 16);
		
		 observacionesL = new JLabel("\uF0B7 Observaciones :");
		observacionesL.setBounds(48, 345, 112, 16);
		
		 tarifaL = new JLabel("\uF0B7 Tarifa :");
		tarifaL.setBounds(48, 373, 55, 16);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(489, 302, 424, 192);
		actualizar(true);
		
		paradasL = new JLabel("Paradas :");
		paradasL.setBounds(489, 274, 55, 16);
		
		numeroLineaTF= new JTextField();
		numeroLineaTF.setBounds(167, 121, 69, 16);
		numeroLineaTF.setEditable(false);
		
		horaEstimadaTF = new JTextField();
		horaEstimadaTF.setBounds(311, 317, 69, 16);
		horaEstimadaTF.setEditable(false);
		
		 paradaIniTF = new JTextField();
		paradaIniTF.setBounds(151, 177, 69, 16);
		paradaIniTF.setEditable(false);
		
		 nombreParadaIniTF = new JTextField();
		nombreParadaIniTF.setBounds(233, 205, 69, 16);
		nombreParadaIniTF.setEditable(false);
		
		
		horaIniTF = new JTextField();
		horaIniTF.setBounds(248, 233, 69, 16);
		horaIniTF.setEditable(false);
		
		nombreLineaTF = new JTextField();
		nombreLineaTF.setBounds(181, 149, 122, 16);
		nombreLineaTF.setEditable(false);
		
		nombreParadaFinTF = new JTextField();
		nombreParadaFinTF.setBounds(220, 289, 69, 16);
		nombreParadaFinTF.setEditable(false);
		
		tarifaTF = new JTextField();
		tarifaTF.setBounds(109, 373, 69, 16);
		tarifaTF.setEditable(false);
		
		paradaFinTF = new JTextField();
		paradaFinTF.setBounds(151, 262, 69, 16);
		paradaFinTF.setEditable(false);
		
		observacionesTF = new JTextField();
		observacionesTF.setBounds(167, 345, 300, 16);
		observacionesTF.setEditable(false);
		
		//scrollPane.setViewportView(descargasTable);
	}

	void actualizar(boolean b) {
		Object[][] usTable=new Object[1][3];
		if(Informacion_Lineas.LineaOrmIndex()!=null){
			Parada[] paradas = null;
			try {
				paradas = MainAplicacion.getBdAdministrador().cargar_ParadaLinea(Informacion_Lineas.LineaOrmIndex().getORMID());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(paradas!=null){
					usTable=new Object[paradas.length][3];
						for (int i = 0; i < paradas.length; i++) {
							
							usTable[i][0]=paradas[i].getORMID();
							usTable[i][1]=paradas[i].getNombre();
							usTable[i][2]=paradas[i].getDireccion();
						}
					}
		}
		paradasTable = new JTable();
		paradasTable.setModel(new DefaultTableModel(usTable, new String[] {
				"Parada","Nombre", "Localizaci\u00F3n" }));
		if (b){
		for (int c = 0; c <
				 paradasTable.getColumnCount(); c++) 
		{
			Class col_class =
			paradasTable.getColumnClass(c);
			paradasTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		}
		paradasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(paradasTable);
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	public JLabel getParadasL() {
		return paradasL;
	}
	public void setParadasL(JLabel paradasL) {
		this.paradasL = paradasL;
	}
	public JTable getParadasTable() {
		return paradasTable;
	}
	public void setParadasTable(JTable paradasTable) {
		this.paradasTable = paradasTable;
	}
	public JLabel getNumeroLineaL() {
		return numeroLineaL;
	}
	public void setNumeroLineaL(JLabel numeroLineaL) {
		this.numeroLineaL = numeroLineaL;
	}
	public JTextField getNumeroLineaTF() {
		return numeroLineaTF;
	}
	public void setNumeroLineaTF(JTextField numeroLineaTF) {
		this.numeroLineaTF = numeroLineaTF;
	}
	public JLabel getDetallesL() {
		return detallesL;
	}
	public void setDetallesL(JLabel detallesL) {
		this.detallesL = detallesL;
	}
	public JLabel getNombreLineaL() {
		return nombreLineaL;
	}
	public void setNombreLineaL(JLabel nombreLineaL) {
		this.nombreLineaL = nombreLineaL;
	}
	public JTextField getNombreLineaTF() {
		return nombreLineaTF;
	}
	public void setNombreLineaTF(JTextField nombreLineaTF) {
		this.nombreLineaTF = nombreLineaTF;
	}
	public JLabel getParadaIniL() {
		return paradaIniL;
	}
	public void setParadaIniL(JLabel paradaIniL) {
		this.paradaIniL = paradaIniL;
	}
	public JTextField getParadaIniTF() {
		return paradaIniTF;
	}
	public void setParadaIniTF(JTextField paradaIniTF) {
		this.paradaIniTF = paradaIniTF;
	}
	public JLabel getNombreParadaIniL() {
		return nombreParadaIniL;
	}
	public void setNombreParadaIniL(JLabel nombreParadaIniL) {
		this.nombreParadaIniL = nombreParadaIniL;
	}
	public JTextField getNombreParadaIniTF() {
		return nombreParadaIniTF;
	}
	public void setNombreParadaIniTF(JTextField nombreParadaIniTF) {
		this.nombreParadaIniTF = nombreParadaIniTF;
	}
	public JLabel getHoraIniL() {
		return horaIniL;
	}
	public void setHoraIniL(JLabel horaIniL) {
		this.horaIniL = horaIniL;
	}
	public JTextField getHoraIniTF() {
		return horaIniTF;
	}
	public void setHoraIniTF(JTextField horaIniTF) {
		this.horaIniTF = horaIniTF;
	}
	public JLabel getParadaFinL() {
		return paradaFinL;
	}
	public void setParadaFinL(JLabel paradaFinL) {
		this.paradaFinL = paradaFinL;
	}
	public JTextField getParadaFinTF() {
		return paradaFinTF;
	}
	public void setParadaFinTF(JTextField paradaFinTF) {
		this.paradaFinTF = paradaFinTF;
	}
	public JLabel getNombreParadaFinL() {
		return nombreParadaFinL;
	}
	public void setNombreParadaFinL(JLabel nombreParadaFinL) {
		this.nombreParadaFinL = nombreParadaFinL;
	}
	public JTextField getNombreParadaFinTF() {
		return nombreParadaFinTF;
	}
	public void setNombreParadaFinTF(JTextField nombreParadaFinTF) {
		this.nombreParadaFinTF = nombreParadaFinTF;
	}
	public JLabel getHoraEstimadaL() {
		return horaEstimadaL;
	}
	public void setHoraEstimadaL(JLabel horaEstimadaL) {
		this.horaEstimadaL = horaEstimadaL;
	}
	public JTextField getHoraEstimadaTF() {
		return horaEstimadaTF;
	}
	public void setHoraEstimadaTF(JTextField horaEstimadaTF) {
		this.horaEstimadaTF = horaEstimadaTF;
	}
	public JLabel getObservacionesL() {
		return observacionesL;
	}
	public void setObservacionesL(JLabel observacionesL) {
		this.observacionesL = observacionesL;
	}
	public JTextField getObservacionesTF() {
		return observacionesTF;
	}
	public void setObservacionesTF(JTextField observacionesTF) {
		this.observacionesTF = observacionesTF;
	}
	public JLabel getTarifaL() {
		return tarifaL;
	}
	public void setTarifaL(JLabel tarifaL) {
		this.tarifaL = tarifaL;
	}
	public JTextField getTarifaTF() {
		return tarifaTF;
	}
	public void setTarifaTF(JTextField tarifaTF) {
		this.tarifaTF = tarifaTF;
	}
	public JLabel getMapaL() {
		return mapaL;
	}
	public void setMapaL(JLabel mapaL) {
		this.mapaL = mapaL;
	}
	public JEditorPane getPanelMapa() {
		return panelMapa;
	}
	public void setPanelMapa(JEditorPane panelMapa) {
		this.panelMapa = panelMapa;
	}
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
}