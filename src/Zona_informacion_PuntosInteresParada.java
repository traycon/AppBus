import java.rmi.RemoteException;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Punto_Interes;

public class Zona_informacion_PuntosInteresParada {
	private JLabel puntos_interesL;
	private JTable puntos_interesTable;
	private JScrollPane scrollPanePInteres;
	public Zona_informacion_PuntosInteresParada(){
		puntos_interesL = new JLabel("\uF0B7 Puntos de Interes :");
		puntos_interesL.setBounds(415, 135, 150, 16);
		
		scrollPanePInteres = new JScrollPane();
		scrollPanePInteres.setBounds(400, 159, 150, 250);
		
		puntos_interesTable = new JTable();
		puntos_interesTable.setModel(new DefaultTableModel(
			new Object[][] {
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
			},
			new String[] {
				"P. Interes"
			}
		));
		for (int c = 0; c < puntos_interesTable.getColumnCount(); c++)
		{
		     Class col_class = puntos_interesTable.getColumnClass(c);
		     puntos_interesTable.setDefaultEditor(col_class, null);        // Hace que no se pueda editar
		}
		puntos_interesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPanePInteres.setViewportView(puntos_interesTable);
	}
	public int getIndexParada(){
		if(Informacion_Paradas.getParadasCBClone().getSelectedIndex()>-1){
		StringTokenizer Parada=new StringTokenizer(Informacion_Paradas.getParadasCBClone().getSelectedItem().toString(), " ");
		Parada.nextToken();
		return Integer.parseInt(Parada.nextToken());
		}
		return -1;
	}
	public JLabel getPuntos_interesL() {
		return puntos_interesL;
	}
	public void setPuntos_interesL(JLabel puntos_interesL) {
		this.puntos_interesL = puntos_interesL;
	}
	public JTable getPuntos_interesTable() {
		return puntos_interesTable;
	}
	public void setPuntos_interesTable(JTable puntos_interesTable) {
		this.puntos_interesTable = puntos_interesTable;
	}
	public JScrollPane getScrollPanePInteres() {
		return scrollPanePInteres;
	}
	public void setScrollPanePInteres(JScrollPane scrollPanePInteres) {
		this.scrollPanePInteres = scrollPanePInteres;
	}
	public void actualizar() {
		Object[][] usTable=new Object[1][2];
		if(getIndexParada()>-1){
			Punto_Interes[] eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_PuntosInteresParada(getIndexParada());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.length][2];
						for (int i = 0; i < eventos.length; i++) {
									usTable[i][0]=eventos[i].getORMID();
									usTable[i][1]=eventos[i].getNombre();
						}
					}
		}
		puntos_interesTable = new JTable();
		puntos_interesTable.setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Puntos interes"}));
		
		for (int c = 0; c <
				 puntos_interesTable.getColumnCount(); c++) 
		{
			Class col_class =
			puntos_interesTable.getColumnClass(c);
			puntos_interesTable.setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		puntos_interesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPanePInteres.setViewportView(puntos_interesTable);
		scrollPanePInteres.revalidate();
		scrollPanePInteres.repaint();
		
	}
}