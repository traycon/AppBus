import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BD.Evento;
import BD.Parada;

public class Zona_Informacion_Puntos_Interes_admin extends Modelo_asignacion {
	private JButton subirFotoB;

	public Zona_Informacion_Puntos_Interes_admin(){
		subirFotoB = new JButton("Subir Foto");
		subirFotoB.setBounds(683, 9, 98, 26);
		subirFotoB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subirFoto();
			}
		});
		
		this.getAgregarB().setBounds(730, 395, 41, 26);
		this.getQuitarB().setBounds(788, 395, 41, 26);
		this.getScrollPaneF().setBounds(644, 300, 268, 90);
		this.getScrollPaneT().setBounds(644, 425, 270, 75);
		
			actualizarTT();
			actualizarTF();
		

	}
 void actualizarTF() {
		Object[][] usTable=new Object[0][2];
		if(Informacion_Puntos_Interes_admin.PuntoOrmIndex()!=null){
			BD.Evento[] eventos = null;
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_Eventos_PuntosInteres(Informacion_Puntos_Interes_admin.PuntoOrmIndex().getORMID());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null){
					usTable=new Object[eventos.length][2];
						for (int i = 0; i < eventos.length; i++) {
									usTable[i][0]=eventos[i].getORMID();
									usTable[i][1]=eventos[i].getNombre();
						}
					}
		}
		this.getTablaFiltrada().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Evento"}));
		
		for (int c = 0; c <
				this.getTablaFiltrada().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaFiltrada().getColumnClass(c);
			this.getTablaFiltrada().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaFiltrada().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneF().setViewportView(this.getTablaFiltrada());
		this.getScrollPaneF().revalidate();
		this.getScrollPaneF().repaint();
		
	}
 void actualizarTT() {
		Object[][] usTable=new Object[0][2];
		if(Informacion_Puntos_Interes_admin.PuntoOrmIndex()!=null){
			Evento[] eventos = null;
			ArrayList<Evento> event=new ArrayList<Evento>();
			try {
				eventos = MainAplicacion.getBdAdministrador().cargar_Eventos_PuntosInteres(Informacion_Puntos_Interes_admin.PuntoOrmIndex().getORMID());
				event=MainAplicacion.getBdAdministrador().Cargar_Eventos();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				if(eventos!=null&&!event.isEmpty()){
					for (int i = 0; i < eventos.length; i++) {
						int id=buscarIndice(event,eventos[i].getORMID());
						if(id>-1){
						event.remove(id);
						}
					}
					
					usTable=new Object[event.size()][2];
						for (int i = 0; i < event.size(); i++) {
									usTable[i][0]=event.get(i).getORMID();
									usTable[i][1]=event.get(i).getNombre();
						}
					}
		}
		this.getTablaTotal().setModel(new DefaultTableModel(usTable, new String[] {
				"Numero","Evento"}));
		
		for (int c = 0; c <
				this.getTablaTotal().getColumnCount(); c++) 
		{
			Class col_class =
					this.getTablaTotal().getColumnClass(c);
			this.getTablaTotal().setDefaultEditor(col_class, null); // Hace que no se pueda editar 
		}
		
		this.getTablaTotal().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getScrollPaneT().setViewportView(this.getTablaTotal());
		this.getScrollPaneT().revalidate();
		this.getScrollPaneT().repaint();
		
	}
	
	private int buscarIndice(ArrayList<Evento> event, int ormid) {
	int aux=-1;
	for (int i = 0; i < event.size(); i++) {
		if(event.get(i).getORMID()==ormid){
			aux=i;
			break;
		}
		
	}
	return aux;
}
	public void subirFoto() {
		throw new UnsupportedOperationException();
	}

	public JButton getSubirFotoB() {
		return subirFotoB;
	}

	public void setSubirFotoB(JButton subirFotoB) {
		this.subirFotoB = subirFotoB;
	}
	@Override
	public void agregar() {
		JTable tabla = this.getTablaTotal();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().agregarEventoPunto_Interes(Integer.parseInt(tabla.getValueAt(fila, 0).toString()), Informacion_Puntos_Interes_admin.PuntoOrmIndex().getORMID());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
	@Override
	public void quitar() {
		JTable tabla = this.getTablaFiltrada();
		int fila = tabla.getSelectedRow();
		try {
			MainAplicacion.getBdAdministrador().quitarEventoPunto_Interes(Integer.parseInt(tabla.getValueAt(fila, 0).toString()), Informacion_Puntos_Interes_admin.PuntoOrmIndex().getORMID());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actualizarTF();
		actualizarTT();
	}
}